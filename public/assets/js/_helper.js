var $ = jQuery.noConflict(),
    helper = (function () {
        return {
            digitOnly: function () {
                $('.js-digitOnly').on('keypress', function (e) {
                    if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
            },
            initTooltip: function () {
                $('[data-toggle="tooltip"]').tooltip();
            },
            initSelect2: function () {
                $('.js-select2').each(function () {
                    $(this).select2();
                });
            },
            extend: function (obj, src) {
                for (var key in src) {
                    if (src.hasOwnProperty(key)) obj[key] = src[key];
                }
                return obj;
            },
            cookieSet: function (c_name, value, exdays) {
                var exdate = new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                document.cookie = c_name + "=" + c_value;
            },
            cookieGet: function (c_name) {
                var i, x, y, ARRcookies = document.cookie.split(";");
                for (i = 0; i < ARRcookies.length; i++) {
                    x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                    x = x.replace(/^\s+|\s+$/g, "");
                    if (x === c_name) {
                        return unescape(y);
                    }
                }
            },
            cookieCheck: function (c_name) {
                var cookie = this.cookieGet(c_name);
                if (cookie != null && cookie !== "") {
                    return true;
                }
                return false;
            },
            cookieDelete: function (c_name) {
                document.cookie = encodeURIComponent(c_name) + "=deleted; expires=" + new Date(0).toUTCString();
            },
            validateForm: function (show_message, elm) {
                var self = this;
                var form = (elm === undefined) ? $('.js-formValidate') : elm;
                form.each(function () {
                    var me = $(this);
                    var opts = {};
                    if (!show_message) {
                        opts = {
                            errorPlacement: function (error, element) {
                                // element.parent().addClass("error");
                                if ($(element).closest('.date-wrapper').length > 0) {
                                    $(element).closest('.date-wrapper').addClass('error');
                                } else {
                                    $(element).closest('.date-wrapper').removeClass('error');
                                }
                                /*
                                var errors = '';
                                $.each(error, function (k, v) {
                                    errors += v.innerHTML;
                                });
                                */
                            }
                        }
                    }
                    me.validate(opts);
                });

                if (lang === 'id') {
                    $.extend(jQuery.validator.messages, {
                        required: "Kolom ini diperlukan.",
                        remote: "Harap benarkan kolom ini.",
                        email: "Silakan masukkan format email yang benar.",
                        url: "Silakan masukkan format URL yang benar.",
                        date: "Silakan masukkan format tanggal yang benar.",
                        dateISO: "Silakan masukkan format tanggal(ISO) yang benar.",
                        number: "Silakan masukkan angka yang benar.",
                        digits: "Harap masukan angka saja.",
                        creditcard: "Harap masukkan format kartu kredit yang benar.",
                        equalTo: "Harap masukkan nilai yg sama dengan sebelumnya.",
                        maxlength: $.validator.format("Input dibatasi hanya {0} karakter."),
                        minlength: $.validator.format("Input tidak kurang dari {0} karakter."),
                        rangelength: $.validator.format("Panjang karakter yg diizinkan antara {0} dan {1} karakter."),
                        range: $.validator.format("Harap masukkan nilai antara {0} dan {1}."),
                        max: $.validator.format("Harap masukkan nilai lebih kecil atau sama dengan {0}."),
                        min: $.validator.format("Harap masukkan nilai lebih besar atau sama dengan {0}.")
                    });
                }
            },
            backToTop: function (window, elm) {
                (window.scrollTop() > 100) ? elm.fadeIn() : elm.fadeOut();
            },
            showNotification: function (option) {
                if (option !== undefined) {
                    option.message = (option.message !== undefined) ? option.message : '';
                    option.type = (option.type !== undefined) ? ((option.type === 'success' || option.type === 'info' || option.type === 'warning' || option.type === 'danger') ? option.type : 'info') : 'info';

                    var icon = 'fa-info-circle';
                    switch (option.type) {
                        case 'success':
                            icon = 'fa fa-check-circle';
                            break;
                        case 'info':
                            icon = 'fa fa-info-circle';
                            break;
                        case 'warning':
                            icon = 'fa fa-exclamation-circle';
                            break;
                        case 'danger':
                            icon = 'fa fa-times-circle';
                            break;
                    }

                    if (option.title === undefined) {
                        switch (option.type) {
                            case 'success':
                                option.title = 'Success';
                                break;
                            case 'info':
                                option.title = 'Information';
                                break;
                            case 'warning':
                                option.title = 'Warning';
                                break;
                            case 'danger':
                                option.title = 'Error';
                                break;
                        }
                    }

                    $.notify({
                        title: option.title,
                        icon: icon,
                        message: option.message,
                    }, {
                        type: option.type, //success, warning, danger, info
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        newest_on_top: true
                    });
                }

            },
            buttonLoading: function (elm, type) {
                var loading = 'loading...';
                setTimeout(function () {
                    var btnType = elm.data('type');
                    if (btnType === 'submit' || btnType === 'reset' || btnType === 'button') {
                        if (type !== 'reset') {
                            elm.attr('disabled', true);
                            if (!(elm.hasClass('btn-loading'))) {
                                elm.attr('data-html', elm.val());
                                elm.val(loading);
                                elm.addClass('btn-loading');
                            }
                        } else {
                            elm.removeAttr('disabled');
                            elm.val(elm.data('html'));
                            elm.removeAttr('data-html');
                            elm.removeClass('btn-loading');
                        }
                    } else {
                        if (type !== 'reset') {
                            elm.attr('disabled', true);
                            if (!(elm.hasClass('btn-loading'))) {
                                if ($('i', elm).length > 0) {
                                    $('i', elm).attr('data-class', $('i', elm).attr('class'));
                                    $('i', elm).attr('class', 'fa fa-spin fa-spinner');
                                }

                                if ($('.txt', elm).length > 0) {
                                    $('.txt', elm).attr('data-html', $('.txt', elm).html());
                                    $('.txt', elm).html(loading);
                                } else {
                                    elm.attr('data-html', elm.html());
                                    elm.html(loading);
                                }
                                elm.addClass('btn-loading');
                            }
                        } else {
                            elm.removeAttr('disabled');
                            if ($('i', elm).length > 0) {
                                $('i', elm).attr('class', $('i', elm).data('class'));
                                $('i', elm).removeAttr('data-class');
                            }

                            if ($('.txt', elm).length > 0) {
                                $('.txt', elm).html($('.txt', elm).data('html'));
                                $('.txt', elm).removeAttr('data-html');
                            } else {
                                elm.html(elm.data('html'));
                                elm.removeAttr('data-html');
                            }
                            elm.removeClass('btn-loading');
                        }
                    }

                }, 100);
            },
            showDialogModal: function (opts, success, cancel) {
                swal({
                    title: (opts.title !== undefined) ? opts.title : '',
                    text: (opts.text !== undefined) ? opts.text : '',
                    type: (opts.type !== undefined) ? opts.type : '',
                    confirmButtonText: (opts.confirmButtonText !== undefined) ? opts.confirmButtonText : 'Yes',
                    cancelButtonText: (opts.cancelButtonText !== undefined) ? opts.cancelButtonText : 'No',
                    showCancelButton: (opts.showCancelButton !== undefined) ? opts.showCancelButton : true,
                    closeOnConfirm: (opts.closeOnConfirm !== undefined) ? opts.closeOnConfirm : true,
                    closeOnCancel: (opts.closeOnCancel !== undefined) ? opts.closeOnCancel : true
                }, function (isConfirm) {
                    if (isConfirm) {
                        if (typeof (success) === 'function') {
                            success();
                        }
                    } else {
                        if (typeof (cancel) === 'function') {
                            cancel();
                        }
                    }
                });
            },
            emojiPicker: function (input, button, parent) {
                button.on('click', function (e) {
                    e.stopPropagation();
                    $('.intercom-composer-emoji-popover', parent).toggleClass('active');
                });

                $(document).on('click', function (e) {
                    if ($(e.target).attr('class') !== '.intercom-composer-emoji-popover' && $(e.target).parents('.intercom-composer-emoji-popover').length < 1) {
                        $('.intercom-composer-emoji-popover', parent).removeClass('active');
                    }
                });

                parent.on('click.emoji', '.intercom-emoji-picker-emoji', function (e) {
                    var value = input.val();
                    input.val(value + $(this).html());
                    input.focus();
                });

                $('.intercom-composer-popover-input', parent).on('input', function () {
                    var query = this.value;
                    if (query !== "") {
                        $('.intercom-emoji-picker-emoji:not([title*="' + query + '"])', parent).hide();
                    } else {
                        $('.intercom-emoji-picker-emoji', parent).show();
                    }
                });
            },
        };
    })();