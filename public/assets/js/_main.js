(function ($) {
    $(function () {
        $(document).ready(function () {
            $('body').addClass(is_mobile ? 'mobile' : 'desktop');
            helper.validateForm(true);
            moment.updateLocale('en', {
                relativeTime: {
                    future: 'in %s',
                    past: '%s ago',
                    s: 'a sec',
                    m: '1 min',
                    mm: '%d mins',
                    h: '1 hour',
                    hh: '%d hours',
                    d: '1 day',
                    dd: '%d days',
                    M: '1 month',
                    MM: '%d months',
                    y: '1 year',
                    yy: '%d years'
                }
            });

            helper.initTooltip();
            helper.initSelect2();

            /* REMOVE LOADING */
            $('.js-telemedLoading').fadeOut(300, function () {
                $(this).remove();
            });

            $(document).on('click', '.js-globalRemove', function () {
                var me = $(this);
                var name = me.data('remove');
                var text = 'Apakah anda yakin ingin menghapus data ini?';
                if (name !== undefined && name !== '') {
                    text = 'Apakah anda yakin ingin menghapus ' + name + '?';
                }
                var swal_opts = {
                    title: 'Hapus?',
                    text: text,
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary rounded',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                };

                helper.showDialogModal(swal_opts, function () {
                    window.location.href = me.attr('href');
                    return true;
                });

                return false;
            });

            /* SHOW CONFIRMATION MODAL BEFORE SUBMIT FORM */
            $('.js-telemedForm').each(function () {
                var me = $(this);
                $('[type="submit"]', me).on('click', function () {
                    var form = $(this).closest('form');
                    var title = me.data('confirmation-title');
                    var text = me.data('confirmation-message');
                    if(form.valid()) {
                        var swal_opts = {
                            title: title !== undefined ? title : 'Konfirmasi',
                            text: text !== undefined ? text : 'Apakah anda yakin ingin menyimpan data ini?',
                            showCancelButton: true,
                            confirmButtonText: 'Ya',
                            cancelButtonText: 'Batal',
                        };

                        helper.showDialogModal(swal_opts, function () {
                            form.submit();
                        });

                        return false;
                    }
                });
            });
        });
    });
})(jQuery);
