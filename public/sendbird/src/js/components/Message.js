import {createDivEl, isImage, protectFromXSS, setDataInElement, timestampToTime} from '../utils';
import {SendBirdAction} from '../SendBirdAction';
import {COLOR_RED, MESSAGE_REQ_ID} from '../const';
import {MessageDeleteModal} from './MessageDeleteModal';
import {UserBlockModal} from './UserBlockModal';
import {Chat} from '../Chat';

class Message {
    constructor({channel, message}) {
        this.channel = channel;
        this.message = message;
        this.element = this._createElement();
    }

    _createElement() {
        if (this.message.isUserMessage()) {
            return this._createUserElement();
        } else if (this.message.isFileMessage()) {
            return this._createFileElement();
        } else if (this.message.isAdminMessage()) {
            return this._createAdminElement();
        } else {
            //console.error('Message is invalid data.');
            return null;
        }
    }

    _hoverOnNickname(nickname, hover) {
        if (!SendBirdAction.getInstance().isCurrentUser(this.message.sender)) {
            nickname.innerHTML = hover ? 'BLOCK   ' : `${protectFromXSS(this.message.sender.nickname)} : `;
            nickname.style.color = hover ? COLOR_RED : '';
            nickname.style.opacity = hover ? '1' : '';
        }
    }

    _hoverOnTime(time, hover) {
        if (SendBirdAction.getInstance().isCurrentUser(this.message.sender)) {
            time.innerHTML = hover ? 'DELETE' : timestampToTime(this.message.createdAt);
            time.style.color = hover ? COLOR_RED : '';
            time.style.opacity = hover ? '1' : '';
            time.style.fontWeight = hover ? '600' : '';
        }
    }

    _createUserElement() {
        const sendbirdAction = SendBirdAction.getInstance();
        const isCurrentUser = sendbirdAction.isCurrentUser(this.message.sender);
        const root = createDivEl({
            className: isCurrentUser ? 'chat-message is-user' : 'chat-message',
            id: this.message.messageId
        });
        setDataInElement(root, MESSAGE_REQ_ID, this.message.reqId);

        const messageContent = createDivEl({className: 'message-content'});
        const nickname = createDivEl({
            className: isCurrentUser ? 'message-nickname is-user' : 'message-nickname',
            content: `${protectFromXSS(this.message.sender.nickname)} : `
        });
        /*nickname.addEventListener('mouseover', () => {
          this._hoverOnNickname(nickname, true);
        });
        nickname.addEventListener('mouseleave', () => {
          this._hoverOnNickname(nickname, false);
        });*/
        nickname.addEventListener('click', () => {
            if (!isCurrentUser) {
                const userBlockModal = new UserBlockModal({user: this.message.sender, isBlock: true});
                userBlockModal.render();
            }
        });
        messageContent.appendChild(nickname);

        const msg = createDivEl({className: 'message-detail', content: protectFromXSS(this.message.message)});
        messageContent.appendChild(msg);

        const time_read = createDivEl({className: 'time-read'});

        const time = createDivEl({
            className: isCurrentUser ? 'time is-user' : 'time',
            content: timestampToTime(this.message.createdAt)
        });
        /*time.addEventListener('mouseover', () => {
          this._hoverOnTime(time, true);
        });
        time.addEventListener('mouseleave', () => {
          this._hoverOnTime(time, false);
        });*/
        time.addEventListener('click', () => {
            if (isCurrentUser) {
                const messageDeleteModal = new MessageDeleteModal({
                    channel: this.channel,
                    message: this.message
                });
                messageDeleteModal.render();
            }
        });
        time_read.appendChild(time);

        if (this.channel.isGroupChannel()) {
            const count = sendbirdAction.getReadReceipt(this.channel, this.message);
            const read = createDivEl({
                className: count ? 'read active' : 'read',
                //content: count
            });
            time_read.appendChild(read);
        }

        messageContent.appendChild(time_read);

        root.appendChild(messageContent);
        return root;
    }

    _createFileElement() {
        const sendbirdAction = SendBirdAction.getInstance();
        const isCurrentUser = sendbirdAction.isCurrentUser(this.message.sender);
        const root = createDivEl({
            className: isCurrentUser ? 'chat-message is-user' : 'chat-message',
            id: this.message.messageId
        });

        setDataInElement(root, MESSAGE_REQ_ID, this.message.reqId);

        const messageContent = createDivEl({className: 'message-content'});
        const nickname = createDivEl({
            className: sendbirdAction.isCurrentUser(this.message.sender) ? 'message-nickname is-user' : 'message-nickname',
            content: `${protectFromXSS(this.message.sender.nickname)} : `
        });
        messageContent.appendChild(nickname);

        const msg = createDivEl({
            className: 'message-detail is-file',
            content: protectFromXSS(this.message.name)
        });

        msg.addEventListener('click', () => {
            window.open(this.message.url);
        });

        if (isImage(this.message.type) && this.message.messageId) {
            const imageContent = createDivEl({className: 'image-content mb-2 mt-1'});
            imageContent.addEventListener('click', () => {
                window.open(this.message.url);
            });
            const imageRender = document.createElement('img');
            imageRender.className = 'image-render';
            imageRender.src = protectFromXSS(this.message.url);
            imageRender.onload = () => {
                Chat.getInstance().main.repositionScroll(imageRender.offsetHeight);
            };
            imageContent.appendChild(imageRender);
            messageContent.appendChild(imageContent);
        }

        messageContent.appendChild(msg);

        const time_read = createDivEl({className: 'time-read'});
        const time = createDivEl({className: 'time', content: timestampToTime(this.message.createdAt)});
        /*time.addEventListener('mouseover', () => {
          this._hoverOnTime(time, true);
        });
        time.addEventListener('mouseleave', () => {
          this._hoverOnTime(time, false);
        });*/
        time.addEventListener('click', () => {
            const messageDeleteModal = new MessageDeleteModal({
                channel: this.channel,
                message: this.message
            });
            messageDeleteModal.render();
        });
        time_read.appendChild(time);

        if (this.channel.isGroupChannel()) {
            const count = sendbirdAction.getReadReceipt(this.channel, this.message);
            const read = createDivEl({
                className: count ? 'read active' : 'read',
                //content: count
            });
            time_read.appendChild(read);
        }

        messageContent.appendChild(time_read);
        root.appendChild(messageContent);

        return root;
    }

    _createAdminElement() {
        const sendbirdAction = SendBirdAction.getInstance();
        const isCurrentUser = sendbirdAction.isCurrentUser(this.message.sender);
        const root = createDivEl({
            className: isCurrentUser ? 'chat-message is-user' : 'chat-message',
            id: this.message.messageId
        });

        const msg = createDivEl({className: 'message-admin', content: protectFromXSS(this.message.message)});
        root.appendChild(msg);
        return root;
    }

    static getRootElementClasasName() {
        return 'chat-message';
    }

    static getReadReceiptElementClassName() {
        return 'active';
    }
}

export {Message};
