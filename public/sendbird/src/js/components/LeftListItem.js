import {
    addClass,
    createDivEl,
    getDataInElement,
    protectFromXSS,
    removeClass,
    setDataInElement,
    timestampFromNow
} from '../utils';
import {ChatLeftMenu} from '../ChatLeftMenu';

const KEY_MESSAGE_LAST_TIME = 'origin';

class LeftListItem {
    constructor({channel, handler}) {
        this.channel = channel;
        this.element = this._createElement(handler);
    }

    get channelUrl() {
        return this.channel.url;
    }

    get title() {
        return this.channel.isOpenChannel()
            ? `# ${this.channel.name}`
            : this.channel.members
                .map(member => {
                    return protectFromXSS(member.nickname);
                })
                .join(', ');
    }

    get name() {
        return this.channel.name;
    }

    get avatar() {
        return this.channel.coverUrl;
    }

    get customData() {
        if(this.channel.data !== '') {
            return JSON.parse(this.channel.data);
        } else {
            return null;
        }
    }

    get lastMessagetime() {
        if (this.channel.isOpenChannel() || !this.channel.lastMessage) {
            return 0;
        } else {
            return this.channel.lastMessage.createdAt;
        }
    }

    get lastMessageTimeText() {
        if (this.channel.isOpenChannel() || !this.channel.lastMessage) {
            return 0;
        } else {
            return LeftListItem.getTimeFromNow(this.channel.lastMessage.createdAt);
        }
    }

    get lastMessageText() {
        if (this.channel.isOpenChannel() || !this.channel.lastMessage) {
            return '';
        } else {
            return this.channel.lastMessage.isFileMessage()
                ? protectFromXSS(this.channel.lastMessage.name)
                : protectFromXSS(this.channel.lastMessage.message);
        }
    }

    get memberCount() {
        return this.channel.isOpenChannel() ? '#' : this.channel.memberCount;
    }

    get unreadMessageCount() {
        const count = this.channel.unreadMessageCount > 9 ? '+9' : this.channel.unreadMessageCount.toString();
        return this.channel.isOpenChannel() ? 0 : count;
    }

    _createElement(handler) {
        const item = createDivEl({className: 'chat-item js-chatItem', id: this.channelUrl});
        if(this.customData !== null) {
            item.setAttribute('data-user-id', this.customData.user_id);
            item.setAttribute('data-chat-id', this.customData.chat_id);
            item.setAttribute('data-channel-url', this.customData.channel_url);
            item.setAttribute('data-status', this.customData.status);
            item.setAttribute('data-leave', this.customData.user_leave_channel);
            item.setAttribute('data-expired', this.customData.expired);
        }
        const itemNotes = createDivEl({className: 'notes d-none', content: '<div class="txt"></div><div class="timer"></div>'});
        const itemRow = createDivEl({className: 'row row-5'});
        item.appendChild(itemNotes);
        item.appendChild(itemRow);

        if (this.channel.isOpenChannel()) {
            const itemTop = createDivEl({className: 'item-top'});
            const itemTopTitle = createDivEl({className: 'item-title', content: this.title});
            itemTop.appendChild(itemTopTitle);
            itemRow.appendChild(itemTop);
        } else {
            //const itemTop = createDivEl({className: 'item-top'});
            //const itemTopCount = createDivEl({className: 'item-count', content: this.memberCount});
            //const itemTopTitle = createDivEl({className: 'item-title', content: this.title});

            const name = createDivEl({className: 'name', content: this.name});
            const image = document.createElement('img');
            image.src = this.avatar;
            const spec_content = (this.customData !== null) ? this.customData.spec : '';
            const spec = createDivEl({className: 'spec', content: spec_content});
            const time = createDivEl({className: 'time', content: this.lastMessageTimeText});
            const btnClose = createDivEl({className: 'btn btn-sm mt-1 btn-orange rounded btn-close js-closeChat', content: 'Tutup'});
            const message = createDivEl({className: 'message', content: this.lastMessageText});
            const notify = createDivEl({className: 'notify', content: this.unreadMessageCount});
            notify.setAttribute('data-count', this.unreadMessageCount);
            const itemRowCol1 = createDivEl({className: 'col-2 text-center'});
            const itemRowCol2 = createDivEl({className: 'col-10'});
            const itemRowCol2Row1 = createDivEl({className: 'row row-5'});
            const itemRowCol2Row1Col1 = createDivEl({className: 'col-7'});
            const itemRowCol2Row1Col2 = createDivEl({className: 'col-5 text-right'});
            const itemRowCol2Row2 = createDivEl({className: 'row row-5'});
            const itemRowCol2Row2Col1 = createDivEl({className: 'col-10 pt-2'});
            const itemRowCol2Row2Col2 = createDivEl({className: 'col-2 text-right align-self-center'});

            // itemTop.appendChild(itemTopCount);
            // itemTop.appendChild(itemTopTitle);

            btnClose.innerHTML = 'Tutup';

            itemRowCol1.appendChild(image);
            itemRow.appendChild(itemRowCol1);
            itemRowCol2Row1Col1.appendChild(name);
            itemRowCol2Row1Col1.appendChild(spec);
            itemRowCol2Row1.appendChild(itemRowCol2Row1Col1);
            itemRowCol2Row1Col2.appendChild(time);
            itemRowCol2Row1Col2.appendChild(btnClose);
            itemRowCol2Row1.appendChild(itemRowCol2Row1Col2);
            itemRowCol2.appendChild(itemRowCol2Row1);
            itemRowCol2Row2Col1.appendChild(message);
            itemRowCol2Row2.appendChild(itemRowCol2Row2Col1);
            itemRowCol2Row2Col2.appendChild(notify);
            itemRowCol2Row2.appendChild(itemRowCol2Row2Col2);
            itemRowCol2.appendChild(itemRowCol2Row2);
            itemRow.appendChild(itemRowCol2);

            setDataInElement(time, KEY_MESSAGE_LAST_TIME, this.lastMessagetime);
        }

        item.addEventListener('click', () => {
            if (handler) handler();
        });

        return item;
    }

    static updateUnreadCount() {
        const items = ChatLeftMenu.getInstance().groupChannelList.getElementsByClassName('item-message-unread');
        if (items && items.length > 0) {
            Array.prototype.slice.call(items).forEach(targetItemEl => {
                const originTs = targetItemEl.textContent;
                if (originTs === '0') {
                    removeClass(targetItemEl, 'active');
                } else {
                    addClass(targetItemEl, 'active');
                }
            });
        }
    }

    static updateLastMessageTime() {
        const items = ChatLeftMenu.getInstance().groupChannelList.getElementsByClassName('item-time');
        if (items && items.length > 0) {
            Array.prototype.slice.call(items).forEach(targetItemEl => {
                const originTs = parseInt(getDataInElement(targetItemEl, KEY_MESSAGE_LAST_TIME));
                if (originTs) {
                    targetItemEl.innerHTML = LeftListItem.getTimeFromNow(originTs);
                }
            });
        }
    }

    static getTimeFromNow(timestamp) {
        return timestampFromNow(timestamp);
    }

    static getItemRootClassName() {
        return 'chat-item';
    }
}

export {LeftListItem};