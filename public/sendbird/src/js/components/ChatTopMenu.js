import {createDivEl, errorAlert, protectFromXSS} from '../utils';
import {Chat} from '../Chat';
import {ChatLeftMenu} from '../ChatLeftMenu';
import {UserList} from './UserList';
import {SendBirdAction} from '../SendBirdAction';

class ChatTopMenu {
    constructor(channel) {
        this.channel = channel;
        this.element = this._createElement(channel);
    }

    get chatTitle() {
        const isOpenChannel = this.channel.isOpenChannel();
        if (isOpenChannel) {
            return `# ${this.channel.name}`;
        } else {
            return this.channel.members
                .map(member => {
                    return protectFromXSS(member.nickname);
                })
                .join(', ');
        }
    }

    get name() {
        return this.channel.name;
    }

    get avatar() {
        return this.channel.coverUrl;
    }

    get customData() {
        if (this.channel.data !== '') {
            return JSON.parse(this.channel.data);
        } else {
            return null;
        }
    }

    _createElement(channel) {
        const isOpenChannel = channel.isOpenChannel();

        const root = createDivEl({className: 'chat-top'});

        const back = createDivEl({
            className: 'back js-back',
            content: '<i class="fa fa-chevron-left"></i>'
        });

        const summary = createDivEl({
            className: 'summary js-summary',
            content: '<i class="fa fa-ellipsis-v"></i>'
        });

        const image = document.createElement('img');
        image.src = this.avatar;

        this.title = createDivEl({
            className: isOpenChannel ? 'chat-title' : 'chat-title is-group',
            content: this.customData.user_name + '<div class="reference d-none js-reference">Reference number: ' + ((this.customData !== null) ? this.customData.chat_id : '') + '</div>'
        });

        const topRowCol1 = createDivEl({className: 'col-md-8 static'});
        const topRowCol2 = createDivEl({
            className: 'col-md-4 text-right static',
            content: '<div class="timer-wrapper text-center d-none js-timerWrapper"><div>Waktu tersisa:</div><div class="timer js-timer"' + ((this.customData !== null) ? ' data-chat-id="' + this.customData.chat_id + '" data-expired="' + this.customData.expired + '"' : '') + '>00 : 00</div></div>'
        });

        const topRow = createDivEl({className: 'row row-5'});

        topRowCol1.appendChild(back);
        topRowCol1.appendChild(image);
        topRowCol1.appendChild(this.title);
        topRowCol1.appendChild(summary);

        topRow.appendChild(topRowCol1);
        topRow.appendChild(topRowCol2);
        root.appendChild(topRow);

        const button = createDivEl({className: 'chat-button'});
        if (!isOpenChannel) {
            const invite = createDivEl({className: 'button-invite'});
            invite.addEventListener('click', () => {
                UserList.getInstance().render(true);
            });
            button.appendChild(invite);
            const hide = createDivEl({className: 'button-hide'});
            hide.addEventListener('click', () => {
                SendBirdAction.getInstance()
                    .hide(channel.url)
                    .then(() => {
                        ChatLeftMenu.getInstance().removeGroupChannelItem(this.channel.url);
                        Chat.getInstance().renderEmptyElement();
                    })
                    .catch(error => {
                        errorAlert(error.message);
                    });
            });
            button.appendChild(hide);
        }
        const leave = createDivEl({className: 'button-leave'});
        leave.addEventListener('click', () => {
            if (isOpenChannel) {
                ChatLeftMenu.getInstance().removeOpenChannelItem(this.channel.url);
                SendBirdAction.getInstance()
                    .exit(channel.url)
                    .then(() => {
                        Chat.getInstance().renderEmptyElement();
                    })
                    .catch(error => {
                        errorAlert(error.message);
                    });
            } else {
                SendBirdAction.getInstance()
                    .leave(channel.url)
                    .then(() => {
                        ChatLeftMenu.getInstance().removeGroupChannelItem(this.channel.url);
                        Chat.getInstance().renderEmptyElement();
                    })
                    .catch(error => {
                        errorAlert(error.message);
                    });
            }
        });
        button.appendChild(leave);
        root.appendChild(button);
        return root;
    }

    updateTitle(channel) {
        this.channel = channel;
        //this.title.innerHTML = this.chatTitle;
    }
}

export {ChatTopMenu};