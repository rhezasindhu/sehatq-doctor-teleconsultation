import {createDivEl, protectFromXSS} from '../utils';
import {DISPLAY_BLOCK, DISPLAY_NONE, FILE_ID, KEY_ENTER} from '../const';
import {SendBirdAction} from '../SendBirdAction';
import {Chat} from '../Chat';

class ChatInput {
    constructor(channel) {
        this.channel = channel;
        this.input = null;
        this.typing = null;
        this.element = this._createElement(channel);
    }

    _createElement(channel) {
        const root = createDivEl({className: 'chat-input'});

        this.typing = createDivEl({className: 'typing-field'});

        const data = JSON.parse(channel.data);

        root.appendChild(this.typing);

        const file = document.createElement('label');
        file.className = 'input-file';
        file.for = FILE_ID;
        file.addEventListener('click', () => {
            if (this.channel.isGroupChannel()) {
                SendBirdAction.getInstance().markAsRead(this.channel);
            }
        });

        const fileInput = document.createElement('input');
        fileInput.type = 'file';
        fileInput.id = FILE_ID;
        fileInput.style.display = DISPLAY_NONE;
        fileInput.addEventListener('change', () => {
            const sendFile = fileInput.files[0];
            if (sendFile) {
                const tempMessage = SendBirdAction.getInstance().sendFileMessage({
                    channel: this.channel,
                    file: sendFile,
                    handler: (message, error) => {
                        error
                            ? Chat.getInstance().main.removeMessage(tempMessage.reqId, true)
                            : Chat.getInstance().main.renderMessages([message]);
                    }
                });
                Chat.getInstance().main.renderMessages([tempMessage]);
            }
        });

        file.appendChild(fileInput);
        const emoji = createDivEl({className: 'emoji js-emojiPicker', content: '<i class="far fa-smile"></i>'});
        const send = createDivEl({className: 'send js-sendMessage', content: '<i class="fab fa-telegram-plane mr-1"></i>'});
        root.appendChild(file);
        root.appendChild(emoji);
        root.appendChild(send);

        const inputText = createDivEl({className: 'input-text'});

        let placeholder = 'Silahkan ketik disini...';
        const user_name = channel.name;

        this.input = document.createElement('textarea');
        this.input.className = 'input-text-area';
        this.input.setAttribute('data-status', data.status);
        this.input.setAttribute('data-chat-id', data.chat_id);

        if(data.user_leave_channel === true) {
            placeholder = user_name + ' telah mengakhiri percakapan';
            this.input.disabled=true;
            root.className = 'chat-input disabled';
        } else {
            switch (data.status) {
                case 'waiting':
                    placeholder = user_name + ' sedang menunggu ingin berkonsultasi dengan anda';
                    break;
                case 'match':
                    placeholder = 'Mohon menunggu sampai ' + user_name + ' siap';
                    break;
                case 'declined':
                    placeholder = user_name + ' telah membatalkan konsultasi dengan anda';
                    break;
                case 'done':
                    placeholder = 'Konsultasi anda dengan ' + user_name + ' telah selesai';
                    break;
                default:
                    placeholder = 'Silahkan ketik disini...';
                    break;
            }

            if(data.status === 'accepted' || data.status === 'extended') {
                this.input.disabled=false;
                root.className = 'chat-input';
            } else {
                this.input.disabled=true;
                root.className = 'chat-input disabled';
            }
        }

        this.input.placeholder = placeholder;

        this.input.addEventListener('click', () => {
            if (this.channel.isGroupChannel()) {
                SendBirdAction.getInstance().markAsRead(this.channel);
            }
        });
        this.input.addEventListener('keypress', e => {
            if (e.keyCode === KEY_ENTER) {
                if (!e.shiftKey) {
                    e.preventDefault();
                    const message = this.input.value;
                    this.input.value = '';
                    if (message) {
                        const tempMessage = SendBirdAction.getInstance().sendUserMessage({
                            channel: this.channel,
                            message,
                            handler: (message, error) => {
                                error
                                    ? Chat.getInstance().main.removeMessage(tempMessage.reqId, true)
                                    : Chat.getInstance().main.renderMessages([message]);
                            }
                        });
                        Chat.getInstance().main.renderMessages([tempMessage]);
                        if (channel.isGroupChannel()) {
                            channel.endTyping();
                        }
                    }
                } else {
                    if (channel.isGroupChannel()) {
                        channel.startTyping();
                    }
                }
            } else {
                if (channel.isGroupChannel()) {
                    channel.startTyping();
                }
            }
        });
        send.addEventListener('click', e => {
            e.preventDefault();
            const message = this.input.value;
            this.input.value = '';
            if (message) {
                const tempMessage = SendBirdAction.getInstance().sendUserMessage({
                    channel: this.channel,
                    message,
                    handler: (message, error) => {
                        error
                            ? Chat.getInstance().main.removeMessage(tempMessage.reqId, true)
                            : Chat.getInstance().main.renderMessages([message]);
                    }
                });
                Chat.getInstance().main.renderMessages([tempMessage]);
                if (channel.isGroupChannel()) {
                    channel.endTyping();
                }
            }
        });
        this.input.addEventListener('focusin', () => {
            this.channel._autoMarkAsRead = true;
            inputText.style.border = '1px solid #2C2D30';
        });
        this.input.addEventListener('focusout', () => {
            this.channel._autoMarkAsRead = false;
            inputText.style.border = '';
        });

        inputText.appendChild(this.input);
        root.appendChild(inputText);
        return root;
    }

    updateTyping(memberList) {
        let nicknames = '';
        if (memberList.length === 1) {
            nicknames = `${protectFromXSS(memberList[0].nickname)} is`;
        } else if (memberList.length === 2) {
            nicknames = `${memberList
                .map(member => {
                    return protectFromXSS(member.nickname);
                })
                .join(', ')} are`;
        } else if (memberList.length !== 0) {
            nicknames = 'Several are';
        }
        this.typing.style.display = nicknames ? DISPLAY_BLOCK : DISPLAY_NONE;
        this.typing.innerHTML = `${nicknames} typing...`;
    }
}

export {ChatInput};
