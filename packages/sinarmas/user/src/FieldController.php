<?php
namespace Sinarmas\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctorfield;

class FieldController extends Controller {
    public function __construct() {
        $this->middleware(['web', 'auth:admin']);
    }

    public function searchEngine($q = "") {
        return helperMasterDoctorField('search', $q);
    }

    public function index() {
        $data = [
            'fields' => self::searchEngine(),
            'query'  => '',
        ];
        return view('user::field_index', ['data' => $data]);
    }

    public function search(Request $request) {
        if ($request->method() === 'GET') {
            $data = [
                'fields' => self::searchEngine($request->q),
                'query'  => $request->q,
            ];
            return view('user::field_index', ['data' => $data]);
        }
        return redirect()->back();
    }

    public function form(Request $request, $id = null) {
        if ($request->method() === 'POST') {
            //-- Validation
            $validatedData = $request->validate([
                'name' => 'required',
            ]);
            //-- Process
            $field = Doctorfield::updateOrCreate([
                'doctorfields_id' => $id
            ],[
                'doctorfields_name'        => $request->name,
                'doctorfields_slug'        => str_slug($request->name),
                'doctorfields_description' => $request->description,
            ]);
            //-- Message
            if ($field->wasRecentlyCreated || $field->wasChanged()) {
                $request->session()->flash('alert-success', ['Spesialisasi dokter telah tersimpan']);
            } else {
                $request->session()->flash('alert-error', ['Spesialisasi dokter gagal tersimpan']);
            }
            return redirect()->route('admin.doctor.field');
        }
        $data = [
            'field' => ($id) ? Doctorfield::findOrFail($id) : [],
        ];
        return view('user::field_form', ['data' => $data]);
    }

    public function delete(Request $request) {
        if ($request->method() === 'GET') {
            if ($request->field_id) {
                $id = $request->field_id;
                Doctorfield::find($id)->delete();
                $request->session()->flash('alert-success', ['Spesialisasi dokter telah dihapus']);
            }
            return redirect()->back();
        }
        return redirect()->back();
    }
}
