<?php
namespace Sinarmas\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usertype;

class RoleController extends Controller {
    public function __construct() {
        $this->middleware(['web', 'auth:admin']);
    }

    public function index() {
        $roles = helperMasterUserRole();
        $data = [
            'roles' => $roles
        ];
        return view('user::role_index', ['data' => $data]);
    }

    public function form(Request $request, $id = null) {
        if ($request->method() === 'POST') {
            //-- Validation
            $validatedData = $request->validate([
                'name' => 'required',
            ]);
            //-- Process
            $role = Usertype::updateOrCreate([
                'usertypes_id' => $id
            ],[
                'usertypes_role'        => $request->name,
                'usertypes_description' => $request->description,
            ]);
            //-- Message
            if ($role->wasRecentlyCreated || $role->wasChanged()) {
                $request->session()->flash('alert-success', ['Role telah tersimpan']);
            } else {
                $request->session()->flash('alert-error', ['Role gagal tersimpan']);
            }
            return redirect()->route('admin.role');
        }
        $data = [
            'role' => ($id) ? Usertype::findOrFail($id) : [],
        ];
        return view('user::role_form', ['data' => $data]);
    }

    public function delete(Request $request) {
        if ($request->method() === 'GET') {
            if ($request->role_id) {
                $id = $request->role_id;
                Usertype::find($id)->delete();
                $request->session()->flash('alert-success', ['Role telah dihapus']);
            }
            return redirect()->back();
        }
        return redirect()->back();
    }
}
