@extends('web.layouts.html')

@section('content')
    @php
        if ($data['role']) {
            $actionURL = route('admin.role.edit', $data['role']['usertypes_id']);
            $confirmationTitle = 'Ubah Role?';
            $confirmationMessage = 'Apakah anda yakin dengan perubahan role ini?';
        } else {
            $actionURL = route('admin.role.add');
            $confirmationTitle = 'Tambah Role?';
            $confirmationMessage = 'Apakah anda yakin dengan penambahan role ini?';
        }
    @endphp
    <div class="telemed page-standard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="telemed-back mb-4">
                    <a href="javascript:history.go(-1);" class="back">
                        <i class="fa fa-angle-left mr-1"></i>
                        <span class="txt">Kembali ke halaman sebelumnya</span>
                    </a>
                </div>
                <div class="telemed-title">{{ ($data['role']) ? 'Ubah' : 'Tambah' }} Role</div>
                <div class="form-section mt-5">
                    <form class="js-formValidate js-telemedForm" action="{{ $actionURL }}" data-confirmation-title="{{ $confirmationTitle }}" data-confirmation-message="{{ $confirmationMessage }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-xl-6 col-lg-8 col-md-10">
                                <div class="form-group mb-4">
                                    <label for="name" class="control-label">Nama Role</label>
                                    <input class="form-control rounded{{ $errors->has('name') ? ' error' : '' }}" name="name" id="name" type="text" placeholder="" value="{{ $data['role']['usertypes_role'] or old('name') }}" required="required" autocomplete="off" autofocus>
                                    @if ($errors->has('name'))
                                        <label class="error">{{ $errors->first('name') }}</label>
                                    @endif
                                </div>
                                <div class="form-group mb-4">
                                    <label for="description" class="control-label">Deskripsi</label>
                                    <textarea class="form-control rounded" name="description" id="description" maxlength="250">{{ $data['role']['usertypes_description'] or old('description') }}</textarea>
                                    <div class="help-block color-grey">Maksimal 250 karakter</div>
                                </div>
                                <div class="mt-4">
                                    <div class="row row-0">
                                        <div class="col-xl-9 col-lg-8 col-sm-7"></div>
                                        <div class="col-xl-3 col-lg-4 col-sm-5">
                                            <input type="hidden" name="created_by" value="{{ $data['role']['created_by'] or old('created_by') }}">
                                            <button class="btn btn-lg btn-block btn-outline-primary rounded upper-case" type="submit">{{ ($data['role']) ? 'Perbarui' : 'Tambah' }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-menu="master"]').addClass('active');
        });
    </script>
@endsection