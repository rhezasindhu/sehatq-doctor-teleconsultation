@extends('web.layouts.html')

@section('content')
    @php
        if ($data['doctor']) {
            $actionURL = route('admin.doctor.edit', $data['doctor']['id']);
            $confirmationTitle = 'Perbarui Data?';
            $confirmationMessage = 'Apakah anda yakin untuk memperbarui data dokter?';
        } else {
            $actionURL = route('admin.doctor.add');
            $confirmationTitle = 'Daftarkan Dokter?';
            $confirmationMessage = 'Apakah data yang anda masukan sudah benar untuk ditambahkan sebagai dokter baru?';
        }
        $doctor = ($data['doctor']) ? $data['doctor']:[];
        $detail = ($data['doctor']) ? $data['doctor']['detail']:[];

        if ($detail) {
            $date_exp = date('d-m-Y', strtotime($detail['userdetails_experience']));
        }

        $selectedField = "";
        if ($detail) {
            $selectedField = $detail['userdetails_doctorfields_id'];
        } elseif (old('field')) {
            $selectedField = old('field');
        }

        $photo = '';
        if ($detail) {
            $photo = $detail['userdetails_photo'];
        }

        $selectedSex = "m";
        if ($detail) {
            $selectedSex = strtolower($detail['userdetails_sex']);
        } elseif (old('sex')) {
            $selectedSex = old('sex');
        }

        $selectedSTR = "1";
        if ($detail) {
            $selectedSTR = $detail['userdetails_str'];
        } elseif (old('str')) {
            $selectedSTR = old('str');
        }

        $selectedSIP = "1";
        if ($detail) {
            $selectedSIP = $detail['userdetails_sip'];
        } elseif (old('sip')) {
            $selectedSIP = old('sip');
        }

        $selectedRole = "";
        if ($doctor) {
            if (old('role')) {
                $selectedRole = old('role');
            } else {
                $selectedRole = $doctor['account_type'];
            }
        }

        $active = 1;
        if ($doctor) {
            $active = $doctor['is_active'];
        }
    @endphp
    <div class="telemed page-standard" id="pageDoctorForm">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="telemed-back mb-4">
                    <a href="javascript:history.go(-1);" class="back">
                        <i class="fa fa-angle-left mr-1"></i>
                        <span class="txt">Kembali ke halaman sebelumnya</span>
                    </a>
                </div>
                <div class="telemed-title">{{ ($data['doctor']) ? 'Ubah Profil' : 'Tambah' }} Dokter</div>
                <div class="form-section mt-5">
                    <form class="js-formValidate js-telemedForm" action="{{ $actionURL }}" data-confirmation-title="{{ $confirmationTitle }}" data-confirmation-message="{{ $confirmationMessage }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-5 text-center">
                                <div class="mb-5">
                                    <div class="avatar-profile large">
                                        <img class="avatar" src="{{ helperGetProfilePicture($photo) }}" alt="">
                                    </div>
                                    <label class="mt-3 d-block">
                                        <span class="color-primary cursor-pointer opensansSemiBold">Change Image</span>
                                        <input class="form-control rounded d-none js-profileImageUpload" name="file" id="file" type="file" placeholder="" value="" autocomplete="off">
                                    </label>
                                </div>
                                @if(($data['doctor']))
                                <div class="form-group mb-4">
                                    <label class="control-label">ID Dokter</label>
                                    <div class="form-value"><strong class="font-16">{{ $doctor['id'] }}</strong></div>
                                </div>
                                @endif
                                <hr class="d-block d-sm-none">
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-7">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group mb-4">
                                            <label for="name" class="control-label">Nama Dokter<span class="required">*</span></label>
                                            <div class="row row-5">
                                                <div class="col-xl-2 col-sm-3 col-4">
                                                    <div class="select2-wrapper block">
                                                        <select class="form-control rounded js-select2" name="title" id="title">
                                                            <option value="Dr.">Dr.</option>
                                                            <option value="Prof.">Prof.</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-10 col-sm-9 col-8">
                                                    <input class="form-control rounded" name="name" id="name" type="text" placeholder="" value="{{ $doctor['name'] or old('name') }}" autocomplete="off" required="required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="sex" class="control-label">Jenis Kelamin</label>
                                            <div class="select2-wrapper block">
                                                <select class="form-control rounded js-select2" name="sex" id="sex">
                                                    <option value="m"{{ ($selectedSex == 'm') ? ' selected':'' }}>Pria</option>
                                                    <option value="f"{{ ($selectedSex == 'f') ? ' selected':'' }}>Wanita</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="date_exp" class="control-label">Berkarir Sejak</label>
                                            <div class="date-wrapper block">
                                                <input class="rounded js-datePickerCareer" type="text" name="date_exp" id="date_exp" placeholder="" value="{{ $date_exp or old('date_exp') }}" autocomplete="off">
                                                <i class="icon-calendar cursor-pointer"></i>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="email" class="control-label">Login Email<span class="required">*</span></label>
                                            <input class="form-control rounded" name="email" id="email" type="email" placeholder="" value="{{ $doctor['email'] or old('email') }}" autocomplete="off" required="required">
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="password" class="control-label">Password{!! $data['doctor'] ? '' : '<span class="required">*</span>' !!}</label>
                                            <input class="form-control rounded" name="password" id="password" type="password" placeholder="" value="" autocomplete="off"{!! $data['doctor'] ? '' : ' required=required minlength=8' !!}>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="contact" class="control-label">Nomor Telepon<span class="required">*</span></label>
                                            <input class="form-control rounded" name="contact" id="contact" type="text" placeholder="" value="{{ $detail['userdetails_contact'] or old('contact') }}" autocomplete="off" required="required">
                                        </div>
                                        <div class="row row-5">
                                            <div class="col-xl-6">
                                                <div class="form-group mb-4">
                                                    <label for="str" class="control-label">Surat Tanda Registrasi (STR)</label>
                                                    <div>
                                                        <div class="animated-checkbox">
                                                            <label>
                                                                <input type="checkbox" name="str" value="1"{{ ($selectedSTR == "1") ? ' checked':'' }}>
                                                                <span class="label-text">Ada</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group mb-4">
                                                    <label for="sip" class="control-label">Surat Izin Praktek (SIP)</label>
                                                    <div>
                                                        <div class="animated-checkbox">
                                                            <label>
                                                                <input type="checkbox" name="sip" value="1"{{ ($selectedSIP == "1") ? ' checked':'' }}>
                                                                <span class="label-text">Ada</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-4">
                                            <label for="field" class="control-label">Spesialisasi<span class="required">*</span></label>
                                            <div class="select2-wrapper block">
                                                <select class="form-control rounded js-select2" name="field" id="field" required="required">
                                                    <option value="">-- Pilih Spesialisasi --</option>
                                                    @foreach($data['fields'] as $val)
                                                        <option value="{{ $val['doctorfields_id'] }}"{{ ($selectedField == $val['doctorfields_id']) ? ' selected':'' }}>{{ $val['doctorfields_name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="role" class="control-label">Role<span class="required">*</span></label>
                                            <div class="select2-wrapper block">
                                                <select class="form-control rounded js-select2" name="role" id="role" required="required">
                                                    <option value="">-- Pilih Role --</option>
                                                    @foreach($data['roles'] as $val)
                                                        <option value="{{ $val['usertypes_id'] }}"{{ ($selectedRole == $val['usertypes_id']) ? ' selected':'' }}>{{ ucfirst($val['usertypes_role']) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="description" class="control-label">Deskripsi</label>
                                            <textarea class="form-control rounded" name="description" id="description" maxlength="250">{{ $detail['userdetails_description'] or old('description') }}</textarea>
                                            <div class="help-block color-grey">Maksimal 250 karakter</div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="sip" class="control-label">Status</label>
                                            <div>
                                                <div class="toggle">
                                                    <label class="cursor-pointer">
                                                        <input type="checkbox" class="js-status" name="status" value="1"{{ $active ? ' checked' : '' }}>
                                                        <span class="button-indecator inline-block v-align-m"></span>
                                                        <span class="txt inline-block v-align-m">Active</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4{{ $active ? ' d-none' : '' }} js-comment">
                                            <label for="comment" class="control-label">Keterangan</label>
                                            <textarea class="form-control rounded" name="comment" id="comment" maxlength="250">{{ $detail['userdetails_comment'] or old('comment') }}</textarea>
                                            <div class="help-block color-grey">Maksimal 250 karakter</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4">
                            <div class="row row-0">
                                <div class="col-xl-10 col-lg-9 col-sm-8"></div>
                                <div class="col-xl-2 col-lg-3 col-sm-4">
                                    <button class="btn btn-lg btn-block btn-outline-primary rounded upper-case" type="submit">{{ ($data['doctor']) ? 'Perbarui' : 'Tambah' }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var parent = $('#pageDoctorForm');
            $('[data-menu="doctor"]').addClass('active');
            $('.js-datePickerCareer', parent).datetimepicker({
                format: 'DD-MM-YYYY',
                maxDate: moment()
            });

            parent.on('change', '.js-profileImageUpload', function() {
                var file = this.files[0];
                reader = new FileReader();
                reader.onloadend = function(e) {
                    var source = e.target.result;
                    $('.avatar', parent).attr('src', source);
                };
                reader.readAsDataURL(file);
            });

            parent.on('click', '.js-status', function () {
                var me = $(this);
                var comment = $('.js-comment', parent);
                if(me.is(':checked')) {
                    comment.addClass('d-none');
                    $('textarea', comment).removeAttr('required');
                } else {
                    var swal_opts = {
                        title: 'Non Aktifkan?',
                        text: 'Apakah anda yakin ingin menon-aktifkan dokter ini?',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-primary rounded',
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Batal',
                    };

                    helper.showDialogModal(swal_opts, function () {
                        comment.removeClass('d-none');
                        $('textarea', comment).attr('required', 'required');
                        me.prop('checked', false);
                        me.removeAttr('checked');
                    });

                    return false;
                }
            });
        });
    </script>
@endsection
