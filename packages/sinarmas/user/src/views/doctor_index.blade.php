@extends('web.layouts.html')

@section('content')
    <div class="telemed page-standard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-4">
                        <div class="telemed-title">Dokter</div>
                    </div>
                    <div class="col-sm-6 text-sm-right text-left">
                        <a class="btn btn-lg btn-orange rounded" href="{{ route('admin.doctor.add') }}">
                            <i class="fa fa-plus mr-1"></i>
                            <span class="txt">Tambah Dokter</span>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="mt-4">
                    <div class="filter-section mb-4">
                        <form class="js-formValidate" name="frmSearch" id="frmSearch" action="{{ route('admin.doctor.search') }}" method="GET">
                            @csrf
                            <div class="row row-10">
                                <div class="col-xl-7">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Cari Dokter</label>
                                        <div class="row row-5">
                                            <div class="col-sm-7 mb-sm-0 mb-3">
                                                <div class="search-wrapper large block">
                                                    <input class="rounded large" type="text" name="q" placeholder=" dokter, spesialis atau nomor telepon" value="{{ isset($_GET['q']) ? $_GET['q']:'' }}" autocomplete="off">
                                                    <i class="fa fa-search"></i>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="row row-5">
                                                    <div class="col-6">
                                                        <button class="btn btn-lg btn-block rounded btn-primary" type="submit">Cari</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button class="btn btn-lg btn-block rounded btn-outline-primary" type="reset" onclick="resetSearch()">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-section">
                        <div class="table-head d-none d-lg-block">
                            <div class="row row-5">
                                <div class="col-md-11 col-sm-9">
                                    <div class="row row-5">
                                        <div class="col-xl-5 col-lg-4">
                                            <div class="th">Detail Dokter</div>
                                        </div>
                                        <div class="col-xl-7 col-lg-8">
                                            <div class="row row-5">
                                                <div class="col-md-4">
                                                    <div class="th">Nomor Handphone</div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="th">Status</div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="th">Terakhir Aktif</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-3"></div>
                            </div>
                        </div>
                        <div class="table-body">
                            @if(count($data['doctors']) > 0)
                                @foreach ($data['doctors'] as $key => $val)
                                    <div class="table-list">
                                        <div class="row row-5">
                                            <div class="col-md-11 col-sm-9">
                                                <div class="row row-5">
                                                    <div class="col-xl-5 col-lg-4">
                                                        <div class="td mb-lg-0 mb-4">
                                                            <div class="row row-10">
                                                                <div class="col-auto">
                                                                    <div class="avatar-profile">
                                                                        <img class="avatar" src="{{ helperGetProfilePicture($val['detail']['userdetails_photo']) }}" alt="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-9 align-self-center">
                                                                    <a href="{{ route('admin.doctor.view', $val['id']) }}">
                                                                        <strong class="color-primary inline-block mb-1">{{ $val['name'] }}</strong>
                                                                    </a>
                                                                    @if ($val['detail']['field']['doctorfields_name'])
                                                                        @if (strtolower($val['detail']['field']['doctorfields_name']) == 'umum')
                                                                            <div>Dokter Umum</div>
                                                                        @else
                                                                            <div>{{ $val['detail']['field']['doctorfields_name'] }}</div>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-7 col-lg-8 align-self-center">
                                                        <div class="row row-5">
                                                            <div class="col-md-4 mb-md-0 mb-2">
                                                                <div class="td-label d-block d-lg-none">Nomor Handphone</div>
                                                                <div class="td">{{ $val['detail']['userdetails_contact'] }}</div>
                                                            </div>
                                                            <div class="col-md-3 mb-md-0 mb-2">
                                                                <div class="td-label d-block d-lg-none">Status</div>
                                                                <div class="td">{{ ($val['is_active']) ? 'Aktif' : 'Non-aktif' }}</div>
                                                            </div>
                                                            <div class="col-md-5 mb-md-0 mb-2">
                                                                <div class="td-label d-block d-lg-none">Terakhir Aktif</div>
                                                                <div class="td">{{ $val['last_login'] }}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-sm-3 text-right mt-md-0 mt-3 align-self-center">
                                                <div class="td no-wrap">
                                                    <a class="mr-2" href="{{ route('admin.doctor.edit', $val['id']) }}" data-toggle="tooltip" title="ubah">
                                                        <i class="icon-pencil mr-1"></i>
                                                    </a>
                                                    <a class="js-globalRemove" data-remove="{{ $val['name'] }}" href="{{ route('admin.doctor.delete').'?doctor_id='.$val['id'] }}" data-toggle="tooltip" title="hapus">
                                                        <i class="icon-garbage mr-1"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="table-list">
                                    <div class="help-block">Tidak ada data</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    @if (count($data['doctors']) > 0)
                        {{ $data['doctors']->appends([ 'q' => $data['query'] ])->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        function resetSearch(){
            location.href = "{{ route('admin.doctor') }}";
        }

        $(document).ready(function () {
            $('[data-menu="doctor"]').addClass('active');
        });
    </script>
@endsection
