@extends('web.layouts.html')

@section('content')
    <div class="telemed page-standard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-4">
                        <div class="telemed-title">Shift Master Data</div>
                    </div>
                    <div class="col-sm-6 text-sm-right text-left">
                        <a class="btn btn-lg btn-orange rounded" href="{{ route('admin.doctor.shift.add') }}">
                            <i class="fa fa-plus mr-1"></i>
                            <span class="txt">Tambah Shift</span>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="mt-4">
                    <div class="filter-section mb-4">
                        <form class="js-formValidate" name="frmSearch" id="frmSearch" action="{{ route('admin.doctor.shift.search') }}" method="GET">
                            @csrf
                            <div class="row row-10">
                                <div class="col-md-7">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Cari Shift</label>
                                        <div class="row row-5">
                                            <div class="col-sm-7 mb-sm-0 mb-3">
                                                <div class="search-wrapper large block">
                                                    <input class="rounded large" type="text" name="q" placeholder="Cari Shift" value="{{ isset($_GET['q']) ? $_GET['q']:'' }}" autocomplete="off">
                                                    <i class="fa fa-search"></i>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="row row-5">
                                                    <div class="col-6">
                                                        <button class="btn btn-lg btn-block rounded btn-primary" type="submit">Cari</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button class="btn btn-lg btn-block rounded btn-outline-primary" type="reset" onclick="resetSearch()">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="row row-10">
                                        <div class="col-6 text-center">
                                            <div class="result inline-block text-center color-primary">
                                                <div class="stats no-wrap">
                                                    <span class="current">{{ $data['countActiveShift'] }}</span>
                                                    <span class="total">/{{ $data['countShifts'] }}</span>
                                                </div>
                                                <div class="desc">Shift Aktif Saat ini</div>
                                            </div>
                                        </div>
                                        {{--<div class="col-6 text-center">
                                            <div class="result inline-block text-left color-grey">
                                                <div class="stats no-wrap">
                                                    <span class="current">4</span>
                                                </div>
                                                <div class="desc">Minimal Shift Aktif</div>
                                            </div>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-section">
                        <div class="table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <td>
                                        <div class="table-head">
                                            <table>
                                                <tr>
                                                    <th class="number"><div class="th">No</div></th>
                                                    <th class="long"><div class="th">Waktu Shift</div></th>
                                                    <th class="medium"><div class="th">Created Date</div></th>
                                                    <th class="medium"><div class="th">Created By</div></th>
                                                    <th class="medium"><div class="th">Last Updated</div></th>
                                                    <th class="medium"><div class="th">Updated By</div></th>
                                                    <th class="medium"><div class="th">Status</div></th>
                                                    <th class="action"><div class="th"></div></th>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($data['shifts']) > 0)
                                    @php $i = $data['shifts']->perPage() * ($data['shifts']->currentPage() - 1); @endphp
                                    @foreach ($data['shifts'] as $key => $val)
                                        @php $i++; @endphp
                                        <tr>
                                            <td>
                                                <div class="table-list">
                                                    <table>
                                                        <tr>
                                                            <td class="number"><div class="td">{{ ($i < 10) ? '0' . $i : $i }}</div></td>
                                                            <td class="long"><div class="td"><a href="{{ route('admin.doctor.shift.edit', $val['doctorshifts_id']) }}"><strong class="color-primary inline-block mb-1">{{ $val['doctorshifts_time'] }}</strong></a></div></td>
                                                            <td class="medium"><div class="td">{{ $val['created_at'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ $val['created_by'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ $val['updated_at'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ $val['updated_by'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ ($val['doctorshifts_active'] == 1) ? 'Aktif' : 'Non-aktif' }}</div></td>
                                                            <td class="action text-right">
                                                                <div class="td no-wrap">
                                                                    <a class="mr-2" href="{{ route('admin.doctor.shift.edit', $val['doctorshifts_id']) }}" data-toggle="tooltip" title="ubah">
                                                                        <i class="icon-pencil mr-1"></i>
                                                                    </a>
                                                                    <a class="js-globalRemove" data-remove="{{ $val['doctorshifts_time'] }}" href="{{ route('admin.doctor.shift.delete').'?shift_id='.$val['doctorshifts_id'] }}" data-toggle="tooltip" title="hapus">
                                                                        <i class="icon-garbage mr-1"></i>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr><td>&nbsp;</td></tr>
                                @else
                                    <tr>
                                        <td colspan="8">
                                            <div class="table-list">
                                                <div class="help-block">Tidak ada data</div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        @if (count($data['shifts']) > 0)
                            {{ $data['shifts']->appends([ 'q' => $data['query'] ])->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        function resetSearch(){
            location.href = "{{ route('admin.doctor.shift') }}";
        }

        $(document).ready(function () {
            $('[data-menu="master"]').addClass('active');
        });
    </script>
@endsection
