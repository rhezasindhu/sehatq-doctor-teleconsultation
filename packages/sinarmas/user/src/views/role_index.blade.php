@extends('web.layouts.html')

@section('content')
    <div class="telemed page-standard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-4">
                        <div class="telemed-title">Master Role</div>
                    </div>
                    <div class="col-sm-6 text-sm-right text-left">
                        <a class="btn btn-lg btn-orange rounded" href="{{ route('admin.role.add') }}">
                            <i class="fa fa-plus mr-1"></i>
                            <span class="txt">Tambah Role</span>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="mt-4">
                    <div class="table-section">
                        <div class="table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <td>
                                        <div class="table-head">
                                            <table>
                                                <tr>
                                                    <th class="number"><div class="th">Id</div></th>
                                                    <th class="long"><div class="th">Role</div></th>
                                                    <th class="medium"><div class="th">Created Date</div></th>
                                                    <th class="medium"><div class="th">Created By</div></th>
                                                    <th class="medium"><div class="th">Last Updated</div></th>
                                                    <th class="medium"><div class="th">Updated By</div></th>
                                                    <th class="medium"><div class="th">Anggota</div></th>
                                                    <th class="action"><div class="th"></div></th>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($data['roles']) > 0)
                                    @foreach ($data['roles'] as $key => $val)
                                        <tr>
                                            <td>
                                                <div class="table-list">
                                                    <table>
                                                        <tr>
                                                            <td class="number"><div class="td">{{ $val['usertypes_id'] }}</div></td>
                                                            <td class="long"><div class="td"><a href="{{ route('admin.role.edit', $val['usertypes_id']) }}"><strong class="color-primary inline-block mb-1">{{ $val['usertypes_role'] }}</strong></a><div class="js-readMore">{{ $val['usertypes_description'] }}</div></div></td>
                                                            <td class="medium"><div class="td">{{ $val['created_at'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ $val['created_by'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ $val['updated_at'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ $val['updated_by'] }}</div></td>
                                                            <td class="medium"><div class="td">{{ count($val['users']) . ' orang' }}</div></td>
                                                            <td class="action text-right">
                                                                <div class="td no-wrap">
                                                                    <a class="mr-2" href="{{ route('admin.role.edit', $val['usertypes_id']) }}" data-toggle="tooltip" title="ubah">
                                                                        <i class="icon-pencil mr-1"></i>
                                                                    </a>
                                                                    <a class="js-globalRemove" data-remove="{{ $val['usertypes_role'] }}" href="{{ route('admin.role.delete').'?role_id='.$val['usertypes_id'] }}" data-toggle="tooltip" title="hapus">
                                                                        <i class="icon-garbage mr-1"></i>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr><td>&nbsp;</td></tr>
                                @else
                                    <tr>
                                        <td colspan="8">
                                            <div class="table-list">
                                                <div class="help-block">Tidak ada data</div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/plugins/readMoreJS' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-menu="master"]').addClass('active');
            $readMoreJS.init({
                target: '.js-readMore',
                numOfWords: 8,
                toggle: false,
                moreLink: '',
                lessLink: '',
                linkClass: '',
                containerClass: ''
            });
        });
    </script>
@endsection