@extends('web.layouts.html')

@section('content')
    <div class="telemed page-standard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-4">
                        <div class="telemed-title">Master Data</div>
                    </div>
                </div>
                <hr>
                <div class="mt-4">
                    <div class="table-section">
                        <div class="table-head d-none d-lg-block">
                            <div class="row row-5">
                                <div class="col-md-11 col-sm-9">
                                    <div class="row row-5">
                                        <div class="col-xl-3 col-lg-4">
                                            <div class="th">Nama Master Data</div>
                                        </div>
                                        <div class="col-xl-9 col-lg-8">
                                            <div class="th">Deskripsi</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-3"></div>
                            </div>
                        </div>
                        <div class="table-body">
                            @if(count($data['masters']) > 0)
                                @foreach ($data['masters'] as $key => $val)
                                    <div class="table-list">
                                        <div class="row row-5">
                                            <div class="col-md-11 col-sm-9">
                                                <div class="row row-5">
                                                    <div class="col-xl-3 col-lg-4">
                                                        <div class="td mb-lg-0 mb-4">
                                                            <a href="{{ $val['linkUrl'] }}" class="opensansBold color-body">{{ $val['name'] }}</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-9 col-lg-8">
                                                        <div class="td-label d-block d-lg-none">Deskripsi</div>
                                                        <div class="td">{{ $val['desc'] }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-sm-3 text-right mt-md-0 mt-3">
                                                <div class="td no-wrap">
                                                    <a class="mr-2" href="{{ $val['linkUrl'] }}" data-toggle="tooltip" title="lihat">
                                                        <i class="fa fa-eye mr-1"></i>
                                                        <span class="txt">Lihat</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="table-list">
                                    <div class="help-block">Tidak ada data</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-menu="master"]').addClass('active');
        });
    </script>
@endsection