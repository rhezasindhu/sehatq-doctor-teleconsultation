@extends('web.layouts.html')

@section('content')
    @php
        if ($data['doctor']) {
            $actionURL = route('admin.doctor.edit', $data['doctor']['id']);
        } else {
            $actionURL = route('admin.doctor.add');
        }
        $doctor = ($data['doctor']) ? $data['doctor']:[];
        $detail = ($data['doctor']) ? $data['doctor']['detail']:[];

        if ($detail) {
            $date_exp = date('d-m-Y', strtotime($detail['userdetails_experience']));
        }

        $selectedField = "";
        if ($detail) {
            $selectedField = $detail['userdetails_doctorfields_id'];
        } elseif (old('field')) {
            $selectedField = old('field');
        }

        $selectedSex = "m";
        if ($detail) {
            $selectedSex = strtolower($detail['userdetails_sex']);
        } elseif (old('sex')) {
            $selectedSex = old('sex');
        }

        $selectedSTR = "1";
        if ($detail) {
            $selectedSTR = $detail['userdetails_str'];
        } elseif (old('str')) {
            $selectedSTR = old('str');
        }

        $selectedSIP = "1";
        if ($detail) {
            $selectedSIP = $detail['userdetails_sip'];
        } elseif (old('sip')) {
            $selectedSIP = old('sip');
        }

        $selectedRole = "";
        if ($doctor) {
            if (old('role')) {
                $selectedRole = old('role');
            } else {
                $selectedRole = $doctor['account_type'];
            }
        }

        $active = 0;
        if ($doctor) {
            $active = $doctor['is_active'];
        }
    @endphp
    <div class="telemed page-standard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="telemed-back mb-4">
                    <a href="javascript:history.go(-1);" class="back">
                        <i class="fa fa-angle-left mr-1"></i>
                        <span class="txt">Kembali ke halaman sebelumnya</span>
                    </a>
                </div>
                <div class="telemed-title">Profil Dokter</div>
                <div class="form-section mt-5">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-5 text-center">
                            <div class="mb-5">
                                <div class="avatar-profile large">
                                    <img class="avatar" src="{{ helperGetProfilePicture($detail['userdetails_photo']) }}" alt="">
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label class="control-label">ID Dokter</label>
                                <div class="form-value"><strong class="font-16">{{ $doctor['id'] }}</strong></div>
                            </div>
                            <hr class="d-block d-sm-none">
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-7">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Title</label>
                                        <div class="form-value">{{ $detail['userdetails_doctortitle'] }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Nama Dokter</label>
                                        <div class="form-value">{{ $doctor['name'] }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Jenis Kelamin</label>
                                        <div class="form-value">{{ ($selectedSex == "m") ? 'Pria' : 'Wanita' }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Berkarir Sejak</label>
                                        <div class="form-value">{{ $date_exp }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Login Email</label>
                                        <div class="form-value">{{ $doctor['email'] }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Nomor Telepon</label>
                                        <div class="form-value">{{ $detail['userdetails_contact'] }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Surat Tanda Registrasi (STR)</label>
                                        <div class="form-value">
                                            {{ ($selectedSIP == "1") ? 'Ada' : 'Tidak ada' }}
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Surat Izin Praktek (SIP)</label>
                                        <div class="form-value">
                                            {{ ($selectedSTR == "1") ? 'Ada' : 'Tidak ada' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Spesialisasi</label>
                                        <div class="form-value">
                                            @foreach($data['fields'] as $val)
                                                {{ ($selectedField == $val['doctorfields_id']) ? $val['doctorfields_name'] : '' }}
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Role</label>
                                        <div class="form-value">
                                            @foreach($data['roles'] as $val)
                                                {{ ($selectedRole == $val['usertypes_id']) ? $val['usertypes_role'] : '' }}
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Deskripsi</label>
                                        <div class="form-value">{{ $detail['userdetails_description'] }}</div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="control-label">Status: {{ ($active) ? 'Aktif':'Non-aktif' }}</label>
                                        <div class="form-value">{{ ($active) ? 'Aktif' : 'Non-aktif' }}</div>
                                    </div>
                                    @if ($detail['userdetails_comment'] != '')
                                        <div class="form-group mb-4">
                                            <label class="control-label">Keterangan</label>
                                            <div class="form-value">{{ $detail['userdetails_comment'] }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <div class="row row-0">
                            <div class="col-xl-10 col-lg-9 col-sm-8"></div>
                            <div class="col-xl-2 col-lg-3 col-sm-4">
                                <a href="{{ route('admin.doctor.edit', $doctor['id']) }}" class="btn btn-lg btn-block btn-outline-primary rounded upper-case">Ubah</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-menu="doctor"]').addClass('active');
        });
    </script>
@endsection
