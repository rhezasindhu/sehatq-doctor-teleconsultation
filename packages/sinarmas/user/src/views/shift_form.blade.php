@extends('web.layouts.html')

@section('content')
    @php
        if ($data['shift']) {
            $actionURL = route('admin.doctor.shift.edit', $data['shift']['doctorshifts_id']);
            $confirmationTitle = 'Ubah Shift?';
            $confirmationMessage = 'Apakah anda yakin dengan perubahan shift ini?';
        } else {
            $actionURL = route('admin.doctor.shift.add');
            $confirmationTitle = 'Tambah Shift?';
            $confirmationMessage = 'Apakah anda yakin dengan penambahan shift ini?';
        }

        $active = 1;
        if ($data['shift']) {
            $active = $data['shift']['doctorshifts_active'];
        }
    @endphp
    <div class="telemed page-standard" id="pageShift">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="telemed-back mb-4">
                    <a href="javascript:history.go(-1);" class="back">
                        <i class="fa fa-angle-left mr-1"></i>
                        <span class="txt">Kembali ke halaman sebelumnya</span>
                    </a>
                </div>
                <div class="telemed-title">{{ ($data['shift']) ? 'Ubah' : 'Tambah' }} Shift</div>
                <div class="form-section mt-5">
                    <form class="js-formValidate js-telemedForm" action="{{ $actionURL }}" data-confirmation-title="{{ $confirmationTitle }}" data-confirmation-message="{{ $confirmationMessage }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-xl-6 col-lg-8 col-md-10">
                                <div class="row row-10">
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label for="start" class="control-label">Mulai</label>
                                            <input class="form-control rounded{{ $errors->has('start') ? ' error' : '' }} js-datePickerShiftStart" name="start" id="start" type="text" placeholder="" value="{{ $data['shift']['doctorshifts_time_start'] or old('start') }}" required="required" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label for="end" class="control-label">Selesai</label>
                                            <input class="form-control rounded{{ $errors->has('end') ? ' error' : '' }} js-datePickerShiftEnd" name="end" id="end" type="text" placeholder="" value="{{ $data['shift']['doctorshifts_time_end'] or old('end') }}" required="required" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="form-group mb-4">
                                    <label for="name" class="control-label">Waktu</label>
                                    <input class="form-control rounded{{ $errors->has('name') ? ' error' : '' }}" name="name" id="name" type="text" placeholder="" value="{{ $data['shift']['doctorshifts_time'] or old('name') }}" required="required" autocomplete="off" autofocus>

                                    @if ($errors->has('name'))
                                        <label class="error">{{ $errors->first('name') }}</label>
                                    @endif
                                </div>--}}
                                <div class="form-group mb-4">
                                    <label for="sip" class="control-label">Status</label>
                                    <div>
                                        <div class="toggle">
                                            <label class="cursor-pointer">
                                                <input type="checkbox" name="active" value="1"{{ $active ? ' checked' : '' }}>
                                                <span class="button-indecator inline-block v-align-m"></span>
                                                <span class="txt inline-block v-align-m">Active</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <div class="row row-0">
                                        <div class="col-xl-9 col-lg-8 col-sm-7"></div>
                                        <div class="col-xl-3 col-lg-4 col-sm-5">
                                            <input type="hidden" name="created_by" value="{{ $data['shift']['created_by'] or old('created_by') }}">
                                            <button class="btn btn-lg btn-block btn-outline-primary rounded upper-case" type="submit">{{ ($data['shift']) ? 'Perbarui' : 'Tambah' }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-menu="master"]').addClass('active');
            var parent = $('#pageShift');
            $('.js-datePickerShiftStart', parent).datetimepicker({
                format: 'hh:mm'
            });
            $('.js-datePickerShiftEnd', parent).datetimepicker({
                format: 'hh:mm'
            });
        });
    </script>
@endsection
