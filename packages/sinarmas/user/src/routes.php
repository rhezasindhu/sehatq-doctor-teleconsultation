<?php
Route::prefix('admin')->group(function() {
    Route::get('/role/delete', 'Sinarmas\User\RoleController@delete')->name('admin.role.delete');
    Route::any('/role/edit/{id}', 'Sinarmas\User\RoleController@form')->name('admin.role.edit');
    Route::any('/role/add', 'Sinarmas\User\RoleController@form')->name('admin.role.add');
    Route::get('/role', 'Sinarmas\User\RoleController@index')->name('admin.role');

    Route::get('/doctor/shift/search', 'Sinarmas\User\ShiftController@search')->name('admin.doctor.shift.search');
    Route::get('/doctor/shift/delete', 'Sinarmas\User\ShiftController@delete')->name('admin.doctor.shift.delete');
    Route::any('/doctor/shift/edit/{id}', 'Sinarmas\User\ShiftController@form')->name('admin.doctor.shift.edit');
    Route::any('/doctor/shift/add', 'Sinarmas\User\ShiftController@form')->name('admin.doctor.shift.add');
    Route::get('/doctor/shift', 'Sinarmas\User\ShiftController@index')->name('admin.doctor.shift');

    Route::get('/doctor/field/search', 'Sinarmas\User\FieldController@search')->name('admin.doctor.field.search');
    Route::get('/doctor/field/delete', 'Sinarmas\User\FieldController@delete')->name('admin.doctor.field.delete');
    Route::any('/doctor/field/edit/{id}', 'Sinarmas\User\FieldController@form')->name('admin.doctor.field.edit');
    Route::any('/doctor/field/add', 'Sinarmas\User\FieldController@form')->name('admin.doctor.field.add');
    Route::get('/doctor/field', 'Sinarmas\User\FieldController@index')->name('admin.doctor.field');

    Route::get('/doctor/search', 'Sinarmas\User\UserController@search')->name('admin.doctor.search');
    Route::get('/doctor/delete', 'Sinarmas\User\UserController@delete')->name('admin.doctor.delete');
    Route::any('/doctor/edit/{id}', 'Sinarmas\User\UserController@form')->name('admin.doctor.edit');
    Route::any('/doctor/view/{id}', 'Sinarmas\User\UserController@view')->name('admin.doctor.view');
    Route::any('/doctor/add', 'Sinarmas\User\UserController@form')->name('admin.doctor.add');
    Route::get('/doctor', 'Sinarmas\User\UserController@index')->name('admin.doctor');

    Route::get('/', 'Sinarmas\User\UserController@admin')->name('admin');
});
