<?php
namespace Sinarmas\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\Usertype;
use App\Models\Doctorfield;

class UserController extends Controller {
    public function __construct() {
        $this->middleware(['web', 'auth:admin']);
    }

    public function searchEngine($q = "") {
        return helperMasterDoctor('search', $q);
    }

    public function index() {
        $data = [
            'doctors' => self::searchEngine(),
            'query'   => '',
        ];
        //dd($data);
        return view('user::doctor_index', ['data' => $data]);
    }

    public function search(Request $request) {
        if ($request->method() === 'GET') {
            $data = [
                'doctors' => self::searchEngine($request->q),
                'query'   => $request->q,
            ];
            return view('user::doctor_index', ['data' => $data]);
        }
        return redirect()->back();
    }

    public function admin() {
        $data = [
            'masters' => [
                [
                    'name' => 'Data Shift',
                    'desc' => 'Module master dokter shift',
                    'linkUrl' => route('admin.doctor.shift')
                ],
                [
                    'name' => 'Data Spesialist',
                    'desc' => 'Module master spesialisasi dokter',
                    'linkUrl' => route('admin.doctor.field')
                ],
                [
                    'name' => 'Data Role',
                    'desc' => 'Module master role',
                    'linkUrl' => route('admin.role')
                ],
            ]
        ];
        return view('user::admin', ['data' => $data]);
    }

    public function view(Request $request, $id = null) {
        //-- Load Page
        $fields = Doctorfield::all();
        $roles  = Usertype::all();
        if ($id) {
            $doctor = User::default()->findOrFail($id);
        } else {
            $doctor = [];
        }
        $data = [
            'doctor' => $doctor,
            'fields' => $fields,
            'roles'  => $roles,
        ];
        return view('user::doctor_view', ['data' => $data]);
    }

    public function form(Request $request, $id = null) {
        if ($request->method() === 'POST') {
            //-- Validation
            $validatedData = $request->validate([
                'name'     => 'required',
                'email'    => ($id) ? 'required|email' : 'required|email|unique:users,email' ,
                'password' => ($id && !$request->password) ? '' : 'required|min:8',
                'contact'  => 'required'
            ]);

            //-- Save Master Data
            $doctor = User::updateOrCreate([
                'id' => $id
            ],[
                'name'         => $request->name,
                'email'        => $request->email,
                'account_type' => $request->role,
                'is_admin'     => 1,
                'is_active'    => ($request->status) ? '1':'0',
            ]);
            //-- Update password, if input password not empty
            if ($request->password) {
                $doctor->password = Hash::make($request->password);
                $doctor->save();
            }
            //-- Save Slave Data
            //-- UPLOAD PROFILE IMG
            $filename = null;
            if ($request->hasFile('file')) {
                $file            = $request->file('file');
                $filename        = str_slug($request->name).'_'.time().'.'.$file->getClientOriginalExtension();
                $destination     = helperPathUpload('/profile')."/".$filename;
                $file->move(helperPathUpload('/profile'), $filename);
                helperImageCompress($destination, $destination, '80'); /* Helper */
            }
            //-- Mode = Edit --> Remove Old Photo Profile
            if ($id && $filename !== null) {
                $doctorDetail = Userdetail::where('userdetails_users_id', $id)->first();
                if ($doctorDetail->userdetails_photo) {
                    unlink(helperPathUpload('/profile')."/".$doctorDetail->userdetails_photo);
                }
            }

            $doctorDetail = Userdetail::updateOrCreate([
                'userdetails_users_id' => $id
            ],[
                'userdetails_users_id'        => $doctor->id,
                'userdetails_doctorfields_id' => $request->field,
                'userdetails_doctortitle'     => $request->title,
                'userdetails_description'     => $request->description,
                'userdetails_experience'      => date('Y-m-d H:i:s', strtotime($request->date_exp)),
                'userdetails_contact'         => $request->contact,
                'userdetails_str'             => ($request->str) ? '1' : '0',
                'userdetails_sip'             => ($request->sip) ? '1' : '0',
                'userdetails_comment'         => $request->comment,
                'userdetails_sex'             => ($request->sex) ? $request->sex : 'm'
            ]);

            if ($filename !== null) {
                $doctorDetail->userdetails_photo = $filename;
            }

            //-- Message
            if ($doctorDetail->save()) {
                $request->session()->flash('alert-success', ['Data dokter telah tersimpan']);
            } else {
                $request->session()->flash('alert-error', ['Data dokter gagal tersimpan']);
            }

            //-- Redirect
            return redirect()->route('admin.doctor');
        }
        //-- Load Page
        $fields = Doctorfield::all();
        $roles  = Usertype::general()->get();
        if ($id) {
            $doctor = User::default()->findOrFail($id);
        } else {
            $doctor = [];
        }

        $data = [
            'doctor' => $doctor,
            'fields' => $fields,
            'roles'  => $roles,
        ];
        return view('user::doctor_form', ['data' => $data]);
    }

    public function delete(Request $request) {
        if ($request->method() === 'GET') {
            if ($request->doctor_id) {
                $id = $request->doctor_id;
                $doctor = User::find($id);
                $doctor->shifts()->detach();    //-- Clear all shift that reference to that doctor
                $doctor->delete();
                Userdetail::where('userdetails_users_id', $id)->delete();
                $request->session()->flash('alert-success', ['Data dokter telah dihapus']);
            }
            return redirect()->back();
        }
        return redirect()->back();
    }
}
