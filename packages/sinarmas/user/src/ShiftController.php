<?php
namespace Sinarmas\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Doctorshift;

class ShiftController extends Controller {
    public function __construct() {
        $this->middleware(['web', 'auth:admin']);
    }

    public function searchEngine($q = "") {
        return helperMasterDoctorShift('search', $q);
    }

    public function index() {
        $data = [
            'shifts'           => self::searchEngine(),
            'query'            => '',
            'countShifts'      => count(helperMasterDoctorShift()),
            'countActiveShift' => count(helperMasterDoctorShift('active')),
        ];
        return view('user::shift_index', ['data' => $data]);
    }

    public function search(Request $request) {
        if ($request->method() === 'GET') {
            $data = [
                'shifts'           => self::searchEngine($request->q),
                'query'            => $request->q,
                'countShifts'      => count(helperMasterDoctorShift()),
                'countActiveShift' => count(helperMasterDoctorShift('active')),
            ];
            return view('user::shift_index', ['data' => $data]);
        }
        return redirect()->back();
    }

    public function form(Request $request, $id = null) {
        if ($request->method() === 'POST') {
            //-- Validation
            $validatedData = $request->validate([
                'start' => 'required',
                'end'   => 'required',
            ]);
            //-- Process
            $shift = Doctorshift::updateOrCreate([
                'doctorshifts_id' => $id
            ],[
                'doctorshifts_time_start' => $request->start,
                'doctorshifts_time_end'   => $request->end,
                'doctorshifts_time'       => $request->start." - ".$request->end,
                'doctorshifts_active'     => ($request->active) ? '1':'0',
                'created_by'              => (!$id) ? Auth::user()->name : $request->created_by,
                'updated_by'              => ($id) ? Auth::user()->name : '',
            ]);
            //-- Message
            if ($shift->wasRecentlyCreated || $shift->wasChanged()) {
                $request->session()->flash('alert-success', ['Jadwal shift dokter telah tersimpan']);
            } else {
                $request->session()->flash('alert-error', ['Jadwal shift dokter gagal tersimpan']);
            }
            return redirect()->route('admin.doctor.shift');
        }

        $data = [
            'shift' => ($id) ? Doctorshift::findOrFail($id) : [],
        ];
        return view('user::shift_form', ['data' => $data]);
    }

    public function delete(Request $request) {
        if ($request->method() === 'GET') {
            if ($request->shift_id) {
                $id = $request->shift_id;
                $doctorshift = Doctorshift::find($id);
                $doctorshift->shifts()->detach();    //-- Clear all shift that reference to that doctorshift
                $doctorshift->delete();
                $request->session()->flash('alert-success', ['Jadwal shift dokter telah dihapus']);
            }
            return redirect()->back();
        }
        return redirect()->back();
    }
}
