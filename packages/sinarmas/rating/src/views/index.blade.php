@extends('web.layouts.html')

@section('content')
    <div class="telemed page-rating">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-loading js-telemedLoading">
                <img src="{{ asset(config('_customs.loading_icon')) }}" alt="">
            </div>
            <div class="telemed-wrapper">
                Rating page
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-rating.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script>
        $(document).ready(function () {
            $('[data-menu="rating"]').addClass('active');
        });
    </script>
@endsection