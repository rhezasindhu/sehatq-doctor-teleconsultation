<?php

namespace Sinarmas\Rating;

use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth:admin']);
    }

    public function index()
    {
        $data = [];

        return view('rating::index', ['data' => $data]);
    }
}