<?php

namespace Sinarmas\Rating;

use Illuminate\Support\ServiceProvider;

class RatingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__ . '/routes.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Sinarmas\Rating\RatingController');
        $this->loadViewsFrom(__DIR__ . '/views', 'rating');
    }
}