<?php

namespace Sinarmas\Schedule;

use Illuminate\Support\ServiceProvider;

class ScheduleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__ . '/routes.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Sinarmas\Schedule\ScheduleController');
        $this->loadViewsFrom(__DIR__ . '/views', 'schedule');
    }
}