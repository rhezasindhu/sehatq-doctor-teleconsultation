<?php
Route::prefix('schedule')->group(function() {
    Route::any('/delete', 'Sinarmas\Schedule\ScheduleController@deleteSchedule')->name('schedule.delete');
    Route::any('/save', 'Sinarmas\Schedule\ScheduleController@saveSchedule')->name('schedule.save');
    Route::get('/daily', 'Sinarmas\Schedule\ScheduleController@getDailySchedule')->name('schedule.daily');
    Route::get('/doctor-info', 'Sinarmas\Schedule\ScheduleController@getDoctorInfo')->name('schedule.doctor_info');
    Route::get('/', 'Sinarmas\Schedule\ScheduleController@index')->name('schedule');
});