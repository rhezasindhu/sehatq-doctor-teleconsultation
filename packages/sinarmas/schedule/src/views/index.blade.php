@extends('web.layouts.html')

@section('content')
    <div class="telemed page-schedule" id="pageSchedule">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-4">
                        <div class="telemed-title">Jadwal Dokter</div>
                    </div>
                    <div class="col-sm-6 text-sm-right text-left">
                        <button class="btn btn-lg btn-orange rounded js-showAssignBox" type="button">ASSIGN DOKTER</button>
                    </div>
                </div>
                <div class="summary-wrapper">
                    <div class="row mt-sm-5 mt-3">
                        @if(!empty($data['availability']))
                            <div class="col-md-6 mb-4">
                                <div class="box-summary">
                                    <div class="row">
                                        <div class="col-xl-3 col-lg-4 align-self-center text-lg-left text-center mb-lg-0 mb-3 no-wrap">
                                            <span class="current">{{ $data['availability']['current'] }}</span>
                                            <span class="total">/{{ $data['availability']['total'] }}</span>
                                        </div>
                                        <div class="col-xl-9 col-lg-8 align-self-center text-lg-right text-center">
                                            <h3>Dokter tersedia untuk shift saat ini</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($data['nextAvailability']))
                            <div class="col-md-6 mb-4">
                                <div class="box-summary next">
                                    <div class="row">
                                        <div class="col-xl-3 col-lg-4 align-self-center text-lg-left text-center mb-lg-0 mb-3 no-wrap">
                                            <span class="current">{{ $data['nextAvailability']['current'] }}</span>
                                            <span class="total">/{{ $data['nextAvailability']['total'] }}</span>
                                        </div>
                                        <div class="col-xl-9 col-lg-8 align-self-center text-lg-right text-center">
                                            <h3>Dokter tersedia untuk shift berikutnya</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="schedule-wrapper">
                    <div class="tab-custom tab-schedule">
                        <div class="row">
                            <div class="col-lg-4 col-md-3 col-sm-2"></div>
                            <div class="col-lg-4 col-md-6 col-sm-8">
                                <ul class="nav nav-tabs row row-0">
                                    <li class="col-6 nav-item">
                                        <a class="nav-link text-center no-wrap active" data-toggle="tab" href="#daily">Daily</a>
                                    </li>
                                    <li class="col-6 nav-item">
                                        <a class="nav-link text-center" data-toggle="tab" href="#monthly">Monthly</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="daily">
                                <div class="tab-wrapper p-0">
                                    <div class="schedule-daily-box">
                                        <div class="head-nav">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-2"></div>
                                                <div class="col-lg-4 col-md-6 col-sm-8">
                                                    <div class="head-date-wrapper">
                                                        <button class="btn btn-link btn-nav left js-changeDailySchedule" data-type="prev" type="button" data-toggle="tooltip" title="Sebelumnya">
                                                            <i class="fa fa-chevron-left"></i>
                                                        </button>
                                                        <button class="btn btn-link btn-nav right js-changeDailySchedule" data-type="next" type="button" data-toggle="tooltip" title="Selanjutnya">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </button>
                                                        <div class="date-wrapper">
                                                            <input class="js-datePickerDaily" type="text" name="" placeholder="" value="{{ date('d-m-Y') }}" autocomplete="off" readonly="readonly">
                                                            <i class="icon-calendar cursor-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="shift-head">
                                            <div class="row row-0">
                                                <div class="col-xl-2 col-lg-3 col-md-4">
                                                    <div class="shift-wrap border-0 text-center">Shift</div>
                                                </div>
                                                <div class="col-xl-10 col-lg-9 col-md-8">
                                                    <div class="row row-0">
                                                        <div class="col-xl-10 col-lg-9 col-md-7">
                                                            <div class="shift-wrap border-0">Nama Dokter</div>
                                                        </div>
                                                        <div class="col-xl-2 col-lg-3 col-md-5">
                                                            <div class="shift-wrap border-0 border-0">Status</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="shift-body js-dailyScheduleWrapper">
                                            <div class="telemed-loading white">
                                                <img src="{{ asset(config('_customs.loading_icon')) }}" alt="">
                                            </div>
                                            {{--<div class="schedule-list">
                                                <div class="row row-0">
                                                    <div class="col-xl-2 col-lg-3 col-md-4">
                                                        <div class="shift-wrap text-center h-100 d-block d-md-flex align-items-center justify-content-center">
                                                            <span class="shift-time">06.00 - 11.00</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-9 col-md-8">
                                                        <div class="schedule-doctor-list">
                                                            <div class="row row-0">
                                                                <div class="col-xl-10 col-lg-9 col-md-7 col-sm-8 col-8">
                                                                    <div class="shift-wrap h-100 pl-0 pr-0">
                                                                        <ol class="list-unstyled doctors">
                                                                            <li>
                                                                                <div class="row row-5">
                                                                                    <div class="col-11">
                                                                                        <a href="javascript:void(0);" class="name ellipsis v-align-m" data-id="1">Adityana, Sp.PD</a>
                                                                                    </div>
                                                                                    <div class="col-1 text-right">
                                                                                        <a href="javascript:void(0);" class="remove" data-id="1">
                                                                                            <i class="fa fa-trash-alt"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="row row-5">
                                                                                    <div class="col-11">
                                                                                        <a href="javascript:void(0);" class="name ellipsis v-align-m" data-id="1">Adityana, Sp.PD</a>
                                                                                    </div>
                                                                                    <div class="col-1 text-right">
                                                                                        <a href="javascript:void(0);" class="remove" data-id="1">
                                                                                            <i class="fa fa-trash-alt"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="row row-5">
                                                                                    <div class="col-11">
                                                                                        <a href="javascript:void(0);" class="name ellipsis v-align-m" data-id="1">Adityana, Sp.PD</a>
                                                                                    </div>
                                                                                    <div class="col-1 text-right">
                                                                                        <a href="javascript:void(0);" class="remove" data-id="1">
                                                                                            <i class="fa fa-trash-alt"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="row row-5">
                                                                                    <div class="col-11">
                                                                                        <a href="javascript:void(0);" class="name ellipsis v-align-m" data-id="1">Adityana, Sp.PD</a>
                                                                                    </div>
                                                                                    <div class="col-1 text-right">
                                                                                        <a href="javascript:void(0);" class="remove" data-id="1">
                                                                                            <i class="fa fa-trash-alt"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ol>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-2 col-lg-3 col-md-5 col-sm-4 col-4">
                                                                    <div class="shift-wrap h-100 pl-0 pr-0">
                                                                        <ol class="list-unstyled doctors">
                                                                            <li><span class="status active">Active</span></li>
                                                                            <li><span class="status busy">Busy</span></li>
                                                                            <li><span class="status inactive">Inactive</span></li>
                                                                            <li><span class="status inactive">Inactive</span></li>
                                                                        </ol>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="monthly">
                                <div class="tab-wrapper">
                                    monthly
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 js-doctorOverall"></div>
            </div>
        </div>
    </div>
@endsection

@section('popup')
    @include('web.modals.schedule_assign_doctor')
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-schedule.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        var api_daily_schedule = '{{ route('schedule.daily') }}',
            api_delete_schedule = '{{ route('schedule.delete') }}',
            api_doctor_info = '{{ route('schedule.doctor_info') }}';
    </script>
    <script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/page-schedule' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
    <script>
        $(document).ready(function () {
            $('[data-menu="schedule"]').addClass('active');
        });
    </script>
@endsection