<?php

namespace Sinarmas\Schedule;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth:admin']);
    }

    public function index()
    {
        $data = [
            'availability' => [
                'current' => 72,
                'total' => 80,
            ],
            'nextAvailability' => [
                'current' => 44,
                'total' => 80,
            ],
            'listDoctors' => helperMasterDoctor(),
            'listShifts'  => helperMasterDoctorShift('active'),
        ];

        return view('schedule::index', ['data' => $data]);
    }

    public function saveSchedule(Request $request)
    {
        if (($request->doctor_id) && ($request->shift_id)) {
            $date = date('Y-m-d H:i:s', strtotime($request->date));
            $doctor = User::find($request->doctor_id);
            $doctor->shifts()->attach($request->shift_id, [
                'date' => $date
            ]);
            $request->session()->flash('alert-success', ['Jadwal shift dokter telah tersimpan']);
        } else {
            $request->session()->flash('alert-danger', ['Jadwal shift dokter gagal disimpan']);
        }
        return redirect()->back();
    }

    public function deleteSchedule(Request $request)
    {
        if (($request->doctorId) && ($request->shiftId)) {
            $doctor = User::find($request->doctorId);
            $doctor->shifts()->detach($request->shiftId);
            $request->session()->flash('alert-success', ['Jadwal shift dokter telah dihapus']);
        }
        return redirect()->back();
    }

    public function getDailySchedule(Request $request)
    {
        $date         = date('Y-m-d', strtotime($request->date));
        $shifts       = helperMasterDoctorShift('active');
        $doctorShifts = helperMasterDoctorShift('join')->where('date', $date);
        $result       = [];
        foreach ($shifts as $key => $shift) {
            $doctors = [];
            $shiftId = $shift->doctorshifts_id;
            $isAnyDoctorSchedule = 0;
            foreach ($doctorShifts as $key2 => $doctorShift) {
                if ($doctorShift->shiftId == $shiftId) {
                    $isAnyDoctorSchedule = 1;
                    $doctors[] = [
                        'id'       => $doctorShift->id,
                        'doctorId' => $doctorShift->userId,
                        'shiftId'  => $doctorShift->shiftId,
                        'name'     => $doctorShift->name,
                        'status'   => ($doctorShift->api_token) ? 'online' : 'offline',
                    ];
                }
            }
            if ($isAnyDoctorSchedule == 0) {
                $doctors[] = [
                    'id'       => '',
                    'doctorId' => '',
                    'shiftId'  => '',
                    'name'     => '-',
                    'status'   => 'empty',
                ];
            }
            $result[$key]['shiftTime'] = $shift->doctorshifts_time;
            $result[$key]['doctors']   = $doctors;
        }

        /*$data = [
            [
                'shiftTime' => '06.00 - 10.00',
                'doctors' => [
                    ['id' => 1, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 2, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 3, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                    ['id' => 4, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                ],
            ],
            [
                'shiftTime' => '06.00 - 10.00',
                'doctors' => [
                    ['id' => 5, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 6, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 7, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                ],
            ],
            [
                'shiftTime' => '06.00 - 10.00',
                'doctors' => [
                    ['id' => 8, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 9, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 10, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                    ['id' => 11, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 12, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 13, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                ],
            ],
            [
                'shiftTime' => '06.00 - 10.00',
                'doctors' => [
                    ['id' => 1, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 2, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 3, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                    ['id' => 4, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                ],
            ],
            [
                'shiftTime' => '06.00 - 10.00',
                'doctors' => [
                    ['id' => 5, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 6, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 7, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                ],
            ],
            [
                'shiftTime' => '06.00 - 10.00',
                'doctors' => [
                    ['id' => 8, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 9, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 10, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                    ['id' => 11, 'name' => 'Adityana, Sp.PD', 'status' => 'active'],
                    ['id' => 12, 'name' => 'Adityana, Sp.PD', 'status' => 'inactive'],
                    ['id' => 13, 'name' => 'Adityana, Sp.PD', 'status' => 'busy'],
                ],
            ],
        ];*/

        $response = [
            'statusCode' => 200,
            'status' => 'success',
            'message' => 'Data found',
            'data' => $result,
        ];

        return response()->json($response, 200);
    }

    public function getDoctorInfo(Request $request)
    {
        $doctor   = User::roleDoctor()->findOrFail($request->id);
        $fileURL  = asset('public/uploads/profile')."/".$doctor->detail->userdetails_photo;
        $rating   = round($doctor->detail->userdetails_rating * 2) / 2;
        $whole    = floor($rating);
        $fraction = $rating - $whole;
        $starHtml = '';
        for($i = 0; $i < $whole; $i++) {
            $starHtml .= '<i class="fa fa-star fa-star checked" aria-hidden="true"></i>';
        }
        if ($fraction > 0) {
            $starHtml .= '<i class="fa fa-star fa-star-half checked" aria-hidden="true"></i>';
        }

        $data = [
            'id' => $doctor->id,
            'name' => $doctor->name,
            'imageUrl' => (!empty($fileURL)) ? $fileURL : asset(config('_customs.assets.path_img') . '/avatar-default.png'),
            'specialization' => ($doctor->detail->field) ? $doctor->detail->field->doctorfields_name:'',
            'phone' => $doctor->detail->userdetails_contact,
            'rating' => [
                'score' => $rating,
                'max'   => 5,
                'total' => $doctor->detail->userdetails_feedback_count,
                'stars' => $starHtml,
            ],
            'detailUrl' => route('admin.doctor.view', $doctor->id),
            'totalPatient' => $doctor->detail->userdetails_feedback_count,
        ];

        return view('web.blocks.schedule_doctor_info', ['data' => $data]);;
    }
}