<?php
Route::prefix('ticket')->group(function() {
    Route::get('/{id}', 'Sinarmas\Ticket\TicketController@detail')->name('ticket.detail');
    Route::get('/', 'Sinarmas\Ticket\TicketController@index')->name('ticket');
});