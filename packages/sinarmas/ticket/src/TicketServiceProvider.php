<?php

namespace Sinarmas\Ticket;

use Illuminate\Support\ServiceProvider;

class TicketServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__ . '/routes.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Sinarmas\Ticket\TicketController');
        $this->loadViewsFrom(__DIR__ . '/views', 'ticket');
    }
}