<?php

namespace Sinarmas\Ticket;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth:admin']);
    }

    public function index()
    {
        $data = [];

        return view('ticket::index', ['data' => $data]);
    }

    public function detail(Request $request, $id = 0)
    {
        $data = [];

        return view('ticket::detail', ['data' => $data]);
    }
}