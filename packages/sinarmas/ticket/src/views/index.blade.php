@extends('web.layouts.html')

@section('content')
    <div class="telemed page-standard" id="pageTicket">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="row">
                    <div class="col-sm-6 mb-sm-0 mb-4">
                        <div class="telemed-title">Daftar Tiket</div>
                    </div>
                    <div class="col-sm-6">
                        <form class="js-formValidate" name="formSearch" id="formSearch" action="#" method="GET">
                            @csrf
                            <div class="form-group">
                                <div class="search-wrapper block">
                                    <input class="rounded" type="text" name="q" placeholder="Cari Nomor Ticket" value="" autocomplete="off">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="filter-section mb-4">
                        <form class="js-formValidate" name="formFilter" id="formFilter" action="#" method="GET">
                            @csrf
                            <div class="row row-5">
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Rating</label>
                                        <div class="select2-wrapper block">
                                            <select class="form-control rounded js-select2" name="rating" id="rating">
                                                <option value="5 - 1">5 - 1</option>
                                                <option value="1 - 5">1 - 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Status</label>
                                        <div class="select2-wrapper block">
                                            <select class="form-control rounded js-select2" name="status" id="status">
                                                <option value="QC">QC</option>
                                                <option value="Not QC">Not QC</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Durasi</label>
                                        <div class="select2-wrapper block">
                                            <select class="form-control rounded js-select2" name="duration" id="duration">
                                                <option value="faster">Lama ke Cepat</option>
                                                <option value="slower">Cepat ke Lama</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">Dari Tanggal</label>
                                        <div class="date-wrapper block">
                                            <input class="rounded js-datePickerStartDate" type="text" name="start_date" id="start_date" placeholder="DD-MM-YYYY" value="" autocomplete="off">
                                            <i class="icon-calendar cursor-pointer"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="form-group mb-2">
                                        <label class="control-label">Sampai Tanggal</label>
                                        <div class="date-wrapper block">
                                            <input class="rounded js-datePickerEndDate" type="text" name="end_date" id="end_date" placeholder="DD-MM-YYYY" value="" autocomplete="off">
                                            <i class="icon-calendar cursor-pointer"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-6">
                                    <div class="form-group mb-4">
                                        <label class="control-label">&nbsp;</label>
                                        <button class="btn btn-block rounded btn-primary" type="reset">
                                            <span class="txt upper-case">Filter</span>
                                            <i class="fa fa-search ml-2"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-section">
                        <div class="table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <td>
                                        <div class="table-head">
                                            <table>
                                                <tr>
                                                    <th class="medium"><div class="th">Nomor Tiket</div></th>
                                                    <th class="long"><div class="th">Nama Dokter</div></th>
                                                    <th class="medium"><div class="th">Rating</div></th>
                                                    <th class="long"><div class="th">Tanggal</div></th>
                                                    <th class="short"><div class="th">Durasi</div></th>
                                                    <th class="short"><div class="th">Status</div></th>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="table-list">
                                            <table>
                                                <tr>
                                                    <td class="medium"><div class="td"><a href="{{ route('ticket.detail', 1) }}"><strong class="color-primary inline-block">TIX-CONV-01-014-03</strong></a></div></td>
                                                    <td class="long"><div class="td"><a href="#">dr. Adam Bachtiar Satibi, Sp.OG</a></div></td>
                                                    <td class="medium">
                                                        <div class="td">
                                                            <div class="rating-wrap">
                                                                <div class="stars">
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="long"><div class="td">Selasa, 14 Juli 2019 08:00</div></td>
                                                    <td class="short"><div class="td">14:29</div></td>
                                                    <td class="short"><div class="td"><strong class="upper-case color-green-light">QC</strong></div></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script>
        $(document).ready(function () {
            var parent = $('#pageTicket');
            $('[data-menu="ticket"]').addClass('active');
            $('.js-datePickerStartDate', parent).datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $('.js-datePickerEndDate', parent).datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });
    </script>
@endsection