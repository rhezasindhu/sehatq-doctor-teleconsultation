@extends('web.layouts.html')

@section('content')
    <div class="telemed page-standard page-ticket" id="pageTicketDetail">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                <div class="telemed-back mb-4">
                    <a href="javascript:history.go(-1);" class="back">
                        <i class="fa fa-angle-left mr-1"></i>
                        <span class="txt">Kembali ke halaman sebelumnya</span>
                    </a>
                </div>
                <div class="telemed-title">Informasi Tiket</div>
                <div class="mt-4">
                    <div class="table-section">
                        <div class="table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <td>
                                        <div class="table-head">
                                            <table>
                                                <tr>
                                                    <th class="medium"><div class="th">Nomor Tiket</div></th>
                                                    <th class="long"><div class="th">Nama Dokter</div></th>
                                                    <th class="medium"><div class="th">Rating</div></th>
                                                    <th class="long"><div class="th">Tanggal</div></th>
                                                    <th class="short"><div class="th">Durasi</div></th>
                                                    <th class="short"><div class="th">Status</div></th>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="table-list">
                                            <table>
                                                <tr>
                                                    <td class="medium"><div class="td"><a href="#"><strong class="color-primary inline-block">TIX-CONV-01-014-03</strong></a></div></td>
                                                    <td class="long"><div class="td"><a href="#">dr. Adam Bachtiar Satibi, Sp.OG</a></div></td>
                                                    <td class="medium">
                                                        <div class="td">
                                                            <div class="rating-wrap">
                                                                <div class="stars">
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                    <i class="fa fa-star fa-star checked" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="long"><div class="td">Selasa, 14 Juli 2019 08:00</div></td>
                                                    <td class="short"><div class="td">14:29</div></td>
                                                    <td class="short"><div class="td"><strong class="upper-case color-green-light">QC</strong></div></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-standard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script>
        $(document).ready(function () {
            var parent = $('#pageTicket');
            $('[data-menu="ticket"]').addClass('active');
            $('.js-datePickerStartDate', parent).datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $('.js-datePickerEndDate', parent).datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });
    </script>
@endsection