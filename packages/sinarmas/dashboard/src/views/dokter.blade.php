@extends('web.layouts.html')

@section('content')
    <div class="telemed page-dashboard">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-wrapper">
                Dashboard Doctor
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-dashboard.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script>
        $(document).ready(function () {
            $('[data-menu="dashboard"]').addClass('active');
        });
    </script>
@endsection