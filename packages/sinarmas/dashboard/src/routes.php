<?php
Route::prefix('dashboard')->group(function() {
    Route::get('/', 'Sinarmas\Dashboard\DashboardController@index')->name('dashboard.page');
});