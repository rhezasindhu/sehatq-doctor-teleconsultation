<?php

namespace Sinarmas\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth:admin']);
    }

    public function index()
    {
        $data = [];

        $role = helperGetUserRole();
        $role = strtolower($role);

        return view('dashboard::' . $role, ['data' => $data]);
    }
}