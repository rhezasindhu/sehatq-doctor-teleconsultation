<?php
/* NEW Consultation -- by yandri 16/04/2019 */

namespace Sinarmas\Consultation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Doctorfield;
use Sinarmas\Consultation\Models\Chatqueue;

class ConsultationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth:admin']);
    }

    public function index()
    {
        $user = User::with('detail')->find(Auth::user()->id);
        $doctorfield = Doctorfield::find($user->detail->userdetails_doctorfields_id);
        $data = [
            'specialization_id' => $doctorfield->doctorfields_id,
            'specialization_name' => $doctorfield->doctorfields_name,
        ];
        return view('consultation::index', ['data' => $data]);
    }

    public function summary($queueId)
    {
        $chatqueue = Chatqueue::find($queueId);
        $summary = (array)json_decode($chatqueue->chatqueues_summary);
        $data = [
            'summary' => $summary
        ];
        return view('consultation::chat', ['data' => $data]);
    }

    public function getPreScreeningSection(Request $request)
    {
        $section_a = [
            [
                'label' => 'Faktor resiko',
                'data' => [
                    ['type' => 'checkbox', 'label' => 'Perokok', 'required' => false, 'value' => null, 'placeholder' => ''],
                    ['type' => 'checkbox', 'label' => 'Alkohol', 'required' => false, 'value' => null, 'placeholder' => ''],
                    ['type' => 'checkbox_textbox', 'label' => 'Lainnya', 'required' => false, 'value' => null, 'placeholder' => ''],
                ]
            ],
            [
                'label' => 'Alergi',
                'data' => [
                    ['type' => 'checkbox_textbox', 'label' => 'Obat', 'required' => false, 'value' => null, 'placeholder' => 'Ex : Antibiotik'],
                    ['type' => 'checkbox_textbox', 'label' => 'Makanan', 'required' => false, 'value' => null, 'placeholder' => 'Ex : Udang'],
                    ['type' => 'checkbox_textbox', 'label' => 'Kondisi', 'required' => false, 'value' => null, 'placeholder' => 'Ex : Debu'],
                ]
            ],
            [
                'label' => 'Pemeriksaan Penunjang',
                'data' => [
                    ['type' => 'checkbox_textbox', 'label' => 'Lab', 'required' => false, 'value' => null, 'placeholder' => 'Ex : Tes Saraf'],
                    ['type' => 'checkbox_textbox', 'label' => 'Radiologi', 'required' => false, 'value' => null, 'placeholder' => 'Ex : MRI'],
                ]
            ],
        ];

        $section_b = [
            [
                'label' => 'Riwayat Penyakit',
                'data' => [
                    ['type' => 'checkbox_textbox', 'label' => 'Ada', 'required' => false, 'value' => null, 'placeholder' => ''],
                ]
            ],
            [
                'label' => 'Riwayat Pengobatan',
                'data' => [
                    ['type' => 'checkbox_textbox', 'label' => 'Ada', 'required' => false, 'value' => null, 'placeholder' => ''],
                ]
            ],
            [
                'label' => 'Diagnosa Dokter',
                'data' => [
                    ['type' => 'textbox', 'label' => '', 'required' => true, 'value' => null, 'placeholder' => ''],
                ]
            ],
            [
                'label' => 'Treatment',
                'data' => [
                    ['type' => 'checkbox_textbox', 'label' => 'Medis/Obat', 'required' => false, 'value' => null, 'placeholder' => 'Ex : Tes Saraf'],
                    ['type' => 'checkbox_textbox', 'label' => 'Non Medis', 'required' => false, 'value' => null, 'placeholder' => 'Ex : Minum air putih'],
                ]
            ],
            [
                'label' => 'Komentar',
                'data' => [
                    ['type' => 'textarea', 'label' => '', 'required' => false, 'value' => null, 'placeholder' => ''],
                ]
            ],
        ];

        $section = $request->get('section');

        if ($section == 'a') {
            $data = $section_a;
        } else if ($section == 'b') {
            $data = $section_b;
        } else {
            $data = array_merge($section_a, $section_b);
        }

        $response = [
            'statusCode' => 200,
            'status' => 'success',
            'message' => 'Data found',
            'data' => $data,
        ];

        return response()->json($response, 200);
    }
}