<?php
Route::prefix('consultation')->group(function() {
    Route::get('/summary/{queue_id}', 'Sinarmas\Consultation\ConsultationController@summary')->name('consultation.summary');
    Route::get('/pre-screening-section', 'Sinarmas\Consultation\ConsultationController@getPreScreeningSection')->name('consultation.chat.prescreening.section');
    Route::get('/', 'Sinarmas\Consultation\ConsultationController@index')->name('consultation');
});