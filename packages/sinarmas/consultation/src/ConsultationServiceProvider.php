<?php

namespace Sinarmas\Consultation;

use Illuminate\Support\ServiceProvider;

class ConsultationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__ . '/routes.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Sinarmas\Consultation\ConsultationController');
        $this->loadViewsFrom(__DIR__ . '/views', 'consultation');
    }
}