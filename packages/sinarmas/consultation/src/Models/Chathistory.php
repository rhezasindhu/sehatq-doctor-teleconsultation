<?php
namespace Sinarmas\Consultation\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Chathistory extends Model {
    protected $table = 'chathistories';
	protected $primaryKey = 'chathistories_id';

	protected $fillable = [
        'chathistories_user_id',
        'chathistories_user_name',
        'chathistories_status',
        'chathistories_summary',
        'chathistories_call_duration',
        'chathistories_call_expired_time',
        'chathistories_medical_report',
        'chathistories_rating',
        'chathistories_comment1',
        'chathistories_comment2',
    ];

    /* Define 'One to One' relationship */
	public function user() {
        return $this->belongsTo(User::class, 'chathistories_user_id');
    }
}
