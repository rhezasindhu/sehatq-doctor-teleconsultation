<?php
namespace Sinarmas\Consultation\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Chatqueue extends Model {
    protected $table = 'chatqueues';
	protected $primaryKey = 'chatqueues_id';

	protected $fillable = ['chatqueues_user_id', 'chatqueues_user_name', 'chatqueues_status', 'chatqueues_summary'];

    /* Define 'One to One' relationship */
	public function user() {
        return $this->belongsTo(User::class, 'chatqueues_user_id');
    }
}
