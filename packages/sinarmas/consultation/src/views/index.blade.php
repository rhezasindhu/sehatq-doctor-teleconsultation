@extends('web.layouts.html')

@section('content')
    <div class="telemed page-chat" id="pageChat">
        @include('web.blocks.sidebar')
        <div class="telemed-content">
            <div class="telemed-loading js-telemedLoading">
                <img src="{{ asset(config('_customs.loading_icon')) }}" alt="">
            </div>
            <div id="user_info" class="d-none">
                <div class="bottom-profile">
                    <img src="https://dxstmhyqfqr1o.cloudfront.net/web-basic/image-profile.svg" width="30" height="30" class="image-profile">
                </div>
                <div class="bottom-nickname">
                    <div class="nickname-title">username</div>
                    <div class="nickname-content"></div>
                </div>
            </div>
            <div class="row row-0 h-100">
                <div class="col-xl-3 col-lg-4 col-md-5 col-chat-list-wrapper">
                    <div class="chats-container">
                        <ul class="nav chat-tab row row-0">
                            <li class="col-6 nav-item">
                                <a class="nav-link no-wrap active" data-toggle="tab" href="#incoming">Incoming Patient <span class="incoming js-incomingCount on-tab"></span></a>
                            </li>
                            <li class="col-6 nav-item">
                                <a class="nav-link" data-toggle="tab" href="#ongoing">Ongoing Chat</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="incoming"></div>
                            <div class="tab-pane fade" id="ongoing">
                                <div class="d-none">
                                    <div id="open_chat"></div>
                                    <div id="open_chat_add"></div>
                                    <div id="open_list">
                                        <div id="default_item_open"></div>
                                    </div>
                                    <div id="group_chat_add"></div>
                                </div>
                                <div id="group_list">
                                    <div id="default_item_group"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8 col-md-7 h-100 col-detail js-colDetail">
                    <div class="row row-0 h-100 row-col-detail">
                        <div class="col h-100">
                            <div class="col-chat-detail-wrapper">
                                <div class="chat-center h-100"></div>
                            </div>
                        </div>
                        <div class="col-xl-4 h-100 col-chat-info-wrapper js-chatInfo d-none">
                            <form class="h-100 js-submitMedicalReport js-formValidate" method="post">
                                <div class="tab-custom tab-info">
                                    <ul class="nav nav-tabs row row-0">
                                        <li class="col nav-item">
                                            <a class="nav-link no-wrap active" data-toggle="tab" href="#data-user">Data Pasien</a>
                                        </li>
                                        <li class="col nav-item col-prescreening">
                                            <a class="nav-link" data-toggle="tab" href="#section-a">Section A</a>
                                        </li>
                                        <li class="col nav-item col-prescreening">
                                            <a class="nav-link" data-toggle="tab" href="#section-b">Section B</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="data-user">
                                            <div class="tab-wrapper">
                                                <div class="pre-screening">
                                                    <div class="pre-screening-section">
                                                        <div class="title">Profile User</div>
                                                        <div class="desc">
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Gender</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataGender">-</span></div></div>
                                                            </div>
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Usia</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataAge">-</span></div></div>
                                                            </div>
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Gol Darah</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataBloodType">-</span></div></div>
                                                            </div>
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Tinggi</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataHeight">-</span></div></div>
                                                            </div>
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Berat</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataWeight">-</span></div></div>
                                                            </div>
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Alergi</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataAllergies">-</span></div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pre-screening-section">
                                                        <div class="title">Pre-screening Result</div>
                                                        <div class="desc">
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Gejala</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataSymptoms">-</span></div></div>
                                                            </div>
                                                            <div class="row row-5 mb-sm-2 mb-3">
                                                                <div class="col-sm-4"><div class="label">Durasi</div></div>
                                                                <div class="col-sm-8"><div class="value"><span class="divider">:</span><span class="ml-sm-2 ml-0 inline-block js-dataDuration">-</span></div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pre-screening-section">
                                                        <div class="title">Additional Notes</div>
                                                        <div class="desc js-dataAdditionalNotes"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="section-a">
                                            <div class="tab-wrapper">
                                                <div class="pre-screening">
                                                    <div class="pre-screening-form">
                                                        <div class="js-preScreeningA">
                                                            {{--<div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Faktor resiko</div>
                                                                <div class="form-group mb-4">
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="risk[]" value="Perokok"/>
                                                                            <span class="label-text">Perokok</span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="risk[]" value="Alkohol"/>
                                                                            <span class="label-text">Alkohol</span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="risk[]" value="Lainnya" data-type="show-input"/>
                                                                            <span class="label-text">Lainnya</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="risk_other" type="text" placeholder="" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Alergi</div>
                                                                <div class="form-group mb-4">
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="drug" value="Obat" data-type="show-input"/>
                                                                            <span class="label-text">Obat</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="drug_detail" type="text" placeholder="Ex : Antibiotik" autocomplete="off">
                                                                    </div>
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="food" value="Makanan" data-type="show-input"/>
                                                                            <span class="label-text">Makanan</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="food_detail" type="text" placeholder="Ex : Udang" autocomplete="off">
                                                                    </div>
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="condition" value="Kondisi" data-type="show-input"/>
                                                                            <span class="label-text">Kondisi</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="condition_detail" type="text" placeholder="Ex : Debu" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Pemeriksaan Penunjang</div>
                                                                <div class="form-group mb-4">
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="lab" value="Lab" data-type="show-input"/>
                                                                            <span class="label-text">Lab</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="lab_detail" type="text" placeholder="Ex : Tes Saraf" autocomplete="off">
                                                                    </div>
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="radiologi" value="Radiologi" data-type="show-input"/>
                                                                            <span class="label-text">Radiologi</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="radiologi_detail" type="text" placeholder="Ex : MRI" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>--}}
                                                        </div>
                                                        <div class="form-section mb-3 text-center">
                                                            <div class="row row-0">
                                                                <div class="col-sm-2"></div>
                                                                <div class="col-sm-8">
                                                                    <button class="btn btn-block btn-outline-primary btn-md rounded upper-case js-goToSection" data-section="#section-b" type="button">Berikutnya</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="section-b">
                                            <div class="tab-wrapper">
                                                <div class="pre-screening">
                                                    <div class="pre-screening-form">
                                                        <div class="js-preScreeningB">
                                                            {{--<div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Riwayat Penyakit</div>
                                                                <div class="form-group mb-4">
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="history_disease" value="Ada" data-type="show-input"/>
                                                                            <span class="label-text">Ada</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="history_disease_detail" type="text" placeholder="" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Riwayat Pengobatan</div>
                                                                <div class="form-group mb-4">
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="history_treatment" value="Ada" data-type="show-input"/>
                                                                            <span class="label-text">Ada</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="history_treatment_detail" type="text" placeholder="" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Diagnosa Dokter*</div>
                                                                <div class="form-group mb-4">
                                                                    <input class="form-control rounded" name="diagnosis" type="text" placeholder="" autocomplete="off">
                                                                    <label class="error d-none">Harus Diisi</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Treatment</div>
                                                                <div class="form-group mb-4">
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="medis" value="Medis/Obat" data-type="show-input"/>
                                                                            <span class="label-text">Medis/Obat</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="medis_detail" type="text" placeholder="Ex : Tes Saraf" autocomplete="off">
                                                                    </div>
                                                                    <div class="animated-checkbox mb-3">
                                                                        <label>
                                                                            <input type="checkbox" name="non_medis" value="Non Medis" data-type="show-input"/>
                                                                            <span class="label-text">Non Medis</span>
                                                                        </label>
                                                                        <input class="form-control rounded mt-2" disabled name="non_medis_detail" type="text" placeholder="Ex : Minum air putih" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-section mb-4">
                                                                <div class="form-legend mb-3">Komentar</div>
                                                                <div class="form-group mb-4">
                                                                    <textarea class="form-control rounded" name="comment"></textarea>
                                                                </div>
                                                            </div>--}}
                                                        </div>
                                                        <div class="form-section mb-3 text-center">
                                                            <div class="row row-0">
                                                                <div class="col-sm-2"></div>
                                                                <div class="col-sm-8">
                                                                    <button class="btn btn-block btn-outline-primary btn-md rounded upper-case" type="submit">Simpan</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="btn-end-chat">
                                <div class="row row-5">
                                    <div class="col-xl-12 col-sm-6 col-4">
                                        <button type="button" class="btn btn-link btn-block d-block d-xl-none js-closeSummary">Tutup</button>
                                    </div>
                                    <div class="col-xl-12 col-sm-6 col-8">
                                        <button type="button" class="btn btn-link btn-block js-endChat">Akhiri Percakapan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="emoji-wrapper js-emojiWrapper d-none">
            @include('web.blocks.emoji')
        </div>
        <input type="hidden" id="avatarDefault" value="{{ helperGetProfilePicture() }}">
        <input type="hidden" id="preScreeningSectionUrl" value="{{ route('consultation.chat.prescreening.section') }}">
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-chat.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript" src="{{ asset('public/sendbird/dist/main.js?v=' . time()) }}"></script>
    <script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/plugins/jquery.countdown' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
    <script type="text/javascript">
        var chat_app_id = '{{ env('SENDBIRD_APP_ID') }}',
            chat_api_token = '{{ env('SENDBIRD_API_TOKEN') }}',
            chat_base_url = '{{ env('CHAT_BASE_URL') }}',
            chat_api_base_url = chat_base_url + '{{ env('CHAT_API_PATH') }}',
            chat_api_sendbird_base_url = 'https://api-' + chat_app_id + '.sendbird.com/v3',
            avatar_default = chat_base_url + '/{{ config('_customs.assets.path_img') . '/avatar-default.png' }}',
            prefix_env = '{{ (env('APP_ENV') != 'live') ? env('APP_ENV') . '_' : '' }}',
            doctor_userid = '{{ Auth::user()->id }}',
            prefix_doctor_userid = 'doctor_' + prefix_env,
            doctor_userid_sendbird = prefix_doctor_userid + doctor_userid,
            doctor_nickname = '{{ Auth::user()->name }}',
            doctor_avatar = '{{ helperGetUserPicture() }}',
            doctor_specialization_id = '{{ $data['specialization_id'] }}',
            doctor_specialization_name = '{{ $data['specialization_name'] }}',
            /*prefix_patient_userid = 'patient_' + prefix_env,*/
            prefix_patient_userid = '';
    </script>
    <script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/page-chat' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-menu="chat"]').addClass('active');
            $('#avatarDefault').val(avatar_default);
        });
    </script>
@endsection