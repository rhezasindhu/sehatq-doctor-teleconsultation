<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //-- Added to enable 'Soft Delete' function

class Userdetail extends Model {
    use SoftDeletes; //-- Include SoftDeletes model
    /* The attributes that should be mutated to dates.
     * @var array */
    protected $dates      = ['deleted_at'];
    protected $table      = 'userdetails';
    protected $primaryKey = 'userdetails_id';
    protected $fillable = [
        'userdetails_users_id',
        'userdetails_doctorfields_id',
        'userdetails_doctortitle',
        'userdetails_description',
        'userdetails_experience',
        'userdetails_contact',
        'userdetails_str',
        'userdetails_sip',
        'userdetails_comment',
        'userdetails_sex',
        'userdetails_rating',
        'userdetails_feedback_count',
        'userdetails_photo'
    ];

    /* Define 'One to One' relationship */
	public function user() {
        return $this->belongsTo(User::class, 'userdetails_user_id');
    }

    public function field() {
        return $this->belongsTo(Doctorfield::class, 'userdetails_doctorfields_id');
    }
}
