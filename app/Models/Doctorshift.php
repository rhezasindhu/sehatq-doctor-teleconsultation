<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Doctorshift extends Model {
    /* The attributes that should be mutated to dates.
     * @var array */
    protected $dates      = ['deleted_at'];
    protected $table      = 'doctorshifts';
    protected $primaryKey = 'doctorshifts_id';
    protected $fillable   = [
        'doctorshifts_time',
        'doctorshifts_time_start',
        'doctorshifts_time_end',
        'doctorshifts_active',
        'created_by',
        'updated_by',
    ];

    public function scopeActive($query) {
        return $query->where('doctorshifts_active', '1');
    }

    /* Define 'Many to Many' relationship */
    public function shifts() {
        return $this->belongsToMany(User::class, 'pv_users_doctorshifts', 'doctorshift_doctorshifts_id', 'user_users_id')->withPivot('date');
    }
}
