<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //-- Added to enable 'Soft Delete' function

class Usertype extends Model {
    use SoftDeletes; //-- Include SoftDeletes model
    /* The attributes that should be mutated to dates.
     * @var array */
    protected $dates      = ['deleted_at'];
    protected $table      = 'usertypes';
    protected $primaryKey = 'usertypes_id';
    protected $fillable = [
        'usertypes_role',
        'usertypes_description',
    ];

    public function scopeGeneral($query) {
        return $query->where('usertypes_role', '<>', 'superadmin');
    }

    /* Define 'One to Many' relationship */
	public function users() {
        return $this->hasMany(User::class, 'account_type');
    }
}
