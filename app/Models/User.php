<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //-- Added to enable 'Soft Delete' function
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordToken;

class User extends Authenticatable {
    use Notifiable, SoftDeletes;
    /* The attributes that should be mutated to dates.
     * @var array */
    protected $dates    = ['deleted_at'];
    protected $table    = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'api_token',
        'remember_token',
        'account_type',
        'is_admin',
        'is_active',
        'last_login',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeDefault($query) {
        return $query
            //->with(array('detail', 'role', 'detail.field' ))
            ->leftJoin('userdetails', 'users.id', '=', 'userdetails.userdetails_users_id')
            ->leftJoin('doctorfields', 'doctorfields.doctorfields_id', '=', 'userdetails.userdetails_doctorfields_id')
            ->leftJoin('usertypes', 'users.account_type', '=', 'usertypes.usertypes_id')
            ->where('usertypes_role', '<>', 'superadmin')
            ->where('is_active', '1')
            ->orderBy('name', 'asc');
    }

    public function scopeRoleDoctor($query) {
        return $query
            //->with(array('detail', 'role', 'detail.field'))
            ->leftJoin('userdetails', 'userdetails.userdetails_users_id', '=', 'users.id')
            ->leftJoin('doctorfields', 'doctorfields.doctorfields_id', '=', 'userdetails.userdetails_doctorfields_id')
            ->leftJoin('usertypes', 'usertypes.usertypes_id', '=', 'users.account_type')
            ->where('usertypes_role', 'dokter')
            ->where('is_active', '1')
            ->orderBy('name', 'asc');
    }

    /* Define 'One to One' relationship */
	public function detail() {
        return $this->hasOne(Userdetail::class, 'userdetails_users_id');
    }

    public function role() {
        return $this->belongsTo(Usertype::class, 'account_type');
    }

    /* Define 'Many to Many' relationship */
    public function shifts() {
        return $this->belongsToMany(Doctorshift::class, 'pv_users_doctorshifts', 'user_users_id', 'doctorshift_doctorshifts_id')->withPivot('date');
    }

    /**
     * Override Send a password reset email to the user
     */
    public function sendPasswordResetNotification($token) {
        $this->notify(new MailResetPasswordToken($token));
    }
}
