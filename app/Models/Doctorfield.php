<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctorfield extends Model {
    /* The attributes that should be mutated to dates.
     * @var array */
    protected $dates      = ['deleted_at'];
    protected $table      = 'doctorfields';
    protected $primaryKey = 'doctorfields_id';
    protected $fillable   = [
        'doctorfields_name',
        'doctorfields_slug',
        'doctorfields_description',
    ];

    /* Define 'One to One' relationship */
	public function user() {
        return $this->belongsTo(Userdetail::class, 'userdetails_doctorfields_id');
    }
}
