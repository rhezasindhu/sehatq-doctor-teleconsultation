<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingFeedback extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($booking, $hospital)
    {
        $this->booking = $booking;
        $this->hospital = $hospital;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@sehatq.com')
            ->view('backend.bookings.booking_feedback')->with([
                'booking' => $this->booking,
                'hospital' => $this->hospital
            ]);
    }
}
