<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sinarmas\Mybooking\Models\DoctorBooking;

class NewBooking extends Mailable
{
    use Queueable, SerializesModels;
    public $booking, $bookingNo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(DoctorBooking $booking, $bookNo)
    {
        $this->booking = $booking;
        $this->bookNo = $bookNo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.book')->with([
            'bookNo' => $this->bookNo,
            'booking' => $this->booking
        ]);
    }
}
