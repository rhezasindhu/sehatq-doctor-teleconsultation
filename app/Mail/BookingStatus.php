<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingStatus extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $hospital;
    protected $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($booking, $hospital, $status)
    {
        $this->booking = $booking;
        $this->hospital = $hospital;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@sehatq.com')
            ->view('backend.bookings.booking_status')->with([
                'booking' => $this->booking,
                'hospital' => $this->hospital,
                'status' => $this->status
            ]);
    }
}
