<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class AccessHelpers {

    private $url;

    private $params = [];

    private $headers = [];

    public function __construct($url = '') {
        $this->client = new Client(['timeout' => 400]);
        $this->url = $url;
    }

    public function setParam($params) {
        $this->params = $params;

        return $this;
    }

    public function setHeader($params = []) {
        $this->headers = $params;

        return $this;
    }

    public function request($method = 'GET', $type = 'query') {
        try {
            $options = $this->typeRequest($type);
            $response = $this->client->request($method, $this->url, $options);
            $result = $response->getBody()->getContents();

            return json_decode($result, TRUE);
        } catch (\Exception $e) {
            return [
                'status' => FALSE,
                'message' => $e->getMessage(),
            ];
        }
    }

    private function typeRequest($type) {
        switch ($type) {
            case 'json' :
                $options['json'] = $this->params;
                break;
            case 'form' :
                $options['form_params'] = $this->params;
                break;
            case 'query' :
                $options['query'] = $this->params;
                break;
            case 'multipart' :
                $options['multipart'] = $this->params;
                break;
        }

        if (!empty($this->headers)) {
            $options['headers'] = $this->headers;
        }

        return $options;
    }
}