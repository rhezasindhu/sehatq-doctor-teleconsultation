<?php
use App\Models\User;
use App\Models\Usertype;
use App\Models\Doctorshift;
use App\Models\Doctorfield;

define("PERPAGE", "15");

if (!function_exists('helperMasterDoctor')) {
    function helperMasterDoctor($mode = 'all', $keyword = '') {
        switch ($mode) {
            case "all":
                return User::roleDoctor()->get();
                break;
            case "search":
                return User::roleDoctor()
                    ->where(function($q) use ($keyword) {
                        $q->orwhereRaw("name like '%".$keyword."%'");
                        $q->orWhereRaw("doctorfields_name like '%".$keyword."%'");
                        $q->orWhereRaw("userdetails_contact like '%".$keyword."%'");
                    })
                    ->paginate(PERPAGE);
                break;
            default:
                break;
        }
    }
}

if (!function_exists('helperMasterDoctorShift')) {
    function helperMasterDoctorShift($mode = 'all', $keyword = '') {
        switch ($mode) {
            case "all":
                return Doctorshift::all();
                break;
            case "active":
                return Doctorshift::active()->get();
                break;
            case "join":
                return Doctorshift::select(
                        'pv_users_doctorshifts.id AS id',
                        'users.name',
                        'users.api_token',
                        'users.id AS userId',
                        'pv_users_doctorshifts.doctorshift_doctorshifts_id AS shiftId',
                        'pv_users_doctorshifts.date'
                    )
                    ->active()
                    ->leftJoin('pv_users_doctorshifts', 'doctorshifts.doctorshifts_id', '=', 'pv_users_doctorshifts.doctorshift_doctorshifts_id')
                    ->leftJoin('users', 'users.id', '=', 'pv_users_doctorshifts.user_users_id')
                    ->get();
                break;
            case "search":
                return Doctorshift::where('doctorshifts_time', 'like', '%'.$keyword.'%')->paginate(PERPAGE);
                break;
            default:
                break;
        }
    }
}

if (!function_exists('helperMasterDoctorField')) {
    function helperMasterDoctorField($mode = 'all', $keyword = '') {
        switch ($mode) {
            case "all":
                return Doctorfield::all();
                break;
            case "search":
                return Doctorfield::where('doctorfields_name', 'like', '%'.$keyword.'%')->paginate(PERPAGE);
                break;
            default:
                break;
        }
    }
}

if (!function_exists('helperMasterUserRole')) {
    function helperMasterUserRole() {
        return Usertype::with('users')->get();
    }
}

?>
