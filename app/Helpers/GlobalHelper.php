<?php
use App\Libraries\SimpleImage;
use App\Models\User;
use App\Models\Userdetail;

if (!function_exists('helperGetUserRole')) {
    function helperGetUserRole() {
        $user = User::with('role')->find(Auth::user()->id);
        return $user->role->usertypes_role;
    }
}

if (!function_exists('helperGetProfilePicture')) {
    function helperGetProfilePicture($picture = '') {
        return (!empty($picture)) ? asset('public/uploads/profile/' . $picture) : asset(config('_customs.assets.path_img') . '/avatar-default.png');
    }
}

if (!function_exists('helperGetUserPicture')) {
    function helperGetUserPicture() {
        $userdetail = Userdetail::find(Auth::user()->id);
        $picture = $userdetail->userdetails_photo;
        return helperGetProfilePicture($picture);
    }
}

if (!function_exists('helperGetMenu')) {
    function helperGetMenu() {
        $role = helperGetUserRole();
        $menu = [];
        $user = Auth::user();
        $prefix_env = (env('APP_ENV') != 'live') ? env('APP_ENV') . '_' : '';
        $userid = $user->id;
        $username = $user->name;
        $role = strtolower($role);

        switch ($role) {
            case 'superadmin':
                $menu = [
                    ['name' => __('Dashboard'), 'linkUrl' => route('dashboard.page'), 'dataMenu' => 'dashboard', 'icon' => '<i class="icon-home-white"></i>'],
                    ['name' => __('Schedule'), 'linkUrl' => route('schedule'), 'dataMenu' => 'schedule', 'icon' => '<i class="icon-schedule-white"></i>'],
                    ['name' => __('Tickets'), 'linkUrl' => route('ticket'), 'dataMenu' => 'ticket', 'icon' => '<i class="icon-ticket-white"></i>'],
                    ['name' => __('Doctor'), 'linkUrl' => route('admin.doctor'), 'dataMenu' => 'doctor', 'icon' => '<i class="icon-doctor-white"></i>'],
                    ['name' => __('Master Data'), 'linkUrl' => route('admin'), 'dataMenu' => 'master', 'icon' => '<i class="icon-book-white"></i>'],
                    ['name' => __('Rating'), 'linkUrl' => route('rating'), 'dataMenu' => 'rating', 'icon' => '<i class="icon-rating-white"></i>'],
                ];
                break;

            case 'admin':
                $menu = [
                    ['name' => __('Dashboard'), 'linkUrl' => route('dashboard.page'), 'dataMenu' => 'dashboard', 'icon' => '<i class="icon-home-white"></i>'],
                    ['name' => __('Schedule'), 'linkUrl' => route('schedule'), 'dataMenu' => 'schedule', 'icon' => '<i class="icon-schedule-white"></i>'],
                    ['name' => __('Consult'), 'linkUrl' => route('consultation', ['userid' => 'doctor_' . $prefix_env . $userid, 'nickname' => $username]), 'dataMenu' => 'chat', 'icon' => '<i class="icon-chat-white"></i>'],
                    ['name' => __('Tickets'), 'linkUrl' => route('ticket'), 'dataMenu' => 'ticket', 'icon' => '<i class="icon-ticket-white"></i>'],
                    ['name' => __('Doctor'), 'linkUrl' => route('admin.doctor'), 'dataMenu' => 'doctor', 'icon' => '<i class="icon-doctor-white"></i>'],
                    ['name' => __('Master Data'), 'linkUrl' => route('admin'), 'dataMenu' => 'master', 'icon' => '<i class="icon-book-white"></i>'],
                    ['name' => __('Rating'), 'linkUrl' => route('rating'), 'dataMenu' => 'rating', 'icon' => '<i class="icon-rating-white"></i>'],
                ];
                break;

            case 'dokter':
                $menu = [
                    ['name' => __('Dashboard'), 'linkUrl' => route('dashboard.page'), 'dataMenu' => 'dashboard', 'icon' => '<i class="icon-home-white"></i>'],
                    ['name' => __('Schedule'), 'linkUrl' => route('schedule'), 'dataMenu' => 'schedule', 'icon' => '<i class="icon-schedule-white"></i>'],
                    ['name' => __('Consult'), 'linkUrl' => route('consultation', ['userid' => 'doctor_' . $prefix_env . $userid, 'nickname' => $username]), 'dataMenu' => 'chat', 'icon' => '<i class="icon-chat-white"></i>'],
                    ['name' => __('Tickets'), 'linkUrl' => route('ticket'), 'dataMenu' => 'ticket', 'icon' => '<i class="icon-ticket-white"></i>'],
                    ['name' => __('Doctor'), 'linkUrl' => route('admin.doctor'), 'dataMenu' => 'doctor', 'icon' => '<i class="icon-doctor-white"></i>'],
                    ['name' => __('Rating'), 'linkUrl' => route('rating'), 'dataMenu' => 'rating', 'icon' => '<i class="icon-rating-white"></i>'],
                ];
                break;
                
            case 'qc':
                $menu = [
                    ['name' => __('Dashboard'), 'linkUrl' => route('dashboard.page'), 'dataMenu' => 'dashboard', 'icon' => '<i class="icon-home-white"></i>'],
                    ['name' => __('Tickets'), 'linkUrl' => route('ticket'), 'dataMenu' => 'ticket', 'icon' => '<i class="icon-ticket-white"></i>'],
                    ['name' => __('Doctor'), 'linkUrl' => route('admin.doctor'), 'dataMenu' => 'doctor', 'icon' => '<i class="icon-doctor-white"></i>'],
                    ['name' => __('Rating'), 'linkUrl' => route('rating'), 'dataMenu' => 'rating', 'icon' => '<i class="icon-rating-white"></i>'],
                ];
                break;
        }

        return $menu;
    }
}

if (!function_exists('helperPathUpload')) {
    function helperPathUpload($path = '') {
        return public_path('/uploads').$path;
    }
}

if ( !function_exists('helperImageCompress')) {
    function helperImageCompress($source, $destination, $quality) {
        $info = getimagesize($source);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);
        imagejpeg($image, $destination, $quality);
        return $destination;
    }
}