<?php

namespace App\Console\Commands;

use App\Mail\BookingReminder as EmailBookingRemainder;
use App\Models\Doctorbooking;
use Sinarmas\Event\Models\Event;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use DB;
use Sinarmas\Hospital\Models\Hospital;

class BookingReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'reminder for booking';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * hari H -6 jam
         */
        $now = date('Y-m-d H.i');
        $bookings = Doctorbooking::where('doctorbookings_status', 5)
            ->where(
                \DB::raw('TIMESTAMPDIFF(HOUR, STR_TO_DATE("'.$now.'", "%Y-%m-%d %H.%i"), STR_TO_DATE(concat(doctorbookings_date, " ", SUBSTRING(doctorbookings_time, 1, 5)), "%Y-%m-%d %H.%i"))')
                , 6)->get();

        foreach($bookings as $booking)
        {
            $hospital = Hospital::find($booking->doctorbookings_hospitals_id);
            Mail::to([
                'email' => $booking->doctorbookings_patients_email,
                'name' => $booking->doctorbookings_patients_name
            ])->send(new EmailBookingRemainder($booking, $hospital));
        }

    }
}