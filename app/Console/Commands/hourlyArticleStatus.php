<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Sinarmas\Article\Models\Article;
use DB;
//use Mail;

class hourlyArticleStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hour:article_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hourly check expired/published Article date & set the status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //== Check an Expired Article based on Expired Date
        //------ Get the list of article that has set an Expired Date & the status is 'Published'
        //------ SELECT IF(`articles_expired_date` < NOW(), TRUE, FALSE) AS flag, `articles_id`, `articles_title`, `articles_status`, `articles_published_date`, `articles_expired_date` FROM `articles` WHERE `articles_expired_date` <> 'NULL' AND `articles_status` = 'published'

        $articles = DB::table('articles')->select(DB::raw('IF(`articles_expired_date` < NOW(), TRUE, FALSE) AS flag'), 'articles_id', 'articles_title', 'articles_status', 'articles_published_date', 'articles_expired_date')
            ->where('articles_expired_date','<>','NULL')
            ->where('articles_status','published')
            ->get();

        foreach ($articles as $article) {
            if ($article->flag == '1') {
                DB::statement("UPDATE articles SET articles_status = 'expired', articles_expired_date = NULL WHERE articles_id = ".$article->articles_id);
                //$this->info('Per Articles '.$article->articles_id);
            }
        }

        //== Check a Published Article based on Published Date
        //------ Get the list of article that has status 'Draft'
        //------ SELECT IF(`articles_published_date` <= NOW(), TRUE, FALSE) AS flag, `articles_id`, `articles_title`, `articles_status`, `articles_published_date`, `articles_expired_date` FROM `articles` WHERE `articles_status` = 'draft'
        $articles = DB::table('articles')->select(DB::raw('IF(`articles_published_date` <= NOW(), TRUE, FALSE) AS flag'), 'articles_id', 'articles_title', 'articles_status', 'articles_published_date', 'articles_expired_date')
            ->where('articles_status','draft')
            ->get();

        foreach ($articles as $article) {
            if ($article->flag == '1') {
                DB::statement("UPDATE articles SET articles_status = 'published' WHERE articles_id = ".$article->articles_id);
                //$this->info('Per Articles '.$article->articles_id);
            }
        }

        //-- If want to send Email Notification
        /*Mail::raw("This is automatically generated Hourly Update", function($message) {
            $message->from('admin@sehatq.com');
            $message->to("abc@sehatq.com")->subject('Hourly Update');
        });*/

        $this->info('Hourly check article status has been successfully executed '.$articles);
    }
}
