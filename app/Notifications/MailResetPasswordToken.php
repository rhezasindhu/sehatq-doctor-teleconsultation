<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordToken extends Notification
{
    use Queueable;
    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        //
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('info@sehatq.com')
            ->subject('Reset ulang kata sandi - SehatQ')
            ->line("Anda menerima email ini karena kami telah menerima permintaan reset kata sandi pada akun anda.")
            ->action('Reset Kata Sandi', url('password/reset', $this->token).'?email='.urlencode($notifiable->email))
            ->line('Apabila anda tidak mengirim permintaan ini, anda dapat mengabaikan pesan ini.');
            //->view('reset.emailer')
            //->action( 'Reset Password', $link)
            //->attach('reset.attachment')
            //->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
