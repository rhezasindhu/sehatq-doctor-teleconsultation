<?php

namespace App\Traits;

trait UpdatedBy
{
	public static function bootUpdatedBy()
	{
        $userEmail = null;
		if(auth()->check())
			$userEmail = auth()->user()->email;

		static::saving(function ($model) use ($userEmail){
			$model->updated_by = $userEmail;
		});
	}
}