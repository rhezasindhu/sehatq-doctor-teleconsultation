<?php

namespace App\Traits;

trait CreatedBy
{
	public static function bootCreatedBy()
	{
		$userEmail = null;
		if(auth()->check())
			$userEmail = auth()->user()->email;

		static::creating(function ($model) use ($userEmail){
			$model->created_by = $userEmail;
		});
	}
}