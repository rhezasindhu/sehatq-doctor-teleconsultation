<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * NOTE: Please comment/remove showLoginForm() in vendor\laravel\framework\src\Illuminate\Foundation\Auth\AuthenticatesUsers.php
     *
     * @return \Illuminate\Http\Response
     */

    public function showLoginForm()
    {
        return view('auth.login');
    }

    // Override 'attemptLogin' method from 'AuthenticatesUsers', for additional 'account_type' checking
    protected function credentials(Request $request)
    {
        return ['email' => $request->email, 'password' => $request->password];
    }



    /*===== Additionals self-made method to support enhance advanced login =====*/
    /*public function ajaxLogin(Request $request) {
        $email      = $request->email;
        $password   = $request->password;
        if (Auth::guard('web')->attempt(['email' => $email, 'password' => $password, 'is_admin' => '0'])) {
            $user = User::with('details')->find(Auth::user()->id);
            session(['user' => $user]);

            $msg = array(
                'status'  => 'success',
                'message' => 'Selamat datang, anda telah berhasil login.',
            );
            $request->session()->flash('alert-success', ['Selamat datang di SehatQ, anda telah berhasil login.']);
            return response()->json($msg);
        } else {
            $msg = array(
                'status'  => 'error',
                'message' => 'Mohon pastikan email & kata kunci anda benar, apabila anda butuh bantuan silahkan hubungi Customer Care kami. ',
            );
            return response()->json($msg);
        }
    }

    public function ajaxSocmedLogin(Request $request){
        $email      = $request->email;
        $user       = User::where('email', $email)->first();

        if ($user->is_admin == 0) {
            Auth::loginUsingId($user->id, TRUE);
            $user = User::with('details')->find($user->id);
            session(['user' => $user]);
            $msg = array(
                'status'  => 'success',
                'message' => 'Selamat datang, anda telah berhasil login.',
            );
            $request->session()->flash('alert-success', ['Selamat datang di SehatQ, anda telah berhasil login.']);
            return response()->json($msg);
        } else {
            $msg = array(
                'status'  => 'warning',
                'message' => 'Penggunaan akses akun berbeda. Mohon gunakan akun yang valid.',
            );
            return response()->json($msg);
        }
    }

    public function loginAPI(Request $request) {
        $api_token = ($request->get('api_token')) ? $request->get('api_token'):"";
        if ($api_token !="") {
            $user = User::where('users.api_token', $api_token)->first();
            if ($user) {
                Auth::loginUsingId($user->id, TRUE);
                return redirect()->route('home');
                //return response()->json(['success' => true], 200);
            } else {
                return response()->json(['error' => 'User type not found'], 401);
            }
        } else {
            return response()->json(['error' => 'Missing API token'], 401);
        }
    }*/
}
