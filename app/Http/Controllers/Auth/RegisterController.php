<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\User;
use App\Models\Usertype;
use App\Models\Userdetail;
use App\Models\Userprofiling;
use App\Models\Useractivity;
use App\Models\Keyword;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //$arrDob = explode('/', $data['dob']);
        //$formattedDob = $arrDob[2].'-'.$arrDob[1].'-'.$arrDob[0];

        $userTypes = Usertype::where('usertypes_role','=','patient')->firstOrFail();
        $user = User::create([
            'name'         => $data['name'],
            'email'        => $data['email'],
            'password'     => Hash::make($data['password']),
            'account_type' => $userTypes->usertypes_id,
            'is_admin'     => 0,
        ]);

        $userDetails = Userdetail::create([
            'userdetails_user_id' => $user->id,
            'userdetails_alias'   => "",
            'userdetails_mobile'  => "",
            'userdetails_photo'   => "",
            'userdetails_city'    => "",
            'userdetails_country' => "",
            'userdetails_sex'     => $data['gender'],
            'userdetails_dob'     => $data['bod-tahun']."-".$data['bod-bulan']."-".$data['bod-tanggal'],
            'userdetails_height'  => $data['height'],
            'userdetails_weight'  => $data['weight'],
        ]);

        Useractivity::create([
            'useractivities_users_id'       => $user->id,
            'useractivities_name'           => 'Selamat datang di SehatQ',
            'useractivities_date'           => date('Y-m-d', strtotime($user->created_at)),
        ]);

        return $user;
    }



    /*===== Additionals self-made method to support enhance advanced register =====*/
    public function ajaxRegisterCheck(Request $request){
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $msg = array(
                'status'  => 'error',
                'message' => 'User telah terdaftar',
            );
            return response()->json($msg);
        } else {
            $msg = array(
                'status'  => $request->email,
                'message' => 'Ke tahap selanjutnya',
            );
            return response()->json($msg);
        }
    }

	public function ajaxRegister(Request $request){
		$validatedData = $request->validate([
			'email'          => 'required|email',
            'password'       => 'required|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+]+.*)[0-9a-zA-Z\w!@#$%^&*()+]{8,}$/',
			'name'           => 'required|min:3|max:190',
			'gender'         => 'required', //-- not_in:0 --> to prevent empty value in select '-- Please Select --'
			'bod-tanggal'    => 'required',
            'bod-bulan'      => 'required',
            'bod-tahun'      => 'required',
			'height'         => 'numeric',
			'weight'         => 'numeric',
			'chkProfiling'   => 'required',
			'chkKeyword'     => 'required',
		]);

		$userTypes = Usertype::where('usertypes_role','=','patient')->firstOrFail();
		$userData = new User();
		$userData->name 		= $request->name;
		$userData->email 		= $request->email;
		$userData->password 	= Hash::make($request->password);
		$userData->account_type = $userTypes->usertypes_id;
		$userData->is_admin		= 0;
        //$redirect               = $request->edtCurrentUrl;

		if($userData->save()) {
            $userDataDetails = new Userdetail();

            //-- Upload user img
			$userpic = '';
			if ($request->hasFile('userpic')) {
				$image = $request->file('userpic');
				$userpic = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('/img/user_pic');
				$image->move($destinationPath, $userpic);
                $userDataDetails->userdetails_photo = $userpic;
			}

			$userDataDetails->userdetails_user_id   = $userData->id;
			$userDataDetails->userdetails_alias 	= "";
			$userDataDetails->userdetails_mobile 	= "";
			$userDataDetails->userdetails_city 	    = "";
			$userDataDetails->userdetails_country 	= "";
			$userDataDetails->userdetails_sex 		= $request->gender;
			$userDataDetails->userdetails_dob 		= $request->get('bod-tahun')."-".$request->get('bod-bulan')."-".$request->get('bod-tanggal');
			$userDataDetails->userdetails_height 	= $request->height;
			$userDataDetails->userdetails_weight 	= $request->weight;

			if($userDataDetails->save()) {
                Useractivity::create([
                    'useractivities_users_id'       => $userData->id,
                    'useractivities_name'           => 'Selamat datang di SehatQ',
                    'useractivities_date'           => date('Y-m-d', strtotime($userData->created_at)),
                ]);

                //-- Save user feeds
                $feeds = array_unique($request->chkKeyword);

                //-- Delete first before insert the feed  -- eventhough it rarely used because register only happen once
                DB::delete('DELETE FROM pv_users_feeds WHERE user_id=?', [$userData->id]);

                $feed = array();
                $catfeed = array();
                foreach ($request->chkKeyword as $key=>$keywords) {
                    $keyword = explode(',', $keywords);
                    $feed[$key] = $keyword[0];
                    $catfeed[$key] = $keyword[1];

                    DB::insert('INSERT INTO pv_users_feeds (user_id, usercat_usercats_id, userfeed_userfeeds_id) values (?, ?, ?)', [$userData->id, $catfeed[$key], $feed[$key] ]);
                }

				//-- Save user profiling
				$profiling  = Userprofiling::find($request->chkProfiling);
				$userData->profilings()->sync($profiling);
				$request->session()->flash('alert-success', ['Terimakasih telah mendaftar di SehatQ, anda telah login.']);
                Auth::loginUsingId($userData->id);
				$msg = array(
					'status'  => 'success',
					'message' => 'Terimakasih telah mendaftar di SehatQ, anda telah login.',
				);
				return response()->json($msg);
			} else {
				$msg = array(
					'status'  => 'error',
					'message' => 'Mohon pastikan data yang anda input benar, apabila anda butuh bantuan silahkan hubungi Customer Care kami. ',
				);
				return response()->json($msg);
			}
		} else {
			$msg = array(
				'status'  => 'error',
				'message' => 'Mohon pastikan data yang anda input benar, apabila anda butuh bantuan silahkan hubungi Customer Care kami. ',
			);
			return response()->json($msg);
		}
    }

    public function ajaxSocmedRegister(Request $request){
        //dd($request->email);
        $userTypes = Usertype::where('usertypes_role','=','patient')->firstOrFail();
		$userData = new User();
		$userData->name 		= $request->name;
		$userData->email 		= $request->email;
		$userData->password 	= Hash::make($request->id);
		$userData->account_type = $userTypes->usertypes_id;
		$userData->is_admin		= 0;
        //$redirect               = $request->edtCurrentUrl;

		if($userData->save()) {
            $userDataDetails = new Userdetail();

            //-- Upload user img
			/*$image = $request->img;
			$userpic = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/img/user_pic');
			$image->move($destinationPath, $userpic);
            $userDataDetails->userdetails_photo = $userpic;*/
			$userDataDetails->userdetails_user_id   = $userData->id;

			if($userDataDetails->save()) {
                $request->session()->flash('alert-success', ['Terimakasih anda telah terdaftar di SehatQ, anda telah login.']);
                Auth::loginUsingId($userData->id);
				$msg = array(
					'status'  => 'success',
					'message' => 'Terimakasih anda telah terdaftar di SehatQ, anda telah login.',
                    //'redirect'=> $redirect,
				);
				return response()->json($msg);
            }
        }
    }
}
