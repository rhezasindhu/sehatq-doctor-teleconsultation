<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller {
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

     /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
        return view('auth.login');
    }

    protected function guard() {
        return Auth::guard('admin');
    }

    /* Override 'attemptLogin' method from 'AuthenticatesUsers', for additional 'account_type' checking */
    protected function credentials(Request $request) {
        return ['email' => $request->email, 'password' => $request->password, 'is_admin' => '1', 'is_active' => '1'];
    }

    protected function authenticated(Request $request, $user) {
        $user->api_token  = str_random(60);
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();
    }
}
