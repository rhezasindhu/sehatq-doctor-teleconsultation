<?php
namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Userdetail;
use App\Helpers\AccessHelpers;
use Sinarmas\Consultation\Models\Chatqueue;
use Sinarmas\Consultation\Models\Chathistory;

class ChatController extends Controller {
    public function getUserbyToken($token) {
        $url = env('API_SEHATQ_WEB') . '/profile';
        $content = new AccessHelpers($url);
        $content->setParam([
            "api_token" => $token
        ]);
        $content_response = $content->request('GET');
        if(!empty($content_response['data'])) {
            return $content_response['data'][0];
        } else {
            $result = array (
                'statusCode' => 200,
                'status'     => 'error',
                'message'    => 'Data not found',
                'data'       => null
            );
            return $result;
        }
    }

    public function queueSave(Request $request){
        $api_token = ($request->get('api_token')) ? $request->get('api_token'):"";
        if ($api_token) {
            $user = self::getUserbyToken($api_token);
            $result['chatqueue'] = [];
            if ($user) {
                //-- Check Limit Active Users
                $chatqueues = Chatqueue::all();
                /*if (count($chatqueues) >= 20) {
                    return response()->json(['error' => 'Sorry, Reached of user limitation'], 401);
                } else {*/
                    $chatqueue = Chatqueue::where('chatqueues_user_id', $user['id'])->first();
                    if ($chatqueue) {
                        return response()->json(['error' => 'Duplicate user found'], 400);
                    } else {
                        $input = $request->all();
                        $summary = [];
                        if (isset($input['name'])) { $summary['name'] = $input['name']; }
                        if (isset($input['gender'])) { $summary['gender'] = $input['gender']; }
                        if (isset($input['avatarURL'])) {
                            $summary['avatarURL'] = $input['avatarURL'];
                        } else {
                            $summary['avatarURL'] = "";
                        }
                        if (isset($input['age'])) { $summary['age'] = $input['age']; }
                        if (isset($input['height'])) { $summary['height'] = $input['height']; }
                        if (isset($input['weight'])) { $summary['weight'] = $input['weight']; }
                        if (isset($input['bloodtype'])) { $summary['bloodtype'] = $input['bloodtype']; }
                        if (isset($input['symptoms'])) { $summary['symptoms'] = $input['symptoms']; }
                        if (isset($input['duration'])) { $summary['duration'] = $input['duration']; }
                        if (isset($input['additional'])) { $summary['additional'] = $input['additional']; }
                        if (isset($input['symptomsDetail'])) {
                            $summary['symptomsDetail'] = $input['symptomsDetail'];
                        } else {
                            $summary['symptomsDetail'] = "";
                        }
                        if (isset($input['allergies'])) { $summary['allergies'] = $input['allergies']; }
                        if (isset($input['question'])) { $summary['question'] = $input['question']; }

                        $chatqueue = new Chatqueue([
                            'chatqueues_user_id'   => $user['id'],
                            'chatqueues_user_name' => $input['name'],
                            'chatqueues_status'    => "waiting",
                            'chatqueues_summary'   => json_encode($summary),
                        ]);
                        $result['chatqueue'] = $chatqueue;

                        if ($chatqueue->save()) {
                            return response()->json(['success' => $result], 200);
                        } else {
                            return response()->json(['error' => 'Failed to save queue'], 401);
                        }
                    }
                //}
            } else {
                return response()->json(['error' => 'User not found'], 401);
            }
        } else {
            return response()->json(['error' => 'Missing API token'], 401);
        }
    }

    public function queueList(Request $request) {
        $chatqueues  = Chatqueue::get();
        $arrChatqueue = [];
        foreach ($chatqueues as $chatqueue) {
            $summary        = (array)json_decode($chatqueue->chatqueues_summary);
            $arrChatqueue[] = [
                'queue_id'   => (int)$chatqueue->chatqueues_id,
                'user_id'    => (int)$chatqueue->chatqueues_user_id,
                'user_name'  => $chatqueue->chatqueues_user_name,
                'created_at' => date("c", strtotime($chatqueue->created_at)),
                'status'     => $chatqueue->chatqueues_status,
                'summary'    => $summary,
            ];
        }
        $result = array (
            'statusCode' => 200,
            'status'     => 'success',
            'message'    => 'Queue list loaded',
            'data'       => $arrChatqueue
        );
        return response()->json($result);
    }

    public function prescreeningDetail(Request $request, $id) {
        $chatMode = ($request->get('chat_mode')) ? $request->get('chat_mode') : "waiting";
        if ($id) {
            $arrChathistory = [];
            if ($chatMode == 'waiting' || $chatMode == 'match') {
                $chatData = Chatqueue::where('chatqueues_id', $id)->first();
                if ($chatData) {
                    $summary = (array)json_decode($chatData->chatqueues_summary);
                    $arrChathistory = [
                        'id'      => (int)$chatData->chatqueues_id,
                        'user_id' => (int)$chatData->chatqueues_user_id,
                        'name'    => $chatData->chatqueues_user_name,
                        'status'  => $chatData->chatqueues_status,
                        'expired' => date("Y/m/d H:i:s", strtotime($chatData->chatqueues_call_expired_time)),
                        'summary' => $summary,
                    ];
                }
            } elseif ($chatMode == 'accepted') {
                $chatData = Chathistory::where('chathistories_id', $id)->first();
                if ($chatData) {
                    $summary = (array)json_decode($chatData->chathistories_summary);
                    $arrChathistory = [
                        'id'      => (int)$chatData->chathistories_id,
                        'user_id' => (int)$chatData->chathistories_user_id,
                        'name'    => $chatData->chathistories_user_name,
                        'status'  => $chatData->chathistories_status,
                        'expired' => date("Y/m/d H:i:s", strtotime($chatData->chathistories_call_expired_time)),
                        'summary' => $summary,
                    ];
                }
            }
            $result = array (
                'statusCode' => 200,
                'status'     => 'success',
                'message'    => 'Prescreening data loaded',
                'data'       => $arrChathistory
            );
        } else {
            $result = array (
                'statusCode' => 200,
                'status'     => 'error',
                'message'    => 'Missing ID',
                'data'       => null
            );
        }
        return response()->json($result);
    }

    public function queueCount(Request $request) {
        $result = array (
            'statusCode' => 200,
            'status'     => 'success',
            'message'    => 'Queue counter',
            'data'       => Chatqueue::where('chatqueues_status', 'waiting')->count()
        );
        return response()->json($result);
    }

    public function queueClear(Request $request) {
        $chatqueues = Chatqueue::truncate();
        $result = array (
            'statusCode' => 200,
            'status'     => 'success',
            'message'    => 'Queue cleared',
            'data'       => null
        );
        return response()->json($result);
    }

    public function queueClearApp(Request $request, $chatId) {
        $api_token = ($request->get('api_token')) ? $request->get('api_token'):"";
        if ($api_token) {
            $user = self::getUserbyToken($api_token);
            if ($user) {
                $chatqueues = Chatqueue::find($chatId);
                if ($chatqueues) {
                    $chatqueues->delete();
                    return response()->json(['success' => true], 200);
                } else {
                    return response()->json(['error' => 'Chat Id not found'], 401);
                }
            } else {
                return response()->json(['error' => 'User not found'], 401);
            }
        } else {
            return response()->json(['error' => 'Missing API token'], 401);
        }
    }

    public function join(Request $request) {
        $chatqueueId = $request->get('queue_id');
        if ($chatqueueId) {
            $chatqueue = Chatqueue::where('chatqueues_id', $chatqueueId)->first();
            if ($chatqueue) {
                $chatqueue->chatqueues_status = 'match';
                if ($chatqueue->save()) {
                    $result = array (
                        'statusCode' => 200,
                        'status'     => 'success',
                        'message'    => 'Join chat',
                        'data'       => $chatqueue
                    );
                } else {
                    $result = array (
                        'statusCode' => 200,
                        'status'     => 'error',
                        'message'    => 'Failed to join chat',
                        'data'       => null
                    );
                }
            } else {
                $result = array (
                    'statusCode' => 200,
                    'status'     => 'error',
                    'message'    => 'Chat queue not found',
                    'data'       => null
                );
            }
        } else {
            $result = array (
                'statusCode' => 200,
                'status'     => 'error',
                'message'    => 'Missing chat queue ID',
                'data'       => null
            );
        }
        return response()->json($result);
    }

    public function end(Request $request) { //-- From Doctor Initiate
        $chat_id     = $request->get('chat_id');
        $chathistory = Chathistory::where('chathistories_id', $chat_id)->first();
        if ($chathistory) {
            $chathistory->chathistories_status = 'done';
            $chathistory->save();
            return response()->json(['success' => true], 200);
        } else {
            return response()->json(['error' => 'chat history not found'], 200);
        }
    }

    public function changeStatus(Request $request) {
        $api_token = ($request->get('api_token')) ? $request->get('api_token'):"";
        if ($api_token) {
            $user = self::getUserbyToken($api_token);
            if ($user) {
                $dataChannel = $request->get('data_channel');
                $channelUrl  = $request->get('channel_url');
                $status      = $request->get('status');
                $chatId      = $dataChannel['chat_id'];
                $time        = 15;

                if ($status == "accepted") {
                    //-- Move from Queue to History
                    $chatqueue   = Chatqueue::where('chatqueues_id', $chatId)->first();
                    $date        = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . " +" . $time . " minutes"));
                    $chathistory = new Chathistory([
                        'chathistories_user_id'           => $chatqueue->chatqueues_user_id,
                        'chathistories_user_name'         => $chatqueue->chatqueues_user_name,
                        'chathistories_status'            => 'accepted',
                        'chathistories_call_duration'     => 0,
                        'chathistories_call_expired_time' => $date,
                        'chathistories_summary'           => $chatqueue->chatqueues_summary,
                    ]);

                    if ($chathistory->save()) {
                        //-- Delete from queue
                        Chatqueue::find($chatId)->delete();

                        //-- Update Group Channel
                        $dataChannel['expired'] = date("Y/m/d H:i:s", strtotime(date("Y-m-d H:i:s")." +" . $time . " minutes"));
                        $dataChannel['status']  = 'accepted';
                        $dataChannel['chat_id'] = $chathistory->chathistories_id;

                        $group_channel = self::updateGroupChannel($dataChannel, $channelUrl);

                        return response()->json(['success' => true, 'update_group_channel' => $group_channel], 200);
                    } else {
                        return response()->json(['error' => 'Failed to accept'], 401);
                    }
                } else if ($status == "declined") {
                    //-- Update Group Channel
                    $dataChannel['expired'] = date("Y/m/d H:i:s", strtotime(date("Y-m-d H:i:s")." +0 minutes"));
                    $dataChannel['status']  = 'declined';
                    $dataChannel['chat_id'] = $chatId;
                    $group_channel = self::updateGroupChannel($dataChannel, $channelUrl);

                    Chatqueue::find($chatId)->delete();

                    return response()->json(['success' => true], 200);
                } else if ($status == "extended") {
                    //-- Update Group Channel
                    $dataChannel['expired'] = date("Y/m/d H:i:s", strtotime(date("Y-m-d H:i:s")." +" . $time . " minutes"));
                    $dataChannel['status']  = 'extended';

                    $group_channel = self::updateGroupChannel($dataChannel, $channelUrl);

                    return response()->json(['success' => true, 'update_group_channel' => $group_channel], 200);
                } else if ($status == "done"){
                    $chathistory = Chathistory::where('chathistories_id', $chatId)->first();
                    $chathistory->chathistories_status = 'done';
                    $chathistory->save();

                    //-- Update Group Channel
                    $dataChannel['expired'] = date("Y/m/d H:i:s", strtotime(date("Y-m-d H:i:s")." 0 minutes"));
                    $dataChannel['status']  = 'done';
                    $dataChannel['user_leave_channel']  = true;

                    $group_channel = self::updateGroupChannel($dataChannel, $channelUrl);

                    return response()->json(['success' => true, 'update_group_channel' => $group_channel], 200);
                }
            } else {
                return response()->json(['error' => 'User not found'], 401);
            }
        } else {
            return response()->json(['error' => 'Missing API token'], 401);
        }
    }

    public function saveMedicalReport(Request $request) { //-- From Doctor Initiate
        $chat_id     = $request->get('chat_id');
        $report      = $request->get('report');
        $chathistory = Chathistory::where('chathistories_id', $chat_id)->first();
        $chathistory->chathistories_medical_report = json_encode($report);
        $chathistory->save();
        return response()->json(['success' => true], 200);
    }

    public function updateGroupChannel($dataChannel, $channelUrl) {
        $url = "https://api-" . env('SENDBIRD_APP_ID') . ".sendbird.com/v3/group_channels/" . $channelUrl;
        $content = new AccessHelpers($url);
        $content->setParam([
            "channel_url" => $channelUrl,
            "data" => json_encode($dataChannel),
        ]);
        $content_response = $content->setHeader([
            'Content-Type' => 'application/json',
            'Api-Token' => env('SENDBIRD_API_TOKEN'),
        ])->request('PUT', 'json');

        return $content_response;
    }

    public function feedback(Request $request) {
        $api_token = ($request->get('api_token')) ? $request->get('api_token'):"";
        if ($api_token) {
            $user = self::getUserbyToken($api_token);
            if ($user) {
                $chat_id = $request->get('chat_id');
                //-- Insert rating at chathistories
                $chathistory = Chathistory::find($chat_id);
                $rating = ($request->get('rating')) ? $request->get('rating') : 0;
                $chathistory->chathistories_rating   = $rating;
                $chathistory->chathistories_comment1 = $request->get('comment1');
                $chathistory->chathistories_comment2 = $request->get('comment2');
                $chathistory->save();
                //-- Update rating at tbl user
                $doctor_id = $request->get('doctor_id');
                $userdetail = Userdetail::where('userdetails_users_id', $doctor_id)->first();
                if ($userdetail->userdetails_rating > 0) {
                    $resultRating = round(($userdetail->userdetails_rating + $rating)/2, 2);
                } else {
                    $resultRating = $rating;
                }
                $userdetail->userdetails_rating = $resultRating;
                $userdetail->userdetails_feedback_count = $userdetail->userdetails_feedback_count + 1;
                $userdetail->save();
                return response()->json(['success' => true], 200);
            } else {
                return response()->json(['error' => 'User not found'], 401);
            }
        } else {
            return response()->json(['error' => 'Missing API token'], 401);
        }
    }
}
