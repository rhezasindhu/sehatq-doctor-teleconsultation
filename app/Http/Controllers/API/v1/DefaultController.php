<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;

class DefaultController extends Controller {

    public function login(Request $request) {
        $email    = $request->get('email');
        $password = $request->get('password');
        if (Auth::attempt(['email' => $email, 'password' => $password, 'is_admin' => '1' ])) {
            $user            = Auth::user();
            $user->api_token = str_random(60);
            $user->save();
            Auth::loginUsingId($user->id);

            $result = array (
                'status'  => 'success',
                'message' => 'Selamat datang, anda telah berhasil login.',
                'data'    => $user
            );
        } else {
            $user = User::where('email', $email)->get();
            if (count($user) == 0) {
                $result = array (
                    'status'  => 'error',
                    'message' => 'Maaf, Email belum terdaftar',
                    'data'    => null
                );
            } else {
                $result = array (
                    'status'  => 'error',
                    'message' => 'Mohon pastikan email & kata kunci anda benar.',
                    'data'    => null
                );
            }
        }
        return response()->json($result);
    }



    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'             => 'required|min:3|max:190',
			'email'            => 'required|unique:users,email|email',
            'password'         => 'required|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()+]+.*)[0-9a-zA-Z\w!@#$%^&*()+]{8,}$/',
            'confirm_password' => 'required|same:password',
			'gender'           => 'required',
			'bod_tanggal'      => 'required',
            'bod_bulan'        => 'required',
            'bod_tahun'        => 'required',
			'height'           => 'numeric',
			'weight'           => 'numeric',
            'photo'            => '',
			//'chk_profiling'     => 'required',
			//'chk_keyword'       => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $requestData = $request->all();
        $input['name']               = $requestData['name'];
        $input['email']              = $requestData['email'];
        $input['password']           = Hash::make($requestData['password']);
        $input['account_type']       = '4';       //-- 4  = Patient
        $input['is_admin']           = '0';
        $user = User::create($input);

        $inputdetail['userdetails_user_id'] = $user->id;
        $inputdetail['userdetails_sex']     = $requestData['gender'];
        $inputdetail['userdetails_dob']     = $requestData['bod_tahun']."-".$requestData['bod_bulan']."-".$requestData['bod_tanggal'];
        if (isset($requestData['weight'])) {
            $inputdetail['userdetails_weight']  = $requestData['weight'];
        }
        if (isset($requestData['height'])) {
            $inputdetail['userdetails_height']  = $requestData['height'];
        }
        if (isset($requestData['photo'])) {
            $inputdetail['userdetails_photo']   = $requestData['photo'];    //-- Only filename
        }
        $userdetails = Userdetail::create($inputdetail);

        //-- Delete first before insert the feed  -- eventhough it rarely used because register only happen once
        if (isset($requestData['chk_keyword'])) {
            DB::delete('DELETE FROM pv_users_feeds WHERE user_id=?', [$user->id]);
            //-- Save User Feeds
            $feed = array();
            $catfeed = array();
            foreach ($requestData['chk_keyword'] as $key=>$keywords) {
                $keyword = explode(',', $keywords);
                $feed[$key] = $keyword[0];
                $catfeed[$key] = $keyword[1];
                DB::insert('INSERT INTO pv_users_feeds (user_id, usercat_usercats_id, userfeed_userfeeds_id) values (?, ?, ?)', [$user->id, $catfeed[$key], $feed[$key] ]);
            }
        }

        //-- Save user profiling
        if (isset($requestData['chk_profiling'])) {
            $profiling  = Userprofiling::find($requestData['chk_profiling']);
            $user->profilings()->sync($profiling);
        }

        //-- Auto login user after register
        if (Auth::loginUsingId($user->id)) {
            $user = Auth::user();
            $user->api_token = str_random(60);
            $user->save();
        }

        /*if(Auth::attempt(['email' => $user->email, 'password' => $user->password, 'is_admin' => '0' ])){
            $user = Auth::user();
            $user->api_token = str_random(60);
            $user->save();
        }*/

        $success['name']  =  $user->name;
        $success['token'] =  $user->api_token;
        if (isset($requestData['chk_profiling'])){
            $success['chk_profiling'] = $requestData['chk_profiling'];
        }
        if (isset($requestData['chk_keyword'])) {
            $success['chk_keyword'] = $requestData['chk_keyword'];
        }
        //$success['token'] =  $user->createToken('MyApp')->accessToken;
        return response()->json(['success'=>$success], $this->successStatus);
    }
}
