<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        return redirect()->route('dashboard.page');
    }

    public function logout() {
        $user = Auth::user();
        $user->api_token  = null;
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();

        auth()->logout();       //-- Manually logout a user
        return redirect('/');
    }
}
