<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUserdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userdetails', function (Blueprint $table) {
            $table->increments('userdetails_id');
			$table->integer('userdetails_user_id');
			$table->string('userdetails_alias', 255);
			$table->tinyInteger('userdetails_marital');
			$table->date('userdetails_dob');
			$table->string('userdetails_mobile', 20);
			$table->string('userdetails_bpjs', 255);
			$table->integer('userdetails_privateinsurance_id');
			$table->enum('userdetails_gender', ['m', 'f']);
			$table->string('userdetails_city', 255);
			$table->string('userdetails_country', 255);
			$table->smallInteger('userdetails_age');
			$table->string('userdetails_account_type', 50);
			$table->integer('userdetails_dependency');																		
            $table->timestamps();
			//$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdetails');
    }
}
