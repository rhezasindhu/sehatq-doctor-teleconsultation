<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPrivateinsurance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privateinsurance', function (Blueprint $table) {
            $table->increments('privateinsurance_id');
			$table->string('privateinsurance_name', 255);
			$table->string('privateinsurance_number', 255);								
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privateinsurance');
    }
}
