let gulp = require('gulp'),
    gulp_scss = require('gulp-sass'),
    gulp_cache = require('gulp-cache'),
    gulp_uglify = require('gulp-uglify'),
    gulp_concat = require('gulp-concat'),
    gulp_rename = require('gulp-rename'),
    del = require('del'),
    run_sequence = require('run-sequence'),
    path_dev = 'resources/assets',
    path_dist = 'public/assets',
    clean_list = [],
    task = {
        listSCSS: function () {
            return [
                {
                    'name': 'web',
                    'dir_dev': path_dev + '/scss',
                    'dir_dist': path_dist + '/css'
                }
            ]
        },
        listJS: function () {
            return [
                {
                    'name': 'web-customs',
                    'dir_dev': path_dev + '/js/customs',
                    'dir_dist': path_dist + '/js',
                    'filename': '',
                },
                {
                    'name': 'web-libs',
                    'dir_dev': path_dev + '/js/libs',
                    'dir_dist': path_dist + '/js',
                    'filename': '_libs',
                },
                {
                    'name': 'web-plugins',
                    'dir_dev': path_dev + '/js/plugins',
                    'dir_dist': path_dist + '/js/plugins',
                    'filename': '',
                }
            ]
        },
        compileSCSS: function (name, dir_dev, dir_dist) {
            gulp.task('scss_' + name + ':compile', function () {
                return gulp.src(dir_dev + '/**/*.scss')
                    .pipe(gulp_scss.sync({outputStyle: 'compressed'}).on('error', gulp_scss.logError))
                    .pipe(gulp.dest(dir_dist));
            });
        },
        compileJS: function (name, dir_dev, dir_dist, filename, option) {
            if (filename !== '') {
                if(option === 'min') {
                    gulp.task('js_' + name + ':compile', function () {
                        return gulp.src([dir_dev + '/**/*.js'])
                            .pipe(gulp_concat(filename + '.min.js'))
                            .pipe(gulp_uglify())
                            .pipe(gulp.dest(dir_dist));
                    });
                } else {
                    gulp.task('js_' + name + ':compile', function () {
                        return gulp.src([dir_dev + '/**/*.js'])
                            .pipe(gulp_concat(filename + '.js'))
                            .pipe(gulp.dest(dir_dist));
                    });
                }
            } else {
                if(option === 'min') {
                    gulp.task('js_' + name + ':compile', function () {
                        return gulp.src([dir_dev + '/**/*.js'])
                            .pipe(gulp_rename(function (file) {
                                if (file.basename.endsWith('.min')) {
                                    file.basename = file.basename.slice(0, -4) + '.min';
                                } else {
                                    file.basename += '.min';
                                }
                            }))
                            .pipe(gulp_uglify())
                            .pipe(gulp.dest(dir_dist));
                    });
                } else {
                    gulp.task('js_' + name + ':compile', function () {
                        return gulp.src([dir_dev + '/**/*.js'])
                            .pipe(gulp_rename(function (file) {
                                if (file.basename.endsWith('.min')) {
                                    file.basename = file.basename.slice(0, -4) + '';
                                } else {
                                    file.basename += '';
                                }
                            }))
                            .pipe(gulp.dest(dir_dist));
                    });
                }
            }
        },
        watch: function (name, dir_dev, type) {
            gulp.task(type + '_' + name + ':watch', function () {
                gulp.watch(dir_dev + '/**/*.' + type, [type + '_' + name + ':compile']);
            });
        },
        clearCache: function (name) {
            gulp.task(name + ':clear', function (callback) {
                return gulp_cache.clearAll(callback);
            });
        },
        clean: function (name, dir_dist) {
            gulp.task(name + ':clean', function () {
                return del.sync(dir_dist);
            });
        },
        runSequence: function (data) {
            var name = data.name;
            var tasks = data.tasks;
            gulp.task(name, function (callback) {
                run_sequence(tasks, callback);
            });
        },
        actionSCSS: function () {
            var self = this,
                task = self.listSCSS(),
                compile_list = [],
                watch_list = [];

            for (var i = 0; i < task.length; i++) {
                self.compileSCSS(task[i].name, task[i].dir_dev, task[i].dir_dist);
                self.watch(task[i].name, task[i].dir_dev, 'scss');
                self.clean('scss_' + task[i].name, task[i].dir_dist);
                compile_list.push('scss_' + task[i].name + ':compile');
                watch_list.push('scss_' + task[i].name + ':watch');
                clean_list.push('scss_' + task[i].name + ':clean');
            }

            self.runSequence({
                'name': 'scss:compile',
                'tasks': compile_list
            });

            self.runSequence({
                'name': 'scss:watch',
                'tasks': watch_list
            });
        },
        actionJS: function () {
            var self = this,
                task = self.listJS(),
                compile_list = [],
                watch_list = [];

            for (var i = 0; i < task.length; i++) {
                self.compileJS(task[i].name, task[i].dir_dev, task[i].dir_dist, task[i].filename);
                self.watch(task[i].name, task[i].dir_dev, 'js');
                self.clean('js_' + task[i].name, task[i].dir_dist);
                compile_list.push('js_' + task[i].name + ':compile');
                watch_list.push('js_' + task[i].name + ':watch');
                clean_list.push('js_' + task[i].name + ':clean');
            }

            /* MINIFY */
            for (var i = 0; i < task.length; i++) {
                var name = task[i].name + '-min';
                self.compileJS(name, task[i].dir_dev, task[i].dir_dist, task[i].filename, 'min');
                self.watch(name, task[i].dir_dev, 'js');
                self.clean('js_' + name, task[i].dir_dist);
                compile_list.push('js_' + name + ':compile');
                watch_list.push('js_' + name + ':watch');
                clean_list.push('js_' + name + ':clean');
            }

            self.runSequence({
                'name': 'js:compile',
                'tasks': compile_list
            });

            self.runSequence({
                'name': 'js:watch',
                'tasks': watch_list
            });
        }
    };

/* TASK SCSS*/
task.actionSCSS();

/* TASK JS*/
task.actionJS();

/* TASK CLEAR ALL CACHE */
task.clearCache('cache');

/* TASK RUN */
task.runSequence({
    'name': 'run',
    'tasks': [
        'cache:clear', 'scss:watch', 'js:watch'
    ]
});

/* TASK CLEAN ALL FOLDER */
task.runSequence({
    'name': 'all:clean',
    'tasks': clean_list
});

/* TASK BUILD */
task.runSequence({
    'name': 'build',
    'tasks': [
        'all:clean', 'cache:clear', 'scss:compile', 'js:compile'
    ]
});