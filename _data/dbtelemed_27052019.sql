/*
 Navicat MySQL Data Transfer

 Source Server         : SehatQ Live
 Source Server Type    : MySQL
 Source Server Version : 50560
 Source Host           : 103.90.248.107:3306
 Source Schema         : sehatq_telemed_dev

 Target Server Type    : MySQL
 Target Server Version : 50560
 File Encoding         : 65001

 Date: 27/05/2019 09:20:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admins
-- ----------------------------
BEGIN;
INSERT INTO `admins` VALUES (1, 'admin@latitudevp.com', '$2y$10$mXUbBoa/FPa2XnIJRda8a.lXBmNMZ22MpjbkO7vEWHhoAaXqMB3Be', NULL, '2018-09-25 08:58:56', '2018-09-25 08:58:56');
COMMIT;

-- ----------------------------
-- Table structure for chatconsultations
-- ----------------------------
DROP TABLE IF EXISTS `chatconsultations`;
CREATE TABLE `chatconsultations` (
  `chatconsultations_id` int(10) NOT NULL AUTO_INCREMENT,
  `chatconsultations_doctors_id` int(10) NOT NULL,
  `chatconsultations_users_id` int(10) NOT NULL,
  `chatconsultations_summary` text NOT NULL,
  `chatconsultations_chathistory` text NOT NULL,
  `chatconsultations_status` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chatconsultations_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for chathistories
-- ----------------------------
DROP TABLE IF EXISTS `chathistories`;
CREATE TABLE `chathistories` (
  `chathistories_id` int(10) NOT NULL AUTO_INCREMENT,
  `chathistories_user_id` int(10) NOT NULL,
  `chathistories_user_name` varchar(255) NOT NULL,
  `chathistories_status` varchar(200) NOT NULL,
  `chathistories_summary` text NOT NULL,
  `chathistories_call_duration` int(10) NOT NULL DEFAULT '0',
  `chathistories_call_expired_time` datetime DEFAULT NULL,
  `chathistories_medical_report` text NOT NULL,
  `chathistories_rating` decimal(5,2) DEFAULT '0.00',
  `chathistories_comment1` text,
  `chathistories_comment2` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chathistories_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of chathistories
-- ----------------------------
BEGIN;
INSERT INTO `chathistories` VALUES (1, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\",\"Mual\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 12:52:05', '', 2.00, 'Jawaban Tidak Memuaskan', NULL, '2019-05-23 12:51:05', '2019-05-23 19:26:43', NULL);
INSERT INTO `chathistories` VALUES (2, 1059, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":60,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"Gei\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:11:40', '', 4.00, 'Jawaban Dokter Membantu,Diagnosis Tepat', 'GG', '2019-05-23 13:10:40', '2019-05-23 13:11:45', NULL);
INSERT INTO `chathistories` VALUES (3, 1059, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":60,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Gatal\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"Asd\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:14:40', '', 2.00, 'GG,TQ', 'MATA PANCING', '2019-05-23 13:13:40', '2019-05-23 19:05:08', NULL);
INSERT INTO `chathistories` VALUES (4, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\",\"Gatal\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"test dong\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:38:41', '', 0.00, NULL, NULL, '2019-05-23 13:37:41', '2019-05-23 13:38:25', NULL);
INSERT INTO `chathistories` VALUES (5, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\",\"Gatal\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"test dong\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:38:41', '', 0.00, NULL, NULL, '2019-05-23 13:37:41', '2019-05-24 09:48:27', NULL);
INSERT INTO `chathistories` VALUES (6, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\",\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"test\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:43:45', '', 0.00, NULL, NULL, '2019-05-23 13:42:45', '2019-05-23 13:43:02', NULL);
INSERT INTO `chathistories` VALUES (7, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Maag\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:45:25', '', 0.00, NULL, NULL, '2019-05-23 13:44:25', '2019-05-23 13:44:37', NULL);
INSERT INTO `chathistories` VALUES (8, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 13:48:03', '', 0.00, NULL, NULL, '2019-05-23 13:47:03', '2019-05-23 13:47:10', NULL);
INSERT INTO `chathistories` VALUES (9, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Maag\",\"Gatal\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 14:08:00', '', 5.00, NULL, NULL, '2019-05-23 14:07:00', '2019-05-24 14:45:50', NULL);
INSERT INTO `chathistories` VALUES (10, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\",\"Mual\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 14:27:18', '', 0.00, NULL, NULL, '2019-05-23 14:26:18', '2019-05-23 14:27:09', NULL);
INSERT INTO `chathistories` VALUES (11, 3, 'test', 'done', '{\"name\":\"test\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"39\",\"height\":11,\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"2 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 14:34:45', '', 0.00, NULL, NULL, '2019-05-23 14:33:45', '2019-05-23 14:55:48', NULL);
INSERT INTO `chathistories` VALUES (12, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 15:15:27', '', 1.00, NULL, NULL, '2019-05-23 15:14:27', '2019-05-23 15:17:34', NULL);
INSERT INTO `chathistories` VALUES (13, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\",\"Sakit mata\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 15:42:37', '', 0.00, NULL, NULL, '2019-05-23 15:41:37', '2019-05-24 09:46:21', NULL);
INSERT INTO `chathistories` VALUES (14, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 15:47:37', '', 5.00, 'Informasi Jelas', NULL, '2019-05-23 15:46:37', '2019-05-23 18:37:02', NULL);
INSERT INTO `chathistories` VALUES (15, 812, 'Jorgie', 'done', '{\"name\":\"Jorgie\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"26\",\"height\":180,\"weight\":90,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"20 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:18:47', '', 0.00, NULL, NULL, '2019-05-23 16:17:47', '2019-05-23 16:19:44', NULL);
INSERT INTO `chathistories` VALUES (16, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:21:10', '', 0.00, NULL, NULL, '2019-05-23 16:20:10', '2019-05-23 16:21:09', NULL);
INSERT INTO `chathistories` VALUES (17, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:22:37', '', 0.00, NULL, NULL, '2019-05-23 16:21:37', '2019-05-23 16:23:29', NULL);
INSERT INTO `chathistories` VALUES (18, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:24:57', '', 0.00, NULL, NULL, '2019-05-23 16:23:57', '2019-05-23 16:25:42', NULL);
INSERT INTO `chathistories` VALUES (19, 3, 'test', 'done', '{\"name\":\"test\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"39\",\"height\":11,\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"2 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:26:54', '', 0.00, NULL, NULL, '2019-05-23 16:25:54', '2019-05-23 16:29:27', NULL);
INSERT INTO `chathistories` VALUES (20, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:27:00', '', 0.00, NULL, NULL, '2019-05-23 16:26:00', '2019-05-23 16:28:53', NULL);
INSERT INTO `chathistories` VALUES (21, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:30:45', '', 0.00, NULL, NULL, '2019-05-23 16:29:45', '2019-05-23 16:32:09', NULL);
INSERT INTO `chathistories` VALUES (22, 3, 'test', 'done', '{\"name\":\"test\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"39\",\"height\":11,\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"3 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:30:57', '', 0.00, NULL, NULL, '2019-05-23 16:29:57', '2019-05-23 16:30:37', NULL);
INSERT INTO `chathistories` VALUES (23, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:33:36', '', 0.00, NULL, NULL, '2019-05-23 16:32:36', '2019-05-23 16:34:04', NULL);
INSERT INTO `chathistories` VALUES (24, 812, 'Sandro', 'done', '{\"name\":\"Sandro\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":180,\"weight\":80,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"30 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:45:51', '', 0.00, NULL, NULL, '2019-05-23 16:44:51', '2019-05-23 19:42:32', NULL);
INSERT INTO `chathistories` VALUES (25, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 16:46:58', '', 0.00, NULL, NULL, '2019-05-23 16:45:58', '2019-05-23 16:47:09', NULL);
INSERT INTO `chathistories` VALUES (26, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 17:44:27', '', 0.00, NULL, NULL, '2019-05-23 17:43:27', '2019-05-23 17:44:09', NULL);
INSERT INTO `chathistories` VALUES (27, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 17:46:48', '', 0.00, NULL, NULL, '2019-05-23 17:45:48', '2019-05-23 17:46:21', NULL);
INSERT INTO `chathistories` VALUES (28, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 17:49:14', '', 0.00, NULL, NULL, '2019-05-23 17:48:14', '2019-05-23 17:48:46', NULL);
INSERT INTO `chathistories` VALUES (29, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 18:01:07', '', 0.00, NULL, NULL, '2019-05-23 18:00:07', '2019-05-23 18:00:27', NULL);
INSERT INTO `chathistories` VALUES (30, 923, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":62,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 18:51:36', '', 0.00, NULL, NULL, '2019-05-23 18:50:36', '2019-05-23 18:52:21', NULL);
INSERT INTO `chathistories` VALUES (31, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 18:56:27', '', 0.00, NULL, NULL, '2019-05-23 18:55:27', '2019-05-23 18:57:44', NULL);
INSERT INTO `chathistories` VALUES (32, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 18:59:44', '', 0.00, NULL, NULL, '2019-05-23 18:58:44', '2019-05-23 19:02:37', NULL);
INSERT INTO `chathistories` VALUES (33, 1059, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":60,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"123 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 19:01:24', '', 0.00, NULL, NULL, '2019-05-23 19:00:24', '2019-05-23 19:01:17', NULL);
INSERT INTO `chathistories` VALUES (34, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 19:17:01', '', 0.00, NULL, NULL, '2019-05-23 19:16:01', '2019-05-23 19:17:25', NULL);
INSERT INTO `chathistories` VALUES (35, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Maag\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 19:22:46', '', 0.00, NULL, NULL, '2019-05-23 19:21:46', '2019-05-23 19:23:22', NULL);
INSERT INTO `chathistories` VALUES (36, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 19:25:09', '', 0.00, NULL, NULL, '2019-05-23 19:24:09', '2019-05-23 19:26:36', NULL);
INSERT INTO `chathistories` VALUES (37, 1059, 'Raymond Anggara', 'done', '{\"name\":\"Raymond Anggara\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":162,\"weight\":60,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 19:31:25', '', 5.00, 'Jawaban Dokter Membantu,Informasi Jelas', 'GG TQ', '2019-05-23 19:30:25', '2019-05-23 19:31:28', NULL);
INSERT INTO `chathistories` VALUES (38, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 19:31:29', '', 0.00, NULL, NULL, '2019-05-23 19:30:29', '2019-05-23 19:34:02', NULL);
INSERT INTO `chathistories` VALUES (39, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 20:03:14', '', 0.00, NULL, NULL, '2019-05-23 20:02:14', '2019-05-23 20:04:31', NULL);
INSERT INTO `chathistories` VALUES (40, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 20:08:31', '', 0.00, NULL, NULL, '2019-05-23 20:07:31', '2019-05-23 20:08:48', NULL);
INSERT INTO `chathistories` VALUES (41, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 20:15:47', '', 0.00, NULL, NULL, '2019-05-23 20:14:47', '2019-05-23 20:16:09', NULL);
INSERT INTO `chathistories` VALUES (42, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 20:50:56', '', 0.00, NULL, NULL, '2019-05-23 20:49:56', '2019-05-23 20:51:02', NULL);
INSERT INTO `chathistories` VALUES (43, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 20:55:02', '', 0.00, NULL, NULL, '2019-05-23 20:54:02', '2019-05-23 20:56:02', NULL);
INSERT INTO `chathistories` VALUES (44, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 20:59:16', '', 0.00, NULL, NULL, '2019-05-23 20:58:16', '2019-05-23 20:58:30', NULL);
INSERT INTO `chathistories` VALUES (45, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 21:01:54', '', 0.00, NULL, NULL, '2019-05-23 21:00:54', '2019-05-23 21:01:07', NULL);
INSERT INTO `chathistories` VALUES (46, 325, 'joshua setiawan', 'done', '{\"name\":\"joshua setiawan\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"18\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 21:14:22', '', 0.00, NULL, NULL, '2019-05-23 21:13:22', '2019-05-23 21:14:47', NULL);
INSERT INTO `chathistories` VALUES (47, 325, 'joshua setiawan', 'done', '{\"name\":\"joshua setiawan\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"18\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 21:17:03', '', 0.00, NULL, NULL, '2019-05-23 21:16:03', '2019-05-23 21:16:27', NULL);
INSERT INTO `chathistories` VALUES (48, 325, 'joshua setiawan', 'done', '{\"name\":\"joshua setiawan\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"18\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 21:21:47', '', 0.00, NULL, NULL, '2019-05-23 21:20:47', '2019-05-23 21:22:49', NULL);
INSERT INTO `chathistories` VALUES (49, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\",\"Muntah\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-23 21:52:46', '', 0.00, NULL, NULL, '2019-05-23 21:51:46', '2019-05-24 09:46:17', NULL);
INSERT INTO `chathistories` VALUES (50, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Pusing\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 00:03:01', '', 0.00, NULL, NULL, '2019-05-24 00:02:01', '2019-05-24 00:03:15', NULL);
INSERT INTO `chathistories` VALUES (51, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 00:04:56', '', 0.00, NULL, NULL, '2019-05-24 00:03:56', '2019-05-24 00:04:02', NULL);
INSERT INTO `chathistories` VALUES (52, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 00:19:56', '', 0.00, NULL, NULL, '2019-05-24 00:18:56', '2019-05-24 00:19:05', NULL);
INSERT INTO `chathistories` VALUES (53, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 00:21:30', '', 0.00, NULL, NULL, '2019-05-24 00:20:30', '2019-05-24 00:20:41', NULL);
INSERT INTO `chathistories` VALUES (54, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 00:52:09', '', 0.00, NULL, NULL, '2019-05-24 00:51:09', '2019-05-24 00:51:23', NULL);
INSERT INTO `chathistories` VALUES (55, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 00:55:56', '', 0.00, NULL, NULL, '2019-05-24 00:54:56', '2019-05-24 00:55:02', NULL);
INSERT INTO `chathistories` VALUES (56, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Muntah\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:25:39', '', 0.00, NULL, NULL, '2019-05-24 09:24:39', '2019-05-24 09:25:28', NULL);
INSERT INTO `chathistories` VALUES (57, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:39:26', '', 0.00, NULL, NULL, '2019-05-24 09:38:26', '2019-05-24 09:38:41', NULL);
INSERT INTO `chathistories` VALUES (58, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Kembung\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:40:52', '', 0.00, NULL, NULL, '2019-05-24 09:39:52', '2019-05-24 09:39:59', NULL);
INSERT INTO `chathistories` VALUES (59, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"12 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:45:24', '', 0.00, NULL, NULL, '2019-05-24 09:44:24', '2019-05-24 09:44:31', NULL);
INSERT INTO `chathistories` VALUES (60, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Maag\",\"Diare\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:46:55', '', 0.00, NULL, NULL, '2019-05-24 09:45:55', '2019-05-24 09:46:57', NULL);
INSERT INTO `chathistories` VALUES (61, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\",\"Batuk berdahak\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:56:27', '', 0.00, NULL, NULL, '2019-05-24 09:55:27', '2019-05-24 09:56:40', NULL);
INSERT INTO `chathistories` VALUES (62, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\",\"Kembung\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 09:59:35', '', 0.00, NULL, NULL, '2019-05-24 09:58:35', '2019-05-24 10:21:11', NULL);
INSERT INTO `chathistories` VALUES (63, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Diare\",\"Pilek\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 10:28:47', '', 0.00, NULL, NULL, '2019-05-24 10:27:47', '2019-05-24 10:28:21', NULL);
INSERT INTO `chathistories` VALUES (64, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\",\"Muntah\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 10:35:10', '', 0.00, NULL, NULL, '2019-05-24 10:34:10', '2019-05-24 10:52:21', NULL);
INSERT INTO `chathistories` VALUES (65, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\",\"Sakit pinggang\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 10:53:13', '', 0.00, NULL, NULL, '2019-05-24 10:52:13', '2019-05-24 10:55:03', NULL);
INSERT INTO `chathistories` VALUES (66, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 11:04:24', '', 0.00, NULL, NULL, '2019-05-24 11:03:24', '2019-05-24 16:21:27', NULL);
INSERT INTO `chathistories` VALUES (67, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 11:10:10', '', 0.00, NULL, NULL, '2019-05-24 11:09:10', '2019-05-24 11:09:26', NULL);
INSERT INTO `chathistories` VALUES (68, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 11:12:21', '', 0.00, NULL, NULL, '2019-05-24 11:11:21', '2019-05-24 11:11:31', NULL);
INSERT INTO `chathistories` VALUES (69, 999, 'Asd', 'done', '{\"name\":\"Asd\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"121\",\"height\":1,\"weight\":1,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\",\"Maag\"],\"duration\":\"1 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil?\",\"desc\":\"Ya\"}],\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 11:12:53', '', 0.00, NULL, NULL, '2019-05-24 11:11:53', '2019-05-24 11:13:19', NULL);
INSERT INTO `chathistories` VALUES (70, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\"],\"duration\":\"1 Hari\",\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 11:14:20', '', 4.00, 'Jawaban Dokter Membantu', 'Asd', '2019-05-24 11:13:20', '2019-05-24 11:13:46', NULL);
INSERT INTO `chathistories` VALUES (71, 999, 'Jak Wazowsky', 'accepted', '{\"name\":\"Jak Wazowsky\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\"],\"duration\":\"1 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil?\",\"desc\":\"Tidak Yakin\"}],\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 11:20:53', '', 0.00, NULL, NULL, '2019-05-24 11:19:53', '2019-05-24 11:19:53', NULL);
INSERT INTO `chathistories` VALUES (72, 713, 'List1234567', 'done', '{\"name\":\"List1234567\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":170,\"weight\":50,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"21 Hari\",\"symptomsDetail\":\"\",\"allergies\":[\"NO DATA\"]}', 0, '2019-05-24 11:34:02', '', 0.00, NULL, NULL, '2019-05-24 11:33:02', '2019-05-24 16:21:23', NULL);
INSERT INTO `chathistories` VALUES (73, 972, 'Bagus', 'done', '{\"name\":\"Bagus\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558677203.jpeg\",\"age\":\"22\",\"height\":180,\"weight\":77,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Muntah\"],\"duration\":\"2 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:02:31', '', 5.00, NULL, NULL, '2019-05-24 16:01:31', '2019-05-24 16:02:58', NULL);
INSERT INTO `chathistories` VALUES (74, 1071, 'Yosua Ian', 'done', '{\"name\":\"Yosua Ian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"23\",\"height\":22,\"weight\":22,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\"],\"duration\":\"2 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:07:58', '', 5.00, NULL, NULL, '2019-05-24 16:06:58', '2019-05-24 16:08:20', NULL);
INSERT INTO `chathistories` VALUES (75, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sesak napas\"],\"duration\":\"1 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:11:48', '', 5.00, NULL, NULL, '2019-05-24 16:10:48', '2019-05-24 16:12:15', NULL);
INSERT INTO `chathistories` VALUES (76, 972, 'Bagus', 'done', '{\"name\":\"Bagus\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558677203.jpeg\",\"age\":\"22\",\"height\":180,\"weight\":77,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"2 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:13:08', '', 5.00, NULL, NULL, '2019-05-24 16:12:08', '2019-05-24 16:14:24', NULL);
INSERT INTO `chathistories` VALUES (77, 1080, 'Yosua Ian', 'done', '{\"name\":\"Yosua Ian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Demam\"],\"duration\":\"2 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:15:15', '', 5.00, NULL, NULL, '2019-05-24 16:14:15', '2019-05-24 16:25:22', NULL);
INSERT INTO `chathistories` VALUES (78, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\",\"Diare\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:24:38', '', 5.00, NULL, NULL, '2019-05-24 16:23:38', '2019-05-24 16:24:58', NULL);
INSERT INTO `chathistories` VALUES (79, 972, 'Bagus', 'done', '{\"name\":\"Bagus\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558677203.jpeg\",\"age\":\"22\",\"height\":180,\"weight\":77,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"5 Hari\",\"additional\":[],\"symptomsDetail\":\"Ted\",\"allergies\":[]}', 0, '2019-05-24 16:25:29', '', 5.00, NULL, NULL, '2019-05-24 16:24:29', '2019-05-24 16:24:57', NULL);
INSERT INTO `chathistories` VALUES (80, 1080, 'Yosua Ian', 'done', '{\"name\":\"Yosua Ian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Mual\"],\"duration\":\"2 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:26:38', '', 5.00, NULL, NULL, '2019-05-24 16:25:38', '2019-05-24 16:26:50', NULL);
INSERT INTO `chathistories` VALUES (81, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\",\"Pilek\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:28:46', '', 0.00, NULL, NULL, '2019-05-24 16:27:46', '2019-05-24 16:33:34', NULL);
INSERT INTO `chathistories` VALUES (82, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Muntah\",\"Pilek\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:34:58', '', 0.00, NULL, NULL, '2019-05-24 16:33:58', '2019-05-24 16:37:43', NULL);
INSERT INTO `chathistories` VALUES (83, 997, 'Testing', 'accepted', '{\"name\":\"Testing\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"39\",\"height\":170,\"weight\":45,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\"],\"duration\":\"12 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:35:11', '', 0.00, NULL, NULL, '2019-05-24 16:34:11', '2019-05-24 16:34:11', NULL);
INSERT INTO `chathistories` VALUES (84, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"12 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:35:27', '', 0.00, NULL, NULL, '2019-05-24 16:34:27', '2019-05-24 17:30:25', NULL);
INSERT INTO `chathistories` VALUES (85, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Diare\",\"Pilek\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:39:47', '', 0.00, NULL, NULL, '2019-05-24 16:38:47', '2019-05-24 16:44:57', NULL);
INSERT INTO `chathistories` VALUES (86, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Muntah\",\"Sesak napas\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:47:02', '', 5.00, 'Informasi Jelas', NULL, '2019-05-24 16:46:02', '2019-05-24 16:49:49', NULL);
INSERT INTO `chathistories` VALUES (87, 712, 'Listi', 'done', '{\"name\":\"Listi\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"27\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\"],\"duration\":\"16 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:50:25', '', 0.00, NULL, NULL, '2019-05-24 16:49:25', '2019-05-24 17:30:00', NULL);
INSERT INTO `chathistories` VALUES (88, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Maag\",\"Diare\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:56:55', '', 5.00, NULL, NULL, '2019-05-24 16:55:55', '2019-05-24 16:59:15', NULL);
INSERT INTO `chathistories` VALUES (89, 710, 'Listi', 'done', '{\"name\":\"Listi\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":120,\"weight\":12,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Muntah\"],\"duration\":\"1 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 16:58:41', '', 5.00, NULL, NULL, '2019-05-24 16:57:41', '2019-05-24 16:58:54', NULL);
INSERT INTO `chathistories` VALUES (90, 710, 'Listi', 'done', '{\"name\":\"Listi\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":12,\"weight\":12,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"1 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:00:42', '', 5.00, NULL, NULL, '2019-05-24 16:59:42', '2019-05-24 17:01:01', NULL);
INSERT INTO `chathistories` VALUES (91, 710, 'Listi', 'done', '{\"name\":\"Listi\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":12,\"weight\":12,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"12 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:03:42', '', 0.00, NULL, NULL, '2019-05-24 17:02:42', '2019-05-24 17:04:03', NULL);
INSERT INTO `chathistories` VALUES (92, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\",\"Pilek\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:08:40', '', 0.00, NULL, NULL, '2019-05-24 17:07:40', '2019-05-24 17:29:55', NULL);
INSERT INTO `chathistories` VALUES (93, 710, 'Listi', 'done', '{\"name\":\"Listi\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":12,\"weight\":12,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"12 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:10:11', '', 5.00, NULL, '.', '2019-05-24 17:09:11', '2019-05-24 17:10:18', NULL);
INSERT INTO `chathistories` VALUES (94, 710, 'Listi', 'done', '{\"name\":\"Listi\",\"gender\":\"f\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"1 Hari\",\"additional\":[{\"label\":\"Apakah anda hamil ?\",\"desc\":\"Tidak\"}],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:11:55', '', 5.00, NULL, NULL, '2019-05-24 17:10:55', '2019-05-24 17:11:52', NULL);
INSERT INTO `chathistories` VALUES (95, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Lainnya\"],\"duration\":\"12 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:16:51', '', 0.00, NULL, NULL, '2019-05-24 17:15:51', '2019-05-24 17:26:28', NULL);
INSERT INTO `chathistories` VALUES (96, 715, 'Josh KF', 'done', '{\"name\":\"Josh KF\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558633582.png\",\"age\":\"23\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit tenggorokan\"],\"duration\":\"1 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:27:46', '', 5.00, NULL, NULL, '2019-05-24 17:26:46', '2019-05-24 17:27:55', NULL);
INSERT INTO `chathistories` VALUES (97, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\",\"Muntah\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 17:33:30', '', 5.00, NULL, NULL, '2019-05-24 17:32:30', '2019-05-24 18:06:36', NULL);
INSERT INTO `chathistories` VALUES (98, 959, 'Yohanes Adrian', 'done', '{\"name\":\"Yohanes Adrian\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"19\",\"height\":\"0\",\"weight\":\"0\",\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit mata\",\"Muntah\"],\"duration\":\"21 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 18:07:59', '', 5.00, NULL, NULL, '2019-05-24 18:06:59', '2019-05-24 18:07:39', NULL);
INSERT INTO `chathistories` VALUES (99, 972, 'Bagus', 'done', '{\"name\":\"Bagus\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558677203.jpeg\",\"age\":\"22\",\"height\":180,\"weight\":77,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Sakit pinggang\"],\"duration\":\"2 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 18:21:52', '', 5.00, NULL, NULL, '2019-05-24 18:20:52', '2019-05-24 18:22:17', NULL);
INSERT INTO `chathistories` VALUES (100, 972, 'Bagus', 'done', '{\"name\":\"Bagus\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558677203.jpeg\",\"age\":\"22\",\"height\":180,\"weight\":77,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Muntah\"],\"duration\":\"1 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 18:23:37', '', 5.00, NULL, NULL, '2019-05-24 18:22:37', '2019-05-24 18:23:43', NULL);
INSERT INTO `chathistories` VALUES (101, 972, 'Bagus', 'done', '{\"name\":\"Bagus\",\"gender\":\"m\",\"avatarURL\":\"https:\\/\\/stg.sehatq.com\\/public\\/img\\/user_pic\\/1558677203.jpeg\",\"age\":\"22\",\"height\":180,\"weight\":77,\"bloodtype\":\"NO DATA\",\"symptoms\":[\"Batuk berdahak\"],\"duration\":\"12 Hari\",\"additional\":[],\"symptomsDetail\":\"\",\"allergies\":[]}', 0, '2019-05-24 18:25:53', '', 5.00, NULL, NULL, '2019-05-24 18:24:53', '2019-05-24 18:26:26', NULL);
COMMIT;

-- ----------------------------
-- Table structure for chatqueues
-- ----------------------------
DROP TABLE IF EXISTS `chatqueues`;
CREATE TABLE `chatqueues` (
  `chatqueues_id` int(10) NOT NULL AUTO_INCREMENT,
  `chatqueues_user_id` int(10) NOT NULL,
  `chatqueues_user_name` varchar(255) NOT NULL,
  `chatqueues_status` varchar(200) DEFAULT NULL,
  `chatqueues_summary` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chatqueues_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for doctorfields
-- ----------------------------
DROP TABLE IF EXISTS `doctorfields`;
CREATE TABLE `doctorfields` (
  `doctorfields_id` int(10) NOT NULL AUTO_INCREMENT,
  `doctorfields_name` varchar(255) NOT NULL,
  `doctorfields_slug` text NOT NULL,
  `doctorfields_description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`doctorfields_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of doctorfields
-- ----------------------------
BEGIN;
INSERT INTO `doctorfields` VALUES (1, 'Umum', 'umum', '', '2018-11-06 10:24:25', '2019-01-30 15:59:25');
INSERT INTO `doctorfields` VALUES (2, 'Kandungan', 'kandungan', '', '2018-11-06 10:24:39', '2019-04-29 11:48:42');
INSERT INTO `doctorfields` VALUES (3, 'Anak', 'anak', '', '2018-11-06 10:24:48', '2019-04-29 11:46:16');
INSERT INTO `doctorfields` VALUES (4, 'Penyakit Dalam', 'penyakit-dalam', '', '2018-11-06 10:25:01', '2019-01-30 16:00:03');
INSERT INTO `doctorfields` VALUES (5, 'Urologi', 'urologi', 'Urologi', '2018-11-06 10:25:12', '2019-05-17 09:32:28');
INSERT INTO `doctorfields` VALUES (6, 'Jantung', 'jantung', '', '2018-11-06 10:25:23', '2019-04-29 11:48:32');
INSERT INTO `doctorfields` VALUES (7, 'Saraf', 'saraf', '', '2018-11-06 10:25:34', '2019-01-30 16:00:46');
INSERT INTO `doctorfields` VALUES (9, 'Mata', 'mata', '', '2018-11-06 10:25:53', '2019-04-29 11:48:57');
INSERT INTO `doctorfields` VALUES (10, 'Lainnya', 'lainnya', '', '2018-11-06 10:26:08', '2019-01-30 17:29:03');
INSERT INTO `doctorfields` VALUES (11, 'THT', 'tht', '', '2018-11-09 03:18:07', '2019-04-29 11:49:25');
INSERT INTO `doctorfields` VALUES (12, 'Paru', 'paru', '', '2018-11-12 04:01:58', '2019-01-30 16:01:51');
INSERT INTO `doctorfields` VALUES (13, 'Bedah', 'bedah', '', '2018-11-12 07:17:02', '2019-04-29 11:47:41');
INSERT INTO `doctorfields` VALUES (14, 'Anestesi', 'anestesi', '', '2018-11-12 08:05:46', '2019-01-30 16:02:24');
INSERT INTO `doctorfields` VALUES (15, 'Jiwa', 'jiwa', '', '2018-11-12 08:10:39', '2019-01-30 16:02:43');
INSERT INTO `doctorfields` VALUES (16, 'Akupuntur', 'akupuntur', '', '2018-11-12 08:11:22', '2019-01-30 16:02:55');
INSERT INTO `doctorfields` VALUES (17, 'Hewan', 'hewan', '', '2018-11-12 08:12:01', '2019-01-30 16:03:09');
INSERT INTO `doctorfields` VALUES (18, 'Gizi', 'gizi', 'Gizi', '2018-11-12 08:12:45', '2019-05-16 13:34:05');
INSERT INTO `doctorfields` VALUES (19, 'Fisioterapi', 'fisioterapi', '', '2018-11-12 08:13:09', '2019-01-30 16:03:38');
INSERT INTO `doctorfields` VALUES (20, 'Ortopedi', 'ortopedi', '', '2018-11-12 08:13:49', '2019-01-30 16:03:54');
INSERT INTO `doctorfields` VALUES (21, 'Endokrin', 'endokrin', '', '2018-11-12 08:14:06', '2019-01-30 16:04:08');
INSERT INTO `doctorfields` VALUES (22, 'Ginjal', 'ginjal', '', '2018-11-12 08:14:23', '2019-01-30 16:04:24');
INSERT INTO `doctorfields` VALUES (23, 'Kulit', 'kulit', '', '2018-11-12 08:14:41', '2019-01-30 16:04:42');
INSERT INTO `doctorfields` VALUES (24, 'Radiologi', 'radiologi', '', '2018-11-12 08:14:58', '2019-01-30 16:04:59');
INSERT INTO `doctorfields` VALUES (25, 'Bedah Anak', 'bedah-anak', '', '2018-11-12 08:15:23', '2019-01-30 16:05:14');
INSERT INTO `doctorfields` VALUES (26, 'Gastroenterologi', 'gastroenterologi', '', '2018-11-12 08:15:54', '2019-01-30 16:05:29');
INSERT INTO `doctorfields` VALUES (27, 'Rehabilitasi Medis', 'rehabilitasi-medis', '', '2018-11-12 08:16:34', '2019-01-30 16:05:44');
INSERT INTO `doctorfields` VALUES (28, 'Bedah Plastik', 'bedah-plastik', '', '2018-11-12 08:17:25', '2019-01-30 16:05:56');
INSERT INTO `doctorfields` VALUES (29, 'Onkologi', 'onkologi', '', '2018-11-12 08:17:50', '2019-01-30 16:06:12');
INSERT INTO `doctorfields` VALUES (30, 'Reumatologi', 'reumatologi', '', '2018-11-12 08:18:28', '2019-01-30 16:06:26');
INSERT INTO `doctorfields` VALUES (31, 'Psikolog', 'psikolog', 'Psikolog', '2018-11-12 08:19:24', '2019-05-15 08:59:46');
COMMIT;

-- ----------------------------
-- Table structure for doctorshifts
-- ----------------------------
DROP TABLE IF EXISTS `doctorshifts`;
CREATE TABLE `doctorshifts` (
  `doctorshifts_id` int(10) NOT NULL AUTO_INCREMENT,
  `doctorshifts_time` varchar(50) NOT NULL,
  `doctorshifts_time_start` varchar(5) DEFAULT NULL,
  `doctorshifts_time_end` varchar(5) DEFAULT NULL,
  `doctorshifts_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`doctorshifts_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of doctorshifts
-- ----------------------------
BEGIN;
INSERT INTO `doctorshifts` VALUES (1, '06.00 - 10.00', '06.00', '10.00', 1, '2019-05-17 04:41:11', '2019-05-19 21:47:15', 'Rheza Dokter', 'Rheza Dokter', NULL);
INSERT INTO `doctorshifts` VALUES (2, '17.00 - 18.00', '17.00', '18.00', 1, '2019-05-17 04:41:26', '2019-05-17 04:41:26', 'Rheza Dokter', '', NULL);
INSERT INTO `doctorshifts` VALUES (3, '18.00 - 19.00', '18.00', '19.00', 1, '2019-05-17 04:41:50', '2019-05-17 04:41:50', 'Rheza Dokter', '', NULL);
INSERT INTO `doctorshifts` VALUES (4, '18.00 - 22.00', '18.00', '22.00', 0, '2019-05-17 04:42:00', '2019-05-20 08:15:49', 'Rheza Dokter', 'Rheza Dokter', NULL);
INSERT INTO `doctorshifts` VALUES (5, '06.50 - 10.30', '06.50', '10.30', 0, '2019-05-19 20:05:50', '2019-05-19 21:45:36', 'Rheza Dokter', 'Rheza Dokter', NULL);
INSERT INTO `doctorshifts` VALUES (6, '00.00 - 05.00', '00.00', '05.00', 0, '2019-05-19 21:48:14', '2019-05-20 08:15:58', 'Rheza Dokter', 'Rheza Dokter', NULL);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_08_04_045017_create_admins_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_09_24_162054_create_tbl_userdetails', 2);
INSERT INTO `migrations` VALUES (5, '2018_09_24_162256_create_tbl_privateinsurance', 2);
COMMIT;

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
INSERT INTO `password_resets` VALUES ('rheza.s@gmail.com', '$2y$10$d9TAypsZNdSpJDioeWrag.esiwGnPYBRKdEj2dBAYlcwpua3ucvCO', '2018-11-12 10:55:13');
INSERT INTO `password_resets` VALUES ('Reni.aff@gmail.com', '$2y$10$JIeD5/vlrT1n4GOHo/I1Vurk5H2a7vqEhF8GIIO7DyJdr/Pz8ZJ06', '2018-11-13 03:12:11');
INSERT INTO `password_resets` VALUES ('erika.zhank@gmail.com', '$2y$10$GGkkSOxaDIPYg3rs83Bzh.g4tRaJjfmlDR/JQGUyle.6E/kLThxzq', '2018-11-14 02:55:15');
INSERT INTO `password_resets` VALUES ('qwerty@gmail.com', '$2y$10$j5fUrmGN1t.6G3YbNPRXOu1QxWLpAdPKgnBTxo1.gA8O1JONqniFC', '2018-11-14 02:57:16');
INSERT INTO `password_resets` VALUES ('felix@latitudevp.com', '$2y$10$HHiax0Km7SI4k7THo0GjS.KXBF81jYXc5XKPg0sxtCitPhSRkZV8.', '2018-11-14 02:57:58');
INSERT INTO `password_resets` VALUES ('rheza@latitudevp.com', '$2y$10$35N92gAbvu.8Fc7Slv6g5uVEsSUIgNXMn5LmftViF831DtWtzoF6y', '2019-02-07 10:40:14');
COMMIT;

-- ----------------------------
-- Table structure for pv_users_doctorshifts
-- ----------------------------
DROP TABLE IF EXISTS `pv_users_doctorshifts`;
CREATE TABLE `pv_users_doctorshifts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_users_id` int(10) NOT NULL,
  `doctorshift_doctorshifts_id` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pv_users_doctorshifts
-- ----------------------------
BEGIN;
INSERT INTO `pv_users_doctorshifts` VALUES (1, 3, 1, '2019-05-21');
INSERT INTO `pv_users_doctorshifts` VALUES (2, 6, 2, '2019-05-21');
INSERT INTO `pv_users_doctorshifts` VALUES (3, 7, 3, '2019-05-21');
INSERT INTO `pv_users_doctorshifts` VALUES (4, 3, 2, '2019-05-21');
INSERT INTO `pv_users_doctorshifts` VALUES (5, 6, 3, '2019-05-21');
COMMIT;

-- ----------------------------
-- Table structure for userdetails
-- ----------------------------
DROP TABLE IF EXISTS `userdetails`;
CREATE TABLE `userdetails` (
  `userdetails_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userdetails_users_id` int(10) NOT NULL,
  `userdetails_doctorfields_id` int(10) NOT NULL,
  `userdetails_doctortitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userdetails_description` longtext COLLATE utf8mb4_unicode_ci,
  `userdetails_experience` date DEFAULT NULL,
  `userdetails_contact` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userdetails_str` tinyint(1) NOT NULL DEFAULT '0',
  `userdetails_sip` tinyint(1) NOT NULL DEFAULT '0',
  `userdetails_comment` longtext COLLATE utf8mb4_unicode_ci,
  `userdetails_sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'm',
  `userdetails_rating` decimal(5,2) DEFAULT '0.00',
  `userdetails_feedback_count` int(11) DEFAULT '0',
  `userdetails_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userdetails_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of userdetails
-- ----------------------------
BEGIN;
INSERT INTO `userdetails` VALUES (1, 1, 8, NULL, '', '1986-05-14', '+62-023495345', 0, 0, 'Superadmin', 'm', 0.00, 0, NULL, '2018-09-27 00:00:00', '2018-10-02 16:02:27', NULL);
INSERT INTO `userdetails` VALUES (2, 2, 2, NULL, NULL, '1980-04-16', '+65-0894293424', 0, 0, 'komentar...', 'm', 0.00, 0, NULL, '2018-10-01 00:00:00', '2019-05-18 08:36:08', NULL);
INSERT INTO `userdetails` VALUES (3, 3, 1, 'Prof.', NULL, '2019-05-23', '+65-94053456', 0, 0, 'Remarks example...', 'm', 0.00, 0, 'rheza-dokter_1558138267.jpg', '2018-10-02 09:06:46', '2019-05-23 14:00:18', NULL);
INSERT INTO `userdetails` VALUES (5, 5, 1, NULL, 'dokter umum', '2010-10-19', '081231434', 1, 0, 'keterangan dokter', 'f', 0.00, 0, '', '2019-05-16 14:43:31', '2019-05-16 14:43:51', '2019-05-16 14:43:51');
INSERT INTO `userdetails` VALUES (6, 6, 4, 'Dr.', 'tes dokter 2', '2019-05-23', '0823425', 0, 0, 'tes 2', 'm', 5.00, 43, 'tes2_1558138323.jpg', '2019-05-16 14:47:16', '2019-05-24 18:26:26', NULL);
INSERT INTO `userdetails` VALUES (7, 7, 3, 'Dr.', NULL, '2019-05-27', '08170020125', 1, 1, 'test', 'f', 0.00, 0, NULL, '2019-05-21 13:43:13', '2019-05-27 09:09:48', NULL);
INSERT INTO `userdetails` VALUES (8, 8, 3, 'Dr.', NULL, '2019-05-23', '0987655', 1, 1, NULL, 'm', 0.00, 0, 'handsome_1558521043.png', '2019-05-22 17:30:43', '2019-05-23 13:57:01', NULL);
COMMIT;

-- ----------------------------
-- Table structure for userpermissions
-- ----------------------------
DROP TABLE IF EXISTS `userpermissions`;
CREATE TABLE `userpermissions` (
  `userpermissions_id` int(10) NOT NULL AUTO_INCREMENT,
  `userpermissions_usertypes_id` int(10) NOT NULL,
  `userpermissions_name` varchar(255) NOT NULL,
  `userpermissions_link` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userpermissions_id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of userpermissions
-- ----------------------------
BEGIN;
INSERT INTO `userpermissions` VALUES (15, 4, 'patient', 'admin/patient', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (16, 4, 'disease', 'admin/disease', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (17, 4, 'drug', 'admin/drug', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (18, 4, 'doctor', 'admin/doctor', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (19, 4, 'hospital', 'admin/hospital', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (20, 4, 'healthlab', 'admin/healthlab', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (21, 4, 'article', 'admin/article', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (22, 4, 'forum', 'admin/forum', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (23, 3, 'patient', 'admin/patient', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (24, 3, 'drug', 'admin/drug', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (25, 3, 'doctor', 'admin/doctor', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (26, 3, 'hospital', 'admin/hospital', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (27, 3, 'healthtool', 'admin/healthtool', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (28, 3, 'forum', 'admin/forum', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (56, 2, 'disease', 'admin/disease', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (57, 2, 'drug', 'admin/drug', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (58, 2, 'doctor', 'admin/doctor', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (59, 2, 'article', 'admin/article', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (60, 2, 'forum', 'admin/forum', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (132, 10, 'article', 'admin/article', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (133, 10, 'disease', 'admin/disease', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (134, 10, 'drug', 'admin/drug', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (135, 10, 'askdoctor', 'admin/askdoctor', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (136, 10, 'booking', 'admin/booking', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (137, 10, 'event', 'admin/event', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (138, 10, 'promotion', 'admin/promotion', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (139, 10, 'doctor', 'admin/doctor', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (140, 10, 'hospital', 'admin/hospital', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (141, 10, 'patient', 'admin/patient', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (142, 10, 'insurance', 'admin/insurance', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (143, 10, 'cmscareers', 'admin/career', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (144, 10, 'setting', 'admin/setting', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (145, 10, 'member', 'admin/member', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (146, 10, 'systemsetting', 'admin/systemsetting', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (147, 1, 'article', 'admin/article', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (148, 1, 'disease', 'admin/disease', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (149, 1, 'drug', 'admin/drug', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (150, 1, 'askdoctor', 'admin/askdoctor', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (151, 1, 'booking', 'admin/booking', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (152, 1, 'event', 'admin/event', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (153, 1, 'promotion', 'admin/promotion', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (154, 1, 'doctor', 'admin/doctor', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (155, 1, 'hospital', 'admin/hospital', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (156, 1, 'patient', 'admin/patient', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (157, 1, 'insurance', 'admin/insurance', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (158, 1, 'cmscareers', 'admin/career', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (159, 1, 'setting', 'admin/setting', '2019-03-22 11:27:14', '2019-03-22 11:27:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` int(10) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Superadmin', 'superadmin@sehatq.com', '$2y$10$ZnVSzKry65G2t6kh.px1BeMm0rataY0muV33pIRC0uv3YlmXSWTxO', NULL, 'Ww3jbQRH44UJmCo7roWUCt2bqpmwsn8KPq5Qcr3T3xaNbZCKsMd6nG1oASm8', 1, 1, 1, '2019-05-20 20:43:11', '2018-09-25 09:35:50', '2019-05-20 20:43:11', NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'Rheza Sindhuwinata', 'rheza@sehatq.com', '$2y$10$93zj5OOy8JsR2HmHLbiF5u2cby79u8wnW0mF6VOjVFtt0/QHX7NEK', NULL, 'lwf1atGedftauZLixRpPb1551bOyMHuTrfqOmK3m7afr2CRV2vxGmHHsDSKL', 2, 1, 1, '2019-05-22 17:30:57', '2018-09-27 02:27:14', '2019-05-22 17:30:57', NULL, NULL, NULL);
INSERT INTO `users` VALUES (3, 'Prof. Dave, Sp. OG', 'david@sehatq.com', '$2y$10$DSRSI9icAMhV9afiLhTK5OllajLM.EbFEiqXlB9Fvss99Hy1PwYL2', NULL, 'ChAxB2r3vhB8qDWHOdoYJYDG4dqD3FuXFkXaLiWlQped0Z6erCg3zuuuu28N', 3, 1, 1, '2019-05-20 17:28:45', '2018-09-27 03:08:48', '2019-05-23 14:00:18', NULL, NULL, NULL);
INSERT INTO `users` VALUES (5, 'Test abc', 'abc@abc.com', '$2y$10$GTxuIpdXz5z9O0y.BOuSsusjRbLUFoxodqizvyxeKzpTaGxWwG79K', NULL, NULL, 2, 1, 1, NULL, '2019-05-16 14:43:31', '2019-05-16 14:43:51', NULL, NULL, '2019-05-16 14:43:51');
INSERT INTO `users` VALUES (6, 'Dr. Rheza Dokter', 'rheza.s@gmail.com', '$2y$10$0AbAApQxEwP4QgMjyPvYhOKNTplU6R/zHR8c1HQPWJKL7dPVxtka6', 'EEILxUa5PRhFI5zIFu2CtbYihQO6YQxFxurOZnyIgwRfjqf8lTJaeFL0ydqN', 'VxDzT0hsM6HEqfF1w831kgB42eUnf014jmTeIXdEu4QB175u3GRzXmlNO75S', 3, 1, 1, '2019-05-27 08:45:15', '2019-05-16 14:47:16', '2019-05-27 08:45:15', NULL, NULL, NULL);
INSERT INTO `users` VALUES (7, 'Prof. Tita', 'dania.sadewo@sehatq.com', '$2y$10$OMtCAyuTfNPi3906ZT9YveETyV.NNsFwlH7HUtfR26lA3rlcoVQTy', 'mZm5KPBw2JL3Jd0RMhlXaPFxdnZnP3KP2yxjEFMzMGNHLMEDquS8px5Y8Qii', NULL, 3, 1, 0, '2019-05-21 13:52:15', '2019-05-21 13:43:13', '2019-05-27 09:09:48', NULL, NULL, NULL);
INSERT INTO `users` VALUES (8, 'Dr. Handsome, Sp.S (dr.)', 'test@test.test', '$2y$10$IxPJ8VIS.16Gt4EF6Sl0GuldiIQ4Isl84Qbz2dkXHJ.YdKIJ0/hxi', 'c6lwfLZb4UNaqNXirXS71ArEMlTRawrpgrF6bj325Zr9ViIgnMVaySHKWvqG', NULL, 3, 1, 1, '2019-05-22 18:26:50', '2019-05-22 17:30:43', '2019-05-23 13:57:01', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for usertypes
-- ----------------------------
DROP TABLE IF EXISTS `usertypes`;
CREATE TABLE `usertypes` (
  `usertypes_id` int(10) NOT NULL AUTO_INCREMENT,
  `usertypes_role` varchar(50) NOT NULL,
  `usertypes_description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usertypes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usertypes
-- ----------------------------
BEGIN;
INSERT INTO `usertypes` VALUES (1, 'superadmin', 'Memiliki kontrol penuh terhadap sistem', '2018-10-03 00:00:00', '2018-10-03 00:00:00', NULL);
INSERT INTO `usertypes` VALUES (2, 'admin', 'Dapat menambah atau menghapus akun dokter', '2018-10-03 00:00:00', '2018-10-03 09:24:54', NULL);
INSERT INTO `usertypes` VALUES (3, 'dokter', 'Akun dokter untuk menangani konsultasi', '2018-10-03 00:00:00', '2019-05-19 19:53:11', NULL);
INSERT INTO `usertypes` VALUES (4, 'finance', 'Departemen bagian keuangan dan shift dokter', '2019-05-16 16:00:37', '2019-05-17 09:33:48', NULL);
INSERT INTO `usertypes` VALUES (5, 'Test', 'test', '2019-05-16 16:02:54', '2019-05-16 16:03:26', '2019-05-16 16:03:26');
INSERT INTO `usertypes` VALUES (6, 'testq', 'testq', '2019-05-17 11:43:13', '2019-05-17 11:43:23', '2019-05-17 11:43:23');
INSERT INTO `usertypes` VALUES (11, 'motivator', 'Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !Saya bisa, kamu bisa, semua pasti bisa !', '2019-05-19 19:54:00', '2019-05-19 19:54:43', NULL);
INSERT INTO `usertypes` VALUES (12, 'CS', 'Pew Pew Pew', '2019-05-19 19:55:36', '2019-05-19 19:55:46', '2019-05-19 19:55:46');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
