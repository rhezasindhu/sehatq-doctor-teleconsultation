/*
 Navicat MySQL Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : lvl_sehatq_doctor

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 21/05/2019 09:17:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admins
-- ----------------------------
BEGIN;
INSERT INTO `admins` VALUES (1, 'admin@latitudevp.com', '$2y$10$mXUbBoa/FPa2XnIJRda8a.lXBmNMZ22MpjbkO7vEWHhoAaXqMB3Be', NULL, '2018-09-25 08:58:56', '2018-09-25 08:58:56');
COMMIT;

-- ----------------------------
-- Table structure for chatconsultations
-- ----------------------------
DROP TABLE IF EXISTS `chatconsultations`;
CREATE TABLE `chatconsultations` (
  `chatconsultations_id` int(10) NOT NULL AUTO_INCREMENT,
  `chatconsultations_doctors_id` int(10) NOT NULL,
  `chatconsultations_users_id` int(10) NOT NULL,
  `chatconsultations_summary` text NOT NULL,
  `chatconsultations_chathistory` text NOT NULL,
  `chatconsultations_status` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chatconsultations_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for chathistories
-- ----------------------------
DROP TABLE IF EXISTS `chathistories`;
CREATE TABLE `chathistories` (
  `chathistories_id` int(10) NOT NULL AUTO_INCREMENT,
  `chathistories_user_id` int(10) NOT NULL,
  `chathistories_user_name` varchar(255) NOT NULL,
  `chathistories_status` varchar(200) NOT NULL,
  `chathistories_summary` text NOT NULL,
  `chathistories_call_duration` int(10) NOT NULL DEFAULT '0',
  `chathistories_call_expired_time` datetime DEFAULT NULL,
  `chathistories_medical_report` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chathistories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for chatqueues
-- ----------------------------
DROP TABLE IF EXISTS `chatqueues`;
CREATE TABLE `chatqueues` (
  `chatqueues_id` int(10) NOT NULL AUTO_INCREMENT,
  `chatqueues_user_id` int(10) NOT NULL,
  `chatqueues_user_name` varchar(255) NOT NULL,
  `chatqueues_status` varchar(200) DEFAULT NULL,
  `chatqueues_summary` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`chatqueues_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of chatqueues
-- ----------------------------
BEGIN;
INSERT INTO `chatqueues` VALUES (1, 50, 'Joni', 'waiting', '{\"name\":\"Joni\",\"gender\":\"m\",\"avatarURL\":\"\",\"age\":\"21\",\"height\":\"170\",\"weight\":\"60\",\"bloodtype\":\"o\",\"symptoms\":\"[\\\"a\\\",\\\"b\\\"]\",\"duration\":\"3 minggu\",\"additional\":\"[{\\\"label\\\": \\\"Apakah anda hamil?\\\",\\\"desc\\\" : \\\"......\\\"}]\",\"symptomsDetail\":\"Saya udah tidak tahan lagi\",\"allergies\":\"[\\\"ikan\\\",\\\"telur\\\"]\"}', '2019-05-17 15:11:53', '2019-05-17 15:11:53', NULL);
COMMIT;

-- ----------------------------
-- Table structure for doctorfields
-- ----------------------------
DROP TABLE IF EXISTS `doctorfields`;
CREATE TABLE `doctorfields` (
  `doctorfields_id` int(10) NOT NULL AUTO_INCREMENT,
  `doctorfields_name` varchar(255) NOT NULL,
  `doctorfields_slug` text NOT NULL,
  `doctorfields_description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`doctorfields_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of doctorfields
-- ----------------------------
BEGIN;
INSERT INTO `doctorfields` VALUES (1, 'Umum', 'umum', '', '2018-11-06 10:24:25', '2019-01-30 15:59:25');
INSERT INTO `doctorfields` VALUES (2, 'Kandungan', 'kandungan', '', '2018-11-06 10:24:39', '2019-04-29 11:48:42');
INSERT INTO `doctorfields` VALUES (3, 'Anak', 'anak', '', '2018-11-06 10:24:48', '2019-04-29 11:46:16');
INSERT INTO `doctorfields` VALUES (4, 'Penyakit Dalam', 'penyakit-dalam', '', '2018-11-06 10:25:01', '2019-01-30 16:00:03');
INSERT INTO `doctorfields` VALUES (5, 'Urologi', 'urologi', 'Urologi', '2018-11-06 10:25:12', '2019-05-17 09:32:28');
INSERT INTO `doctorfields` VALUES (6, 'Jantung', 'jantung', '', '2018-11-06 10:25:23', '2019-04-29 11:48:32');
INSERT INTO `doctorfields` VALUES (7, 'Saraf', 'saraf', '', '2018-11-06 10:25:34', '2019-01-30 16:00:46');
INSERT INTO `doctorfields` VALUES (9, 'Mata', 'mata', '', '2018-11-06 10:25:53', '2019-04-29 11:48:57');
INSERT INTO `doctorfields` VALUES (10, 'Lainnya', 'lainnya', '', '2018-11-06 10:26:08', '2019-01-30 17:29:03');
INSERT INTO `doctorfields` VALUES (11, 'THT', 'tht', '', '2018-11-09 03:18:07', '2019-04-29 11:49:25');
INSERT INTO `doctorfields` VALUES (12, 'Paru', 'paru', '', '2018-11-12 04:01:58', '2019-01-30 16:01:51');
INSERT INTO `doctorfields` VALUES (13, 'Bedah', 'bedah', '', '2018-11-12 07:17:02', '2019-04-29 11:47:41');
INSERT INTO `doctorfields` VALUES (14, 'Anestesi', 'anestesi', '', '2018-11-12 08:05:46', '2019-01-30 16:02:24');
INSERT INTO `doctorfields` VALUES (15, 'Jiwa', 'jiwa', '', '2018-11-12 08:10:39', '2019-01-30 16:02:43');
INSERT INTO `doctorfields` VALUES (16, 'Akupuntur', 'akupuntur', '', '2018-11-12 08:11:22', '2019-01-30 16:02:55');
INSERT INTO `doctorfields` VALUES (17, 'Hewan', 'hewan', '', '2018-11-12 08:12:01', '2019-01-30 16:03:09');
INSERT INTO `doctorfields` VALUES (18, 'Gizi', 'gizi', 'Gizi', '2018-11-12 08:12:45', '2019-05-16 13:34:05');
INSERT INTO `doctorfields` VALUES (19, 'Fisioterapi', 'fisioterapi', '', '2018-11-12 08:13:09', '2019-01-30 16:03:38');
INSERT INTO `doctorfields` VALUES (20, 'Ortopedi', 'ortopedi', '', '2018-11-12 08:13:49', '2019-01-30 16:03:54');
INSERT INTO `doctorfields` VALUES (21, 'Endokrin', 'endokrin', '', '2018-11-12 08:14:06', '2019-01-30 16:04:08');
INSERT INTO `doctorfields` VALUES (22, 'Ginjal', 'ginjal', '', '2018-11-12 08:14:23', '2019-01-30 16:04:24');
INSERT INTO `doctorfields` VALUES (23, 'Kulit', 'kulit', '', '2018-11-12 08:14:41', '2019-01-30 16:04:42');
INSERT INTO `doctorfields` VALUES (24, 'Radiologi', 'radiologi', '', '2018-11-12 08:14:58', '2019-01-30 16:04:59');
INSERT INTO `doctorfields` VALUES (25, 'Bedah Anak', 'bedah-anak', '', '2018-11-12 08:15:23', '2019-01-30 16:05:14');
INSERT INTO `doctorfields` VALUES (26, 'Gastroenterologi', 'gastroenterologi', '', '2018-11-12 08:15:54', '2019-01-30 16:05:29');
INSERT INTO `doctorfields` VALUES (27, 'Rehabilitasi Medis', 'rehabilitasi-medis', '', '2018-11-12 08:16:34', '2019-01-30 16:05:44');
INSERT INTO `doctorfields` VALUES (28, 'Bedah Plastik', 'bedah-plastik', '', '2018-11-12 08:17:25', '2019-01-30 16:05:56');
INSERT INTO `doctorfields` VALUES (29, 'Onkologi', 'onkologi', '', '2018-11-12 08:17:50', '2019-01-30 16:06:12');
INSERT INTO `doctorfields` VALUES (30, 'Reumatologi', 'reumatologi', '', '2018-11-12 08:18:28', '2019-01-30 16:06:26');
INSERT INTO `doctorfields` VALUES (31, 'Psikolog', 'psikolog', 'Psikolog', '2018-11-12 08:19:24', '2019-05-15 08:59:46');
INSERT INTO `doctorfields` VALUES (32, 'Others', 'others', 'Lainnya', '2019-05-16 13:30:36', '2019-05-16 13:31:00');
COMMIT;

-- ----------------------------
-- Table structure for doctorshifts
-- ----------------------------
DROP TABLE IF EXISTS `doctorshifts`;
CREATE TABLE `doctorshifts` (
  `doctorshifts_id` int(10) NOT NULL AUTO_INCREMENT,
  `doctorshifts_time` varchar(50) NOT NULL,
  `doctorshifts_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`doctorshifts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of doctorshifts
-- ----------------------------
BEGIN;
INSERT INTO `doctorshifts` VALUES (3, '18.00 - 19.00', 0, '2019-05-17 04:41:50', '2019-05-20 15:26:32', 'Rheza Dokter', 'Rheza Dokter', NULL);
INSERT INTO `doctorshifts` VALUES (4, '19.00 - 20.00', 1, '2019-05-17 04:42:00', '2019-05-17 11:38:30', 'Rheza Dokter', 'Rheza Dokter', NULL);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_08_04_045017_create_admins_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_09_24_162054_create_tbl_userdetails', 2);
INSERT INTO `migrations` VALUES (5, '2018_09_24_162256_create_tbl_privateinsurance', 2);
COMMIT;

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
INSERT INTO `password_resets` VALUES ('Reni.aff@gmail.com', '$2y$10$JIeD5/vlrT1n4GOHo/I1Vurk5H2a7vqEhF8GIIO7DyJdr/Pz8ZJ06', '2018-11-13 03:12:11');
INSERT INTO `password_resets` VALUES ('erika.zhank@gmail.com', '$2y$10$GGkkSOxaDIPYg3rs83Bzh.g4tRaJjfmlDR/JQGUyle.6E/kLThxzq', '2018-11-14 02:55:15');
INSERT INTO `password_resets` VALUES ('qwerty@gmail.com', '$2y$10$j5fUrmGN1t.6G3YbNPRXOu1QxWLpAdPKgnBTxo1.gA8O1JONqniFC', '2018-11-14 02:57:16');
INSERT INTO `password_resets` VALUES ('felix@latitudevp.com', '$2y$10$HHiax0Km7SI4k7THo0GjS.KXBF81jYXc5XKPg0sxtCitPhSRkZV8.', '2018-11-14 02:57:58');
INSERT INTO `password_resets` VALUES ('rheza@latitudevp.com', '$2y$10$35N92gAbvu.8Fc7Slv6g5uVEsSUIgNXMn5LmftViF831DtWtzoF6y', '2019-02-07 10:40:14');
COMMIT;

-- ----------------------------
-- Table structure for pv_users_doctorshifts
-- ----------------------------
DROP TABLE IF EXISTS `pv_users_doctorshifts`;
CREATE TABLE `pv_users_doctorshifts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_users_id` int(10) NOT NULL,
  `doctorshift_doctorshifts_id` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pv_users_doctorshifts
-- ----------------------------
BEGIN;
INSERT INTO `pv_users_doctorshifts` VALUES (1, 3, 1, '2019-05-18');
INSERT INTO `pv_users_doctorshifts` VALUES (6, 2, 3, '2019-05-20');
COMMIT;

-- ----------------------------
-- Table structure for userdetails
-- ----------------------------
DROP TABLE IF EXISTS `userdetails`;
CREATE TABLE `userdetails` (
  `userdetails_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userdetails_users_id` int(10) NOT NULL,
  `userdetails_doctorfields_id` int(10) NOT NULL,
  `userdetails_description` longtext COLLATE utf8mb4_unicode_ci,
  `userdetails_experience` date DEFAULT NULL,
  `userdetails_contact` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userdetails_str` tinyint(1) NOT NULL DEFAULT '0',
  `userdetails_sip` tinyint(1) NOT NULL DEFAULT '0',
  `userdetails_comment` longtext COLLATE utf8mb4_unicode_ci,
  `userdetails_sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'm',
  `userdetails_rating` tinyint(1) DEFAULT '0',
  `userdetails_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userdetails_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of userdetails
-- ----------------------------
BEGIN;
INSERT INTO `userdetails` VALUES (1, 1, 8, '', '1986-05-14', '+62-023495345', 0, 0, 'Superadmin', 'm', 0, NULL, '2018-09-27 00:00:00', '2018-10-02 16:02:27', NULL);
INSERT INTO `userdetails` VALUES (2, 2, 0, NULL, '2019-05-20', '+65-0894293424', 0, 0, 'komentar...', 'm', 0, 'rheza-sindhuw_1558325601.jpg', '2018-10-01 00:00:00', '2019-05-20 11:13:22', NULL);
INSERT INTO `userdetails` VALUES (3, 3, 1, NULL, '1979-10-01', '+65-94053456', 0, 0, 'Remarks example...', 'm', 0, 'rheza-dokter_1558138267.jpg', '2018-10-02 09:06:46', '2019-05-18 07:11:07', NULL);
INSERT INTO `userdetails` VALUES (4, 4, 3, 'Dokter anak di RS Columbia Medan', '1980-05-20', '0219435354', 1, 1, NULL, 'm', 0, NULL, '2019-05-20 13:00:19', '2019-05-20 13:00:19', NULL);
INSERT INTO `userdetails` VALUES (5, 5, 11, 'Dokter THT di RS Columbia Medan', '2008-05-20', '08345944355', 1, 1, NULL, 'm', 0, NULL, '2019-05-20 13:57:57', '2019-05-20 13:57:57', NULL);
INSERT INTO `userdetails` VALUES (6, 6, 1, 'test dokter', '2000-05-20', '0802354534', 0, 0, NULL, 'm', 0, NULL, '2019-05-20 14:07:14', '2019-05-20 14:09:06', '2019-05-20 14:09:06');
COMMIT;

-- ----------------------------
-- Table structure for userpermissions
-- ----------------------------
DROP TABLE IF EXISTS `userpermissions`;
CREATE TABLE `userpermissions` (
  `userpermissions_id` int(10) NOT NULL AUTO_INCREMENT,
  `userpermissions_usertypes_id` int(10) NOT NULL,
  `userpermissions_name` varchar(255) NOT NULL,
  `userpermissions_link` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userpermissions_id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of userpermissions
-- ----------------------------
BEGIN;
INSERT INTO `userpermissions` VALUES (15, 4, 'patient', 'admin/patient', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (16, 4, 'disease', 'admin/disease', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (17, 4, 'drug', 'admin/drug', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (18, 4, 'doctor', 'admin/doctor', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (19, 4, 'hospital', 'admin/hospital', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (20, 4, 'healthlab', 'admin/healthlab', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (21, 4, 'article', 'admin/article', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (22, 4, 'forum', 'admin/forum', '2018-10-04 04:21:34', '2018-10-04 04:21:34', NULL);
INSERT INTO `userpermissions` VALUES (23, 3, 'patient', 'admin/patient', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (24, 3, 'drug', 'admin/drug', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (25, 3, 'doctor', 'admin/doctor', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (26, 3, 'hospital', 'admin/hospital', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (27, 3, 'healthtool', 'admin/healthtool', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (28, 3, 'forum', 'admin/forum', '2018-10-04 04:23:26', '2018-10-04 04:23:26', NULL);
INSERT INTO `userpermissions` VALUES (56, 2, 'disease', 'admin/disease', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (57, 2, 'drug', 'admin/drug', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (58, 2, 'doctor', 'admin/doctor', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (59, 2, 'article', 'admin/article', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (60, 2, 'forum', 'admin/forum', '2018-10-15 02:12:24', '2018-10-15 02:12:24', NULL);
INSERT INTO `userpermissions` VALUES (132, 10, 'article', 'admin/article', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (133, 10, 'disease', 'admin/disease', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (134, 10, 'drug', 'admin/drug', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (135, 10, 'askdoctor', 'admin/askdoctor', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (136, 10, 'booking', 'admin/booking', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (137, 10, 'event', 'admin/event', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (138, 10, 'promotion', 'admin/promotion', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (139, 10, 'doctor', 'admin/doctor', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (140, 10, 'hospital', 'admin/hospital', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (141, 10, 'patient', 'admin/patient', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (142, 10, 'insurance', 'admin/insurance', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (143, 10, 'cmscareers', 'admin/career', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (144, 10, 'setting', 'admin/setting', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (145, 10, 'member', 'admin/member', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (146, 10, 'systemsetting', 'admin/systemsetting', '2019-03-22 11:27:04', '2019-03-22 11:27:04', NULL);
INSERT INTO `userpermissions` VALUES (147, 1, 'article', 'admin/article', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (148, 1, 'disease', 'admin/disease', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (149, 1, 'drug', 'admin/drug', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (150, 1, 'askdoctor', 'admin/askdoctor', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (151, 1, 'booking', 'admin/booking', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (152, 1, 'event', 'admin/event', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (153, 1, 'promotion', 'admin/promotion', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (154, 1, 'doctor', 'admin/doctor', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (155, 1, 'hospital', 'admin/hospital', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (156, 1, 'patient', 'admin/patient', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (157, 1, 'insurance', 'admin/insurance', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (158, 1, 'cmscareers', 'admin/career', '2019-03-22 11:27:13', '2019-03-22 11:27:13', NULL);
INSERT INTO `userpermissions` VALUES (159, 1, 'setting', 'admin/setting', '2019-03-22 11:27:14', '2019-03-22 11:27:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` int(10) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Superadmin', 'superadmin@sehatq.com', '$2y$10$ZnVSzKry65G2t6kh.px1BeMm0rataY0muV33pIRC0uv3YlmXSWTxO', NULL, 'FlabsGsShWNnmdtkUKaBtooV9duftu1l2MjwNGblziod8WcfubCx9gShmNCi', 1, 1, 1, NULL, '2018-09-25 09:35:50', '2019-05-13 08:52:21', NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'Rheza Sindhuw', 'rheza@sehatq.com', '$2y$10$rYHpydw43BZqwxrZCbbbu.bo2X4Hzxr7zp1hdAE4hMvDrD3GAvZ7K', NULL, 'sYO9CW7CUz2NPOHtpv6HqfOwiD3AhvFsr2T21RF3W8dt4iHW00Il7tQXLPlz', 3, 1, 1, NULL, '2018-09-27 02:27:14', '2019-05-20 11:13:21', NULL, NULL, NULL);
INSERT INTO `users` VALUES (3, 'Rheza Dokter', 'rheza.s@gmail.com', '$2y$10$/sIppKo7amIWFuMHoxUqHu.tQDhHrtkHnBdeVofrhx.lsq1RBOQvK', NULL, 'iPmXPVp3x5SHe2Ycz6gnzIEP3N7p4FtAnRBWKcUU32w0eS9qesXiaT7E8vI6', 3, 1, 1, '2019-05-21 09:17:08', '2018-09-27 03:08:48', '2019-05-21 09:17:08', NULL, NULL, NULL);
INSERT INTO `users` VALUES (4, 'Prof. dr. A. A. Afif Siregar, Sp.A(K), Sp.JP, FIHA', 'afif@test.com', '$2y$10$LQNPW/mcLNW1dHlypptU7.dYnS0eZAvlsaQFQKeCbaZuZVlDLREnW', NULL, NULL, 3, 1, 1, NULL, '2019-05-20 13:00:19', '2019-05-20 13:00:19', NULL, NULL, NULL);
INSERT INTO `users` VALUES (5, 'dr. Adlin Adnan SpTHT-KL(K)', 'adlin@test.com', '$2y$10$jeQfdh0d1jKLtXBv4qdALeYvAFCnAZvkxeGQo8IzTokvu5NAppsMm', NULL, NULL, 3, 1, 1, NULL, '2019-05-20 13:57:57', '2019-05-20 13:57:57', NULL, NULL, NULL);
INSERT INTO `users` VALUES (6, 'dr. Bambang Suprayogi, Sp.THT', 'suprayogi@gmail.com', '$2y$10$NGsTq7im4awrlzz3NXbd5e/AXyzzbnfVHC.7xyuceWL2x6NJWBS/K', NULL, NULL, 3, 1, 1, NULL, '2019-05-20 14:07:14', '2019-05-20 14:09:06', NULL, NULL, '2019-05-20 14:09:06');
COMMIT;

-- ----------------------------
-- Table structure for usertypes
-- ----------------------------
DROP TABLE IF EXISTS `usertypes`;
CREATE TABLE `usertypes` (
  `usertypes_id` int(10) NOT NULL AUTO_INCREMENT,
  `usertypes_role` varchar(50) NOT NULL,
  `usertypes_description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usertypes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usertypes
-- ----------------------------
BEGIN;
INSERT INTO `usertypes` VALUES (1, 'superadmin', 'Memiliki kontrol penuh terhadap sistem', '2018-10-03 00:00:00', '2018-10-03 00:00:00', NULL);
INSERT INTO `usertypes` VALUES (2, 'admin', 'Dapat menambah atau menghapus akun dokter', '2018-10-03 00:00:00', '2018-10-03 09:24:54', NULL);
INSERT INTO `usertypes` VALUES (3, 'dokter', 'Akun dokter untuk menangani konsultasi', '2018-10-03 00:00:00', '2018-10-03 00:00:00', NULL);
INSERT INTO `usertypes` VALUES (4, 'finance', 'Departemen bagian keuangan dan shift dokter', '2019-05-16 16:00:37', '2019-05-17 09:33:48', NULL);
INSERT INTO `usertypes` VALUES (5, 'Test', 'test', '2019-05-16 16:02:54', '2019-05-16 16:03:26', '2019-05-16 16:03:26');
INSERT INTO `usertypes` VALUES (6, 'testq', 'testq', '2019-05-17 11:43:13', '2019-05-17 11:43:23', '2019-05-17 11:43:23');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
