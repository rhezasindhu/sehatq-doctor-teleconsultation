<?php

$theme = [
    'default'      => '#54ABAD',
    'black'        => '#000000',
    'white'        => '#ffffff',
    'red'          => '#ff0000',
    'red_light'    => '#ffb8c3',
    'red_dark'     => '#890000',
    'grey'         => '#aaaaaa',
    'grey_light'   => '#eeeeee',
    'grey_dark'    => '#888888',
    'green'        => '#52ae4c',
    'green_light'  => '#00d400',
    'green_dark'   => '#007300',
    'blue'         => '#0000FF',
    'blue_light'   => '#8bbbff',
    'blue_dark'    => '#0000af',
    'yellow'       => '#FFFF00',
    'orange'       => '#f4792c',
    'lime'         => '#00FF00',
    'cyan'         => '#00FFFF',
    'magenta'      => '#FF00FF',
    'silver'       => '#C0C0C0',
    'maroon'       => '#800000',
    'olive'        => '#808000',
    'purple'       => '#800080',
    'teal'         => '#008080',
    'navy'         => '#000080',
    'turquoise'    => '#12b9d2',
    'gold'         => '#cbbc29',
    'brown'        => '#523809',
];

$theme_default = 'default';

return [
    'js_min' => (env('JS_MINIFY', TRUE) ? '.min' : ''),
    'assets' => [
        'path_img' => 'public/assets/img',
        'path_css' => 'public/assets/css',
        'path_js' => 'public/assets/js',
    ],
    'meta' => [
        'type' => 'website',
        'title' => env('APP_TITLE', 'SehatQ'),
        'url' => '',//url()->full(),
        'description' => env('APP_DESC', 'SehatQ'),
        'site_name' => env('APP_NAME', 'SehatQ'),
        'author' => env('APP_AUTHOR', 'SehatQ'),
        'keyword' => env('APP_KEYWORD', 'SehatQ'),
        'theme_color' => [
            'color_name' => !empty($theme_default) ? $theme_default : env('APP_THEME_COLOR_NAME', 'green'),
            'color_hex_code' => !empty($theme[$theme_default]) ? $theme[$theme_default] : env('APP_THEME_COLOR_HEXCODE', '#54ABAD'),
        ],
    ],
    'loading_icon' => 'public/assets/img/loading.svg'
];