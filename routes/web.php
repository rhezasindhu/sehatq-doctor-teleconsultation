<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/doctor/logout', 'AdminController@logout')->name('doctor.logout');
Route::any('/doctor/login', 'Auth\AdminLoginController@login')->name('doctor.login');
Route::get('/home', 'AdminController@index')->name('doctor.dashboard');
Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('home');
Auth::routes();

/*Route::prefix('admin')->group(function() {
    Route::get('/settings/clearcache/{id}', 'SettingController@clearcache')->name('backend.setting.clearcache');
    Route::any('/settings/userprofile/{id}', 'SettingController@userprofile')->name('backend.setting.userprofile');

    Route::get('/systemsettings/accounttype/delete/{id}', 'SettingController@deleteAccountType')->name('backend.setting.accounttype.delete');
    Route::any('/systemsettings/accounttype/add', 'SettingController@addAccountType')->name('backend.setting.accounttype.add');
    Route::get('/systemsettings/accounttype', 'SettingController@accountType')->name('backend.setting.accounttype');
    Route::any('/systemsettings/setpermission/{id}', 'SettingController@formPermission')->name('backend.setting.permission.set');
    Route::get('/settings/index', 'SettingController@index')->name('backend.setting.index');
    Route::get('/settings', 'SettingController@index')->name('backend.setting');
});
Route::get('admin', function () { return redirect()->route('backend.login'); });*/



/*Route::any('/tentang-kami', 'AboutController@index')->name('about');
Route::any('/kebijakan-editorial', 'AboutController@editorial')->name('editorial');
//Route::any('/promo', 'PromoController@index')->name('promo');
//Route::any('/promo/detil', 'PromoController@details')->name('promo.details');
//Route::any('/acara', 'EventController@index')->name('event');
//Route::any('/acara/detil', 'EventController@details')->name('event.details');
Route::any('/syarat', 'TermsController@index')->name('terms');
Route::any('/kebijakan-privasi', 'PrivacyController@index')->name('privacy');
Route::any('/kontak', 'ContactController@index')->name('contact');
Route::any('/mobile/daftar', 'MobileController@register')->name('mobile.register');
Route::get('/akun-saya', 'DashboardController@index')->name('dashboard');
Route::post('/akun-saya/perbaharui', 'DashboardController@update')->name('myaccount.update');*/
#Route::get('/keluar', 'UserController@logout')->name('logout');

//Route::any('/login_api', 'Auth\LoginController@loginAPI')->name('login.api');
/*Route::post('/ajaxsocmedlogin', 'Auth\LoginController@ajaxSocmedLogin')->name('ajaxsocmedlogin');
Route::post('/ajaxregistercheck', 'Auth\RegisterController@ajaxRegisterCheck')->name('ajaxregistercheck');
Route::post('/ajaxsocmedregister', 'Auth\RegisterController@ajaxSocmedRegister')->name('ajaxsocmedregister');
Route::post('/ajaxregister', 'Auth\RegisterController@ajaxRegister')->name('ajaxregister');
Route::any('/google/distance', 'GoogleController@getDistance')->name('google.distance');
Route::any('/google/getdata', 'GoogleController@getDataFromPlaceID')->name('google.getdata');*/


// ajax for kota/kabupaten & kecamatan
//Route::get('ajax/{region}/{code}', 'AjaxController@getRegionData')->name('ajax.getregion'); // region = 'kecamatan' or 'regency' | code = code of province or regency
