<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Chat API */
Route::prefix('v1')->group(function() {
    Route::any('chat/feedback', 'API\v1\ChatController@feedback');
    Route::any('chat/medical/save', 'API\v1\ChatController@saveMedicalReport');
    Route::any('chat/end', 'API\v1\ChatController@end');
    Route::get('chat/prescreening/{id}', 'API\v1\ChatController@prescreeningDetail');
    Route::any('chat/change_status', 'API\v1\ChatController@changeStatus'); //-- Step 3
    Route::any('chat/join', 'API\v1\ChatController@join');                  //-- Step 2
    //Route::any('chat/accept', 'API\v1\ChatController@accept');
    //Route::any('chat/queue/cancel', 'API\v1\ChatController@queueCancel');
    Route::post('chat/queue/clear/{id}', 'API\v1\ChatController@queueClearApp');
    Route::any('chat/queue/clear', 'API\v1\ChatController@queueClear');
    Route::get('chat/queue/count', 'API\v1\ChatController@queueCount');
    Route::get('chat/queue', 'API\v1\ChatController@queueList');
    Route::post('chat/queue', 'API\v1\ChatController@queueSave');           //-- Step 1
});
