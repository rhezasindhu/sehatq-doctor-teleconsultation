<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password minimal enam karakter dan sesuai dengan ketentuan.',
    'reset' => 'Password anda telah di reset ulang!',
    'sent' => 'Permintaan reset kata sandi anda telah kami kirim melalui email anda!',
    'token' => 'Token reset password ini tidak valid.',
    'user' => 'Kami tidak dapat menemukan pengguna dengan alamat email tersebut.',

];
