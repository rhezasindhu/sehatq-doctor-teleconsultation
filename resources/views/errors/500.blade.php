<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129301594-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-129301594-1');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/favicon.ico') }}">
    <title>Page not found - 500</title>
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
    <?php if(isset($meta['ogimg'])): ?><meta property="og:image" content="<?php echo $meta['ogimg']; ?>" /><?php endif; ?>
    <?php if(isset($meta['ogtitle'])): ?><meta property="og:title" content="<?php echo $meta['ogtitle']; ?>" /><?php endif; ?>
    <?php if(isset($meta['ogdesc'])): ?><meta property="og:description" content="<?php echo $meta['ogdesc']; ?>" /><?php endif; ?>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}"><!-- v4.1.3 -->
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('public/vendor/fontawesome/css/all.min.css') }}"><!-- v5.3 -->
    <!-- Custom Style CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}?ts=<?=time()?>"><!-- Global Custom CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}"><!-- Mobile Reponsive CSS -->
</head>
<body class="page-404">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 col-sm-4 col-md-4 col-lg-2 pt-3">
                <img src="{{ asset('public/img/logo.png') }}" alt="" class="img-fluid">
            </div>
        </div>
        <div class="row justify-content-end">
            <div class="col col-sm-8 col-md-5">
                <h1>ERROR 500</h1>
                <p>You are almost there! <br>
                Harap bersabar atau kembali ke <a href="{{ url('/') }}">beranda</a>.</p>
            </div>
        </div>
    </div>
</body>
</html>
