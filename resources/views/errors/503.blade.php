<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129301594-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-129301594-1');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex,nofollow" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/favicon.ico') }}">
    <title>Under Construction - 503</title>
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
    <?php if(isset($meta['ogimg'])): ?><meta property="og:image" content="<?php echo $meta['ogimg']; ?>" /><?php endif; ?>
    <?php if(isset($meta['ogtitle'])): ?><meta property="og:title" content="<?php echo $meta['ogtitle']; ?>" /><?php endif; ?>
    <?php if(isset($meta['ogdesc'])): ?><meta property="og:description" content="<?php echo $meta['ogdesc']; ?>" /><?php endif; ?>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}"><!-- v4.1.3 -->
    <!-- Custom Style CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}?ts=<?=time()?>"><!-- Global Custom CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}"><!-- Mobile Reponsive CSS -->
</head>
<body class="page-503">
    <div class="container-fluid">
        <div class="p-3 p-md-5">
            <h1>UNDER CONSTRUCTION</h1>
            <p>Good stuff is coming. We’re still working on this page to give you a better experience.</p>
        </div>
    </div>
</body>
</html>
