@extends('web.layouts.app')

@section('content')
    <div class="card card-outline-info">
        <div class="card-header"><h4 class="mb-0 text-white">Settings: Add Account Type</h4></div>
        <div class="card-body">
            <!-- Form -->
            <form method="post" id="frmRole" class="validateForm" action="{{ Request::fullUrl() }}">
                @csrf
                <div class="row">
                    <div class="col-md-3"><label for="role" class="required">Role Name</label></div>
                    <div class="col-md-9"><input type="text" id="role" name="role" class="form-control" value="{{ old('role') }}" minlength="3" maxlength="50" required></div>
                </div><br>

                <!-- Action Button Part -->
                <div class="wrapper-action text-right">
                    <button type="button" class="btn btn-success" onclick="validateForm()">Save</button>
                    <button type="button" class="btn btn-secondary" onclick="location.href='{{ route('backend.setting.accounttype') }}'">Cancel</button>
                </div>
            </form><!-- End of Form -->
        </div>
    </div>
@endsection

@section('local_script')
    <script>
        function validateForm() {
            if ($(".validateForm").valid()) {
                $(".validateForm").submit();
            }
        }

        $(function() {
            $('.datatable').DataTable({
                "aaSorting": [],
                "aoColumns": [
                    null,
                    null,
                    { "bSortable": false },
                ],
                "stateSave": true,
                "stateDuration": 60 * 60 * 24
            });
        });
    </script>
@endsection
