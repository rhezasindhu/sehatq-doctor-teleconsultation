@extends('web.layouts.app')

@section('content')
    <div class="card card-outline-info">
        <div class="card-header"><h4 class="mb-0 text-white">Settings: Manage Roles</h4></div>
        <div class="card-body">
            <!-- Table -->
            <table class="table datatable table-striped hover" id="tblAccountType">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Role</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usertypes as $usertype)
                        <tr>
                            <td>{{ $usertype->usertypes_id }}</td>
                            <td>{{ ucwords($usertype->usertypes_role) }}</td>
                            <td class="text-center action-table"><!-- Actions Button -->
                                <div class="btn-group" role="group">
                                    <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.permission.set', $usertype->usertypes_id) }}'">
                                        <i class="fas fa-key fa-sm fa-fw"></i>
                                    </button>
                                    <button class="btn btn-danger btnDelete" title="Delete Role" onclick="confirmDelete('{{ route('backend.setting.accounttype.delete', $usertype->usertypes_id) }}')">
                                        <i class="far fa-trash-alt fa-sm fa-fw"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table><!-- End of Table -->
            <br>

            <!-- Action Button Part -->
            <div class="wrapper-action text-right">
                <button type="button" class="btn btn-success" onclick="location.href='{{ route('backend.setting.accounttype.add') }}'">Add More Role</button>
            </div>
        </div>
    </div>
@endsection

@section('local_script')
    <script>
        jQuery(document).ready(function($) {
            $('.datatable').DataTable({
                "dom"        : 'lBfrtip',
                "pageLength" : 10,
                "responsive" : {
                    details: {
                        display : $.fn.dataTable.Responsive.display.childRowImmediate,
                        type    : 'inline'
                    }
                },
                "aaSorting": [],
                "aoColumns": [
                    { "width": 30 },
                    null,
                    { "bSortable": false, "width": 100 },
                ],
                "pagingType"    :'full_numbers',
                "language": {
                    "paginate": {
                        "first"    : '<i class="ti-angle-double-left"></i>',
                        "last"     : '<i class="ti-angle-double-right"></i>',
                        "previous" : '<i class="ti-angle-left"></i>',
                        "next"     : '<i class="ti-angle-right"></i>',
                    },
                },
                "stateSave": true,
                "stateDuration": 60 * 60 * 24
            });
        });
    </script>
@endsection
