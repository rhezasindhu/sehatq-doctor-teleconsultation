@extends('web.layouts.app')

@section('content')
    <div class="card card-outline-info">
        <div class="card-header"><h4 class="mb-0 text-white">Settings: General</h4></div>
        <div class="card-body">
            <h3 class="card-title">Cache Management</h3>
            <hr>
            <!-- Table -->
            <table class="table datatable table-bordered table-striped" id="tblSettingGeneral">
                <tbody>
                    <tr>
                        <td><label class="col-form-label">All Cache</label></td>
                        <td>
                            <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.clearcache', 'all') }}'">
                                <i class="fas fa-fw fa-eraser"></i> Clear
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="col-form-label">Cache: Doctor</label></td>
                        <td>
                            <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.clearcache', 'doctor') }}'">
                                <i class="fas fa-fw fa-eraser"></i> Clear
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="col-form-label">Cache: Hospital</label></td>
                        <td>
                            <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.clearcache', 'hospital') }}'">
                                <i class="fas fa-fw fa-eraser"></i> Clear
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="col-form-label">Cache: Article</label></td>
                        <td>
                            <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.clearcache', 'article') }}'">
                                <i class="fas fa-fw fa-eraser"></i> Clear
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="col-form-label">Cache: Disease</label></td>
                        <td>
                            <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.clearcache', 'disease') }}'">
                                <i class="fas fa-fw fa-eraser"></i> Clear
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>C<label class="col-form-label">ache: Drug</label></td>
                        <td>
                            <button class="btn btn-warning btnView" title="Set/View Permissions" onclick="location.href='{{ route('backend.setting.clearcache', 'drug') }}'">
                                <i class="fas fa-fw fa-eraser"></i> Clear
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table><!-- End of Table -->
        </div>
    </div>
@endsection

@section('local_script')
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
@endsection
