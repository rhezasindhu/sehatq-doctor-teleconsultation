@extends('web.layouts.app')

@section('content')
    <div class="card card-outline-info">
        <div class="card-header">
            <h4 class="mb-0 text-white">User Profiles</h4>
        </div>
        <div class="card-body">
            <!-- Tabs -->
            <!--ul class="nav nav-tabs" id="nav-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="personalTab" href="#personal" data-toggle="tab">Personal Information</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="medicalTab" href="#medical" data-toggle="tab">Medical Information</a>
                </li>
            </ul-->

            <!--div class="tab-content clearfix"-->
                <!-- Personal Information Tab -->
                <!--div class="tab-pane show active" id="personal"-->
                    <?php //-- Request::fullUrl() --> will send the action to the current url ?>
                    <form method="post" id="frmPatient" class="validateForm" action="{{ Request::fullUrl() }}" enctype="multipart/form-data">
                        @csrf
                        <h5 class="card-title">Personal Particulars</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="control-label label-content required">Name</label>
                                    <input type="text" id="name" name="name" class="form-control" value="{{ isset($user->name) ? $user->name : old('name') }}" minlength="3" maxlength="190" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="alias" class="control-label label-content">Alias</label>
                                    <input type="text" id="alias" name="alias" class="form-control" value="{{ isset($user->userdetails_alias) ? $user->userdetails_alias : old('alias') }}" minlength="3" maxlength="255">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="sex" class="control-label label-content required">Gender</label>
                                    <?php $dbSex = isset($user->userdetails_sex)? $user->userdetails_sex : old('sex'); ?>
                                    <select id="sex" name="sex" class="form-control" required>
                                        <option value="">-- Please select --</option>
                                        <option value="M" <?php if($dbSex == 'M') { echo 'selected'; }  ?> >Male</option>
                                        <option value="F" <?php if($dbSex == 'F') { echo 'selected'; }  ?> >Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobileno" class="control-label label-content required">Mobile</label>
                                    <div class="form-row">
                                        <?php
                                            $mobileno = array('','');
                                            if (isset($user->userdetails_mobile) && $user->userdetails_mobile != ''):
                                                $mobileno = explode('-', $user->userdetails_mobile);
                                        ?>
                                            <div class="col-auto">
                                                <select id="mobilepre" name="mobilepre" class="form-control narrow">
                                                    <option value="+62" <?php if($mobileno[0]=="+62") { echo "selected"; } ?> >+62</option>
                                                    <option value="+65" <?php if($mobileno[0]=="+65") { echo "selected"; } ?> >+65</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <input type="text" id="mobileno" name="mobileno" class="form-control wide" value="<?php echo $mobileno[1]; ?>" minlength="6" maxlength="12" required>
                                            </div>
                                        <?php else: ?>
                                            <div class="col-auto">
                                                <select id="mobilepre" name="mobilepre" class="form-control narrow">
                                                    <option value="+62" <?php if(old('mobilepre')=="+62") { echo "selected"; } ?> >+62</option>
                                                    <option value="+65" <?php if(old('mobilepre')=="+65") { echo "selected"; } ?> >+65</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <input type="text" id="mobileno" name="mobileno" class="form-control wide" value="{{ old('mobileno') }}" minlength="6" maxlength="12" required>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="marital" class="control-label label-content">Marital Status</label>
                                    <?php $dbMarital = isset($user->userdetails_marital) ? $user->userdetails_marital : old('marital'); ?>
                                    <select id="marital" name="marital" class="form-control">
                                        <option value="">-- Please select --</option>
                                        <option value="0" <?php if($dbMarital == '0') { echo 'selected'; }  ?> >Not Married</option>
                                        <option value="1" <?php if($dbMarital == '1') { echo 'selected'; }  ?> >Married</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php
                                    $dbDOB = "";
                                    if (isset($user->userdetails_dob)){
                                        $arrDOB  = explode('-', $user->userdetails_dob);
                                        $dbYear  = $arrDOB[0];
                                        $dbMonth = $arrDOB[1];
                                        $dbDate  = $arrDOB[2];
                                        $numDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $dbMonth, $dbYear);
                                    } else {
                                        $now   = new DateTime('now');
                                        $dbMonth = $now->format('m');
                                        $dbYear  = $now->format('Y');
                                        $dbYear  = '2018';
                                        $dbMonth = '11';
                                        $dbDate  = old('date');
                                        $numDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $dbMonth, $dbYear);
                                    }
                                ?>
                                <div class="form-group">
                                    <label for="dob" class="control-label label-content required">Date of Birth</label>
                                    <div class="form-row wrapper-dob">
                                        <div class="col">
                                            <input type="number" id="year" name="year" class="form-control year" placeholder="YYYY" value="<?= $dbYear; ?>" maxlength="4" min="1800" max="<?php echo date("Y"); ?>" required>
                                        </div>
                                        <div class="col">
                                            <select id="month" name="month" class="form-control month" required>
                                                <option value="">-- Month --</option>
                                                <option value="01" <?= ($dbMonth == '01')? 'selected':''; ?>>January</option>
                                                <option value="02" <?= ($dbMonth == '02')? 'selected':''; ?>>February</option>
                                                <option value="03" <?= ($dbMonth == '03')? 'selected':''; ?>>March</option>
                                                <option value="04" <?= ($dbMonth == '04')? 'selected':''; ?>>April</option>
                                                <option value="05" <?= ($dbMonth == '05')? 'selected':''; ?>>May</option>
                                                <option value="06" <?= ($dbMonth == '06')? 'selected':''; ?>>June</option>
                                                <option value="07" <?= ($dbMonth == '07')? 'selected':''; ?>>July</option>
                                                <option value="08" <?= ($dbMonth == '08')? 'selected':''; ?>>August</option>
                                                <option value="09" <?= ($dbMonth == '09')? 'selected':''; ?>>September</option>
                                                <option value="10" <?= ($dbMonth == '10')? 'selected':''; ?>>October</option>
                                                <option value="11" <?= ($dbMonth == '11')? 'selected':''; ?>>November</option>
                                                <option value="12" <?= ($dbMonth == '12')? 'selected':''; ?>>December</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            @if(Route::current()->getName() == 'backend.setting.userprofile')
                                                <select id="date" name="date" class="form-control dates" required>
                                                    <option value="">-- Date --</option>
                                                    <?php if($numDaysInMonth > 0): ?>
                                                        <?php for($i=1; $i<=$numDaysInMonth; $i++): ?>
                                                            <?php $numPad = str_pad($i,2,"0",STR_PAD_LEFT); ?>
                                                            <option value="<?php echo $numPad; ?>" <?= ($dbDate == $numPad)? 'selected':''; ?>>
                                                                <?php echo $numPad; ?>
                                                            </option>
                                                        <?php endfor; ?>
                                                    <?php endif; ?>
                                            @else
                                                <select id="date" name="date" class="form-control date" disabled required>
                                                    <option value="">-- Date --</option>
                                            @endif
                                                </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country" class="control-label label-content required">Country of Origin</label>
                                    <select id="country" name="country" class="form-control">
                                        <option value="Indonesia" <?php if(old('country') == 'Indonesia') { echo 'selected'; }  ?> >Indonesia</option>
                                        <option value="Singapore" <?php if(old('country') == 'Singapore') { echo 'selected'; }  ?> >Singapore</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="city" class="control-label label-content required">City of Residence</label>
                                    <input type="text" id="city" name="city" class="form-control" value="{{ isset($user->userdetails_city) ? ucwords($user->userdetails_city) : old('city') }}" maxlength="255" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email" class="control-label label-content required">Email</label>
                                    <?php //-- If the current page = 'Edit Page' then show the checkbox otherwise hide it ?>
                                    @if(Route::current()->getName() == 'backend.setting.userprofile')
                                        <input type="text" readonly class="form-control-plaintext" id="email" value="{{ $user->email }}">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <h5 class="card-title mt-5">Account Details</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php //-- If the current page = 'Edit Page' then show the checkbox otherwise hide it ?>
                                    @if(Route::current()->getName() == 'backend.setting.userprofile')
                                        <input type="checkbox" id="chkChangePass" class="filled-in chk-col-light-blue" name="chkChangePass"/>
                                        <label for="chkChangePass" class="label-content">&nbsp; Change Password</label>
                                    @endif
                                    <div id="wrapper-changepass" class="wrapper-toggle">
                                        <label for="password" class="control-label label-content required">Password</label>
                                        <div class="input-group">
                                            <input type="password" id="password" name="password" class="form-control" autocomplete="off" minlength="8">
                                            <div class="input-group-append">
                                                <button class="btn btn-info toggle-pass" type="button"><i class="toggle-pass far fa-eye"></i></button>
                                            </div>
                                        </div>
                                        <div class="form-control-feedback">
                                        <label id="password-error" class="error" for="password">* Min. 8 chars, 1 uppercase, 1 special char</label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                        <!-- Related Accounts Part -->

                        <!-- Action Button Part -->
                        <div class="wrapper-action text-right">
                            <button type="button" class="btn btn-success" onclick="validateForm()">Save</button>
                            <button type="button" class="btn btn-secondary" onclick="location.href='{{ route('backend.dashboard') }}'">Cancel</button>
                        </div>
                    </form>
                <!--/div-->

                <!-- Medical Information Tab -->
                <!--div class="tab-pane" id="medical">
                    <p>Masih belum fix!</p>
                </div-->
            <!--/div--><!-- End of Tabs -->

        </div>
    </div>
@endsection

@section('local_script')
    <script>
        function validateForm() {
            if ($(".validateForm").valid()) {
                $(".validateForm").submit();
            }
        }

        $(function() {
            //-- Determine to show the change password panel, in 'Add page' should always show interval
            //-- while in 'Edit page' we have a choice wheather to change the password/not
            var currentUrl = window.location.href;
            if (currentUrl.indexOf('add')>0) {
                $('#wrapper-changepass').show();
                $('#wrapper-changepass #password').attr('required','required');
            } else {
                $('#wrapper-changepass').hide();
            }

            //-- Toggle panel (show/hide) change password
            $('#chkChangePass').click(function(){
                $('#wrapper-changepass').toggle(function(){
                    if ($(this).is(":visible")) {
                        $('#wrapper-changepass #password').attr('required','required');
                    } else {
                        $('#wrapper-changepass #password').removeAttr('required');
                        $('#wrapper-changepass #password').removeClass('error');
                    }
                });
            });

            //-- Function confirmDelete init at app.js (Global - can be reused)
            //-- Function to generate date options for DOB selector at app.js (Global - can be reused)
            //-- Form client side validator at app.js (Global - can be reused)
        });
    </script>
@endsection
