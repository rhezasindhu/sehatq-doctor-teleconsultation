@extends('web.layouts.app')

@section('local_css')
    <link rel="stylesheet" href="{{ asset('public/materialPro/assets/plugins/switchery/dist/switchery.min.css') }}">
@endsection

@section('content')
    <div class="card card-outline-info">
        <div class="card-header"><h4 class="mb-0 text-white">Settings: Set Account Roles</h4></div>
        <div class="card-body">
            <form method="post" action="{{ Request::fullUrl() }}" enctype="multipart/form-data" class="form-material m-t-40">
                @csrf
                <div class="row">
                    <div class="col-md-3"><label class="control-label label-content">Role Name</label></div>
                    <div class="col-md-9">
                        <input type="text" class="form-control form-control-line" value="{{ ucwords($usertypes->usertypes_role) }}" readonly>
                    </div>
                </div>

                <!--div class="col-md-9"><label class="control-label text-center" style="font-weight:bold;">{{ ucwords($usertypes->usertypes_role) }}</label></div-->
                <input type="hidden" name="usertypes_id" id="usertypes_id" value="{{ $usertypes->usertypes_id }}">
                <br>

                <?php
                    //-- Populate permissions array, so it can show assigned permissions through the on/off switch
                    $permission = array();
                    foreach ($userpermissions as $userpermission) {
                        switch ($userpermission->userpermissions_name) {
                            case 'article' :
                                $permission['article'] = 1;
                                break;
                            case 'disease' :
                                $permission['disease'] = 1;
                                break;
                            case 'drug' :
                                $permission['drug'] = 1;
                                break;
                            case 'askdoctor' :
                                $permission['askdoctor'] = 1;
                                break;
                            case 'booking' :
                                $permission['booking'] = 1;
                                break;
                            case 'event' :
                                $permission['event'] = 1;
                                break;
                            case 'promotion' :
                                $permission['promotion'] = 1;
                                break;
                            case 'doctor' :
                                $permission['doctor'] = 1;
                                break;
                            case 'hospital' :
                                $permission['hospital'] = 1;
                                break;
                            /*case 'forum' :
                                $permission['forum'] = 1;
                                break;
                            case 'healthlab' :
                                $permission['healthlab'] = 1;
                                break;
                            case 'healthtool' :
                                $permission['healthtool'] = 1;
                                break;*/
                            case 'patient' :
                                $permission['patient'] = 1;
                                break;
                            case 'insurance' :
                                $permission['insurance'] = 1;
                                break;
                            case 'cmscareers' :
                                $permission['cmscareers'] = 1;
                                break;
                            case 'setting' :
                                $permission['setting'] = 1;
                                break;
                            case 'member' :
                                $permission['member'] = 1;
                                break;
                            case 'systemsetting' :
                                $permission['systemsetting'] = 1;
                                break;
                        }
                    }
                ?>

                <!-- Table -->
                <table class="table datatable table-striped" id="tblPermission">
                    <thead>
                        <tr>
                            <th>Module Name</th>
                            <th class="text-center">Allow Access</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><label for="chkPermissionArticles" class='control-label label-content'>Articles</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="article,admin/article" name="chkPermission[]" id="chkPermissionArticles" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['article'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionDisease" class='control-label label-content'>Disease</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="disease,admin/disease" name="chkPermission[]" id="chkPermissionDisease" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['disease'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionDrugs" class='control-label label-content'>Drugs</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="drug,admin/drug" name="chkPermission[]" id="chkPermissionDrugs" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['drug'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionAskdoctor" class='control-label label-content'>Forum</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="askdoctor,admin/askdoctor" name="chkPermission[]" id="chkPermissionAskdoctor" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['askdoctor'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionPatient" class='control-label label-content'>Booking</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="booking,admin/booking" name="chkPermission[]" id="chkPermissionBooking" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['booking'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionSystemSettings" class='control-label label-content'>Event</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="event,admin/event" name="chkPermission[]" id="chkPermissionSystemSettings" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['event'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionSystemSettings" class='control-label label-content'>Promotion</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="promotion,admin/promotion" name="chkPermission[]" id="chkPermissionSystemSettings" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['promotion'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionDoctors" class='control-label label-content'>Doctors</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="doctor,admin/doctor" name="chkPermission[]" id="chkPermissionDoctors" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['doctor'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionHospital" class='control-label label-content'>Hospital</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="hospital,admin/hospital" name="chkPermission[]" id="chkPermissionHospital" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['hospital'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <!--tr>
                            <td><label for="chkPermissionForum" class='control-label label-content'>Forum</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="forum,admin/forum" name="chkPermission[]" id="chkPermissionForum" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['forum'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionHealthlab" class='control-label label-content'>Health Lab</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="healthlab,admin/healthlab" name="chkPermission[]" id="chkPermissionHealthlab" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['healthlab'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionHealthtools" class='control-label label-content'>Health Tools</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="healthtool,admin/healthtool" name="chkPermission[]" id="chkPermissionHealthtools" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['healthtool'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr-->
                        <tr>
                            <td><label for="chkPermissionPatient" class='control-label label-content'>Patients</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="patient,admin/patient" name="chkPermission[]" id="chkPermissionPatient" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['patient'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionPatient" class='control-label label-content'>Insurance</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="insurance,admin/insurance" name="chkPermission[]" id="chkPermissionInsurance" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['insurance'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionPatient" class='control-label label-content'>CMS Careers</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="cmscareers,admin/career" name="chkPermission[]" id="chkPermissionCareer" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['cmscareers'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionSettings" class='control-label label-content'>Settings</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="setting,admin/setting" name="chkPermission[]" id="chkPermissionSettings" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['setting'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionMember" class='control-label label-content'>User Backend</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="member,admin/member" name="chkPermission[]" id="chkPermissionMember" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['member'])) ? 'checked':''; ?> >
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="chkPermissionSystemSettings" class='control-label label-content'>System Settings</label></td>
                            <td>
                                <label class="switch text-center">
                                    <input type="checkbox" value="systemsetting,admin/systemsetting" name="chkPermission[]" id="chkPermissionSystemSettings" class="js-switch" data-color="#009efb" data-size="small" <?= (isset($permission['systemsetting'])) ? 'checked':''; ?>>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table><!-- End of Table -->

                <!-- Action Button Part -->
                <div class="wrapper-action text-right">
                    <button type="button" class="btn btn-success" name="btnSave" onclick="form.submit()">Save</button>
                    <button type="button" class="btn btn-secondary" onclick="location.href='{{ route('backend.setting.accounttype') }}'">Cancel</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('local_script')
    <script src="{{ asset('public/materialPro/assets/plugins/switchery/dist/switchery.min.js') }}" defer></script>
    <script>
        $(function() {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

            $('.datatable').DataTable({
                "paging"      : false,
                "ordering"    : false,
                "searching"   : false,
                "lengthChange": false,
                "info"        : false,
                "aaSorting": [],
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                ],
            });
        });
    </script>
@endsection
