<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>
        @hasSection('title')
            {{ strtoupper(env('APP_ENV_PREFIX')) }}@yield('title'){{ ' | ' . env('APP_TITLE_MAIN') }}
        @else
            {{ config('_customs.meta.title') }}
        @endif
    </title>
    <meta charset="utf-8"/>
    <meta name="language" content="{{ app()->getLocale() }}"/>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1">
    <meta name="description" content="{{ config('_customs.meta.description') }}"/>
    <meta name="keywords" content="{{ config('_customs.meta.keyword') }}"/>
    <meta name="author" content="{{ config('_customs.meta.author') }}"/>
    <meta name="publisher" content="{{ config('_customs.meta.author') }}"/>
    <meta name="revisit-after" content="5"/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta name="theme-color" content="{{ config('_customs.meta.theme_color.color_hex_code') }}">
    @if(isset($type))
        <meta property="og:type" content="{{ $type }}"/>
    @endif
    @hasSection('og')
        @yield('og')
    @else
        <meta property="og:title" content="{{ config('_customs.meta.title') }}"/>
        <meta property="og:description" content="{{ config('_customs.meta.description') }}"/>
        <meta property="og:url" content="{{ config('_customs.meta.url') }}"/>
        <meta property="og:image" content="{{ asset(config('_customs.assets.path_img') . '/og_image.png') }}"/>
    @endif
    <meta property="og:site_name" content="{{ config('_customs.meta.title') }}"/>
    <meta property="og:type" content="{{ config('_customs.meta.type') }}"/>
    <link rel="shortcut icon" href="{{ asset(config('_customs.assets.path_img') . '/favicon.ico') }}" type="image/x-icon"/>
    <link rel="canonical" href="{{ config('_customs.meta.url') }}">
    {{-- CORE CSS --}}
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/core.css?v=' . time()) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/main.css?v=' . time()) }}">
    {{-- CUSTOM CSS --}}
    @yield('local-styles')
</head>
<body>
@include('web.layouts._header')
<div class="content">
    @yield('content')
</div>
@yield('popup')
<script type="text/javascript">
    var base_url = '{{ Request::fullUrl() }}',
        img_path = '{{ asset(config('_customs.assets.path_img')) }}',
        img_loading = '{{ asset(config('_customs.loading_icon')) }}',
        is_login = '{{ Auth::check() }}',
        lang = '{{ app()->getLocale() }}',
        is_mobile = (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/webOS/i));
</script>
{{-- CORE JS --}}
<script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/_libs' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
<script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/_helper' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
{{-- CUSTOM JS --}}
@yield('local-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        /* GLOBAL NOTIFICATION */
        @foreach (['danger', 'warning', 'success', 'info'] as $type_message)
            @if(Session::has('alert-' . $type_message))
                @if (is_array(Session::get('alert-' . $type_message)))
                    @foreach (Session::get('alert-' . $type_message) as $message)
                        helper.showNotification({
                            message: '{{ $message }}',
                            type: '{{ $type_message }}',
                        });
                    @endforeach
                @else
                    helper.showNotification({
                        message: '{{ Session::get('alert-' . $type_message) }}',
                        type: '{{ $type_message }}',
                    });
                @endif
            @endif
        @endforeach

        @if ($errors->any())
            @foreach ($errors->all() as $error)
                helper.showNotification({
                    message: '{{ $error }}',
                    type: 'danger',
                });
            @endforeach
        @endif

        @if (session('status'))
            helper.showNotification({
                message: '{{ session('status') }}',
                type: 'success',
            });
        @endif
    });
</script>
<script type="text/javascript" src="{{ asset(config('_customs.assets.path_js') . '/_main' . config('_customs.js_min') . '.js?v=' . time()) }}"></script>
</body>
</html>