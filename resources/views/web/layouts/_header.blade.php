<div class="header fixed-top">
    <div class="container-fluid">
        <div class="row row-10">
            <div class="col-8 align-self-center">
                <a href="{{ route('doctor.dashboard') }}">
                    <img src="{{ asset(config('_customs.assets.path_img') . '/logo.png') }}" alt="SehatQ Logo" class="logo">
                </a>
            </div>
            <div class="col-4 text-right">
                @if(Auth::check())
                    <div class="dropdown dropdown-custom user-menu">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <div class="avatar-profile">
                                <img class="avatar" src="{{ helperGetUserPicture() }}" alt="">
                            </div>
                        </div>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="top">
                                <div class="row row-10">
                                    <div class="col-auto">
                                        <div class="avatar-profile medium">
                                            <img class="avatar" src="{{ helperGetUserPicture() }}" alt="">
                                        </div>
                                    </div>
                                    <div class="col align-self-center">
                                        <div class="time">{{ date('d / m / Y') }}</div>
                                        <div class="name">{{ Auth::user()->name }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom">
                                <a href="{{ route('doctor.logout') }}" class="upper-case">
                                    <div>{{ __('Log out') }}</div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>