<div class="sidebar-menu">
    <ul class="sidebar-lists list-unstyled">
        @if(!empty(helperGetMenu()))
            @foreach(helperGetMenu() as $val)
                <li class="sidebar-item">
                    <a href="{{ $val['linkUrl'] }}" data-menu="{{ $val['dataMenu'] }}">
                        {!! $val['icon'] !!}<div>{{ $val['name'] }}</div>
                    </a>
                </li>
            @endforeach
        @endif
        <li class="sidebar-item">
            <a href="{{ route('doctor.logout') }}" data-menu="logout">
                <i class="fa fa-sign-out-alt"></i><div>{{ __('Logout') }}</div>
            </a>
        </li>
    </ul>
</div>