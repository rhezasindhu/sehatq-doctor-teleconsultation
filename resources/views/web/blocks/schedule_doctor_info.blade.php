@if(!empty($data))
<div class="doctor-overall">
    <div class="row row-5">
        <div class="col-11">
            <div class="telemed-subtitle">Doctor’s Overalls</div>
        </div>
        <div class="col-1 text-right">
            <a href="javascript:void(0);" class="js-removeDoctorInfo">
                <i class="icon-close"></i>
            </a>
        </div>
    </div>
    <div class="overall-wrapper mt-4">
        <div class="row row-0">
            <div class="col-md-4">
                <div class="overall-wrap first text-center h-100">
                    <div class="avatar-wrap active">
                        <div class="img-frame"></div>
                        <img src="{{ (!empty($data['imageUrl'])) ? $data['imageUrl'] : asset(config('_customs.assets.path_img') . '/avatar-default.png') }}" alt="">
                    </div>
                    <div class="name">{{ $data['name'] }}</div>
                    <div class="specialization">{{ $data['specialization'] }}</div>
                    <div class="phone">{{ $data['phone'] }}</div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row row-0 h-100">
                    @if(!empty($data['rating']))
                    <div class="col-6">
                        <div class="overall-wrap text-center h-100 d-block d-md-flex align-items-center justify-content-center">
                            <div>
                                <div class="rating-wrap">
                                    <div class="score">
                                        <span class="current">{{ $data['rating']['score'] }}</span>
                                        <span class="total">/{{ $data['rating']['max'] }}</span>
                                    </div>
                                    <div class="title">Rangking Dokter</div>
                                    <div class="stars mt-2">{!! $data['rating']['stars'] !!}</div>
                                    <div class="total mt-3">({{ $data['rating']['total'] }})</div>
                                </div>
                                <div class="mt-3">
                                    <a href="{{ $data['detailUrl'] }}" class="btn btn-link underline">Lihat detail Dokter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-6 align-self-center">
                        <div class="overall-wrap border-0 text-center h-100">
                            <div class="total-patient">
                                <h5>Total Patients Handled</h5>
                                <h2>{{ $data['totalPatient'] }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif