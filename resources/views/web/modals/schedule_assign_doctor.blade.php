<div class="modal fade custom-modal modal-assign-doctor" id="modalAssignDoctor" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-link btn-close" data-dismiss="modal" aria-label="Close">
                <i class="icon-close"></i>
            </button>
            <div class="modal-header border-0">
                <div class="title">Cari Dokter</div>
            </div>
            <div class="modal-body">
                <div class="wrapper">
                    <form class="js-formValidate" id="formAssignDoctor" action="{{ route('schedule.save') }}" method="POST">
                        @csrf
                        <div class="row row-10">
                            <div class="col-md-3">
                                <div class="form-group mb-4">
                                    <label class="control-label">Nama Dokter</label>
                                    @if(!empty($data['listDoctors']))
                                        <div class="select2-wrapper block">
                                            <select class="form-control js-select2" name="doctor_id" required="required">
                                                @foreach($data['listDoctors'] as $val)
                                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-control">Tidak ada data</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mb-4">
                                    <label class="control-label">Tanggal</label>
                                    <div class="date-wrapper block">
                                        <input class="rounded js-datePickerAssignDoctor" type="text" name="date" placeholder="Tanggal" value="" autocomplete="off" required="required">
                                        <i class="icon-calendar cursor-pointer"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mb-4">
                                    <label class="control-label">Shift</label>
                                    @if(!empty($data['listShifts']))
                                        <div class="select2-wrapper block">
                                            <select class="form-control js-select2" name="shift_id" required="required">
                                                @foreach($data['listShifts'] as $val)
                                                    <option value="{{ $val['doctorshifts_id'] }}">{{ $val['doctorshifts_time'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-control">Tidak ada data</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12 col-sm-3"></div>
                                    <div class="col-md-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>
                                            <button class="btn btn-orange btn-block rounded upper-case js-btnAssignDoctor" type="button">Assign</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
