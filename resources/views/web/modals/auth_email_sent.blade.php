<div class="modal fade custom-modal" id="modalEmailSent" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="wrapper">
                    <div class="text-center">
                        <h5>Email Terkirim</h5>
                        <p>Email perubahan password telah berhasil kami kirimkan. Cek kotak spam anda apabila anda tidak menemukan di kotak masuk.</p>
                        <div class="mt-4">
                            <a href="{{ route('home') }}" class="btn btn-orange rounded upper-case">{{ __('Back to login') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>