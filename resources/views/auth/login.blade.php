@extends('web.layouts.html')

@section('content')
<div class="page-auth" id="pageLogin">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-5 col-lg-8 col-md-10">
                <div class="auth-box mt-sm-5 mt-3">
                    <div class="auth-head">
                        <h2>
                            {{ __('Welcome to') }}
                            <strong>{{ __('Telemed') }}</strong>,
                        </h2>
                        <h3>{{ __('please enter your login account.') }}</h3>
                        <h4>{{ __('Lets Work!') }}</h4>
                    </div>
                    <form class="auth-form mt-4 js-formValidate" method="POST" action="{{ route('doctor.login') }}">
                        @csrf
                        <div class="form-group line-style mb-4">
                            <label for="email" class="control-label">{{ __('Email') }}</label>
                            <input class="form-control{{ $errors->has('email') ? ' error' : '' }}" name="email" id="email" type="email" placeholder="{{ __('Enter your email') }}" value="{{ old('email') }}" required="required" autocomplete="off" autofocus>
                            @if ($errors->has('email'))
                                <label class="error">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                        <div class="form-group line-style mb-5">
                            <label for="password" class="control-label">{{ __('Password') }}</label>
                            <input class="form-control{{ $errors->has('password') ? ' error' : '' }}" name="password" id="password" type="password" placeholder="{{ __('Enter your password') }}" value="" required="required" autocomplete="off">
                            @if ($errors->has('password'))
                                <label class="error">{{ $errors->first('password') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="row row-10">
                                <div class="col-sm-6">
                                    <button class="btn btn-orange btn-lg btn-block rounded upper-case" type="submit">{{ __('Login') }}</button>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{ route('password.request') }}" class="btn btn-link color-orange btn-block opensansSemiBold">{{ __('Forgot password') }}?</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-auth.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    {{--<script>
        $.ajax({
            url: 'https://dev-telemed.sehatq.com/api/v1/chat/queue',
            type: 'GET',
            dataType: 'json',
            /*headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },*/
            beforeSend: function () {

            },
            success: function (result) {
                console.log('local telemed to dev telemed');
                console.log(result);
            },
            error: function (error) {
                console.log('error')
            }
        });
    </script>--}}
@endsection

@section('title')
    {{ __('Login') }}
@endsection

@section('og')
@endsection
