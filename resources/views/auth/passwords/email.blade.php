@extends('web.layouts.html')

@section('content')
    <div class="page-auth" id="pageForgotPassword">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-5 col-lg-8 col-md-10">
                    <div class="auth-box mt-sm-5 mt-3">
                        <div class="auth-head">
                            <h2>
                                <strong>{{ __('Forgot password') }}</strong>
                            </h2>
                        </div>
                        <form class="auth-form mt-4 js-formValidate" id="formForgotPassword" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group line-style mb-4">
                                <label for="email" class="control-label">{{ __('Email') }}</label>
                                <input class="form-control{{ $errors->has('email') ? ' error' : '' }}" name="email" id="email" type="email" placeholder="{{ __('Enter your email') }}" value="{{ old('email') }}" required="required" autocomplete="off" autofocus>
                                @if ($errors->has('email'))
                                    <label class="error">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="row row-10">
                                    <div class="col-sm-6">
                                        <button class="btn btn-orange btn-lg btn-block rounded upper-case" type="submit">{{ __('Send') }}</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{ route('home') }}" class="btn btn-link color-orange btn-block opensansSemiBold">{{ __('Back to login') }}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('popup')
    @include('web.modals.auth_email_sent')
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-auth.css?v=' . time()) }}">
@endsection

@section('local-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var parent = $('#pageForgotPassword');
            $('#formForgotPassword', parent).on('submit', function () {
                var form = $(this);
                if($(this).valid()) {
                    $.ajax({
                        type : 'POST',
                        url  : '{{ route('password.email') }}',
                        data : form.serialize(),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            helper.buttonLoading($(':submit', form), 'loading');
                        },
                        success: function (data) {
                            helper.buttonLoading($(':submit', form), 'reset');
                            $('#modalEmailSent').modal('show');
                        },
                        error: function (data) {
                            helper.buttonLoading($(':submit', form), 'reset');
                            helper.showNotification({
                                message: '{{ __('Failed to connect to server') }}',
                                type: 'danger',
                            });
                        }
                    });
                    return false;
                }
            });
        });
    </script>
@endsection

@section('title')
    {{ __('Forgot password') }}
@endsection

@section('og')
@endsection
