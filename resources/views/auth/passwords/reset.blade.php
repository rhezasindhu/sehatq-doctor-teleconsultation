@extends('web.layouts.html')

@section('content')
    <div class="page-auth" id="pageLogin">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-5 col-lg-8 col-md-10">
                    <div class="auth-box mt-sm-5 mt-3">
                        <div class="auth-head">
                            <h2>
                                <strong>{{ __('Reset password') }}</strong>
                            </h2>
                        </div>
                        <form class="auth-form mt-4 js-formValidate" method="POST" action="{{ route('password.request') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group line-style mb-4">
                                <label for="email" class="control-label">{{ __('Email') }}</label>
                                <input class="form-control{{ $errors->has('email') ? ' error' : '' }}" name="email" id="email" type="email" placeholder="{{ __('Enter your email') }}" value="{{ $email or old('email') }}" required="required" autocomplete="off" readonly>
                                @if ($errors->has('email'))
                                    <label class="error">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                            <div class="form-group line-style mb-5">
                                <label for="password" class="control-label">{{ __('New Password') }}</label>
                                <input class="form-control{{ $errors->has('password') ? ' error' : '' }}" name="password" id="password" type="password" placeholder="{{ __('Enter your password') }}" value="" required="required" autocomplete="off" autofocus>
                                @if ($errors->has('password'))
                                    <label class="error">{{ $errors->first('password') }}</label>
                                @endif
                            </div>
                            <div class="form-group line-style mb-5">
                                <label for="password" class="control-label">{{ __('Confirm New Password') }}</label>
                                <input class="form-control{{ $errors->has('password_confirmation') ? ' error' : '' }}" name="password_confirmation" id="password_confirmation" type="password" placeholder="{{ __('Enter your password') }}" value="" required="required" autocomplete="off" autofocus>
                                @if ($errors->has('password_confirmation'))
                                    <label class="error">{{ $errors->first('password_confirmation') }}</label>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="row row-10">
                                    <div class="col-sm-6">
                                        <button class="btn btn-orange btn-lg btn-block rounded upper-case" type="submit">{{ __('Submit') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('local-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset(config('_customs.assets.path_css') . '/page-auth.css?v=' . time()) }}">
@endsection

@section('local-scripts')
@endsection

@section('title')
    {{ __('Reset password') }}
@endsection

@section('og')
@endsection
