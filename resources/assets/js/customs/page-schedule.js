$(document).ready(function () {
    var parent = $('#pageSchedule');
    var main = {
        templateDailySchedule: function (data) {
            var self = this, doctor_info = '', doctor_status = '';
            $.each(data.doctors, function (k, v) {
                doctor_info += self.templateDailyScheduleDoctorInfo(v);
                doctor_status += self.templateDailyScheduleDoctorStatus(v);
            });

            return (
                '<div class="schedule-list">' +
                '    <div class="row row-0">' +
                '        <div class="col-xl-2 col-lg-3 col-md-4">' +
                '            <div class="shift-wrap text-center h-100 d-block d-md-flex align-items-center justify-content-center">' +
                '                <span class="shift-time">' + data.shiftTime + '</span>' +
                '            </div>' +
                '        </div>' +
                '        <div class="col-xl-10 col-lg-9 col-md-8">' +
                '            <div class="schedule-doctor-list">' +
                '                <div class="row row-0 h-100">' +
                '                    <div class="col-xl-10 col-lg-9 col-md-7 col-sm-8 col-8">' +
                '                        <div class="shift-wrap h-100 pl-0 pr-0 border-0">' +
                '                            <ol class="list-unstyled doctors">' + doctor_info + '</ol>' +
                '                        </div>' +
                '                    </div>' +
                '                    <div class="col-xl-2 col-lg-3 col-md-5 col-sm-4 col-4">' +
                '                        <div class="shift-wrap h-100 pl-0 pr-0">' +
                '                            <ol class="list-unstyled doctors">' + doctor_status + '</ol>' +
                '                        </div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '</div>'
            );
        },
        templateDailyScheduleDoctorStatus: function (data) {
            return (
                '<li class="js-doctorList" data-id="' + data.doctorId + '"><span class="status ' + (data.status).toLowerCase() + '"><span class="indicator"></span><span class="txt">' + data.status + '</span></span></li>'
            );
        },
        templateDailyScheduleDoctorInfo: function (data) {
            var name = '';
            if(data.doctorId === '') {
                name = '<span class="name ellipsis v-align-m">' + data.name + '</span>';
            } else {
                name = '<a href="javascript:void(0);" class="name ellipsis v-align-m js-showDoctorInfo" data-id="' + data.doctorId + '">' + data.name + '</a>';
            }
            return (
                '<li class="js-doctorList" data-id="' + data.doctorId + '">' +
                '    <div class="row row-5">' +
                '        <div class="col-11">' + name + '</div>' +
                '        <div class="col-1 text-right">' +
                '            <a href="javascript:void(0);" class="remove js-removeDoctor" data-id="' + data.doctorId + '" data-doctor-id="' + data.doctorId + '" data-shift-id="' + data.shiftId + '" data-name="' + data.name + '" data-toggle="tooltip" title="Hapus">' +
                '                <i class="icon-garbage"></i>' +
                '            </a>' +
                '        </div>' +
                '    </div>' +
                '</li>'
            );
        },
        getDailySchedule: function (date) {
            var self = this,
                container = $('.js-dailyScheduleWrapper', parent);

            $.ajax({
                type: 'GET',
                url: api_daily_schedule,
                data: {
                    date: date
                },
                cache: false,
                beforeSend: function () {
                    container.html('' +
                        '<div class="telemed-loading white">' +
                        '    <img src="' + img_loading + '" alt="">' +
                        '</div>');
                },
                success: function (result) {
                    var tpl = '';
                    if(result.status === 'success') {
                        if (result.data !== undefined) {
                            $.each(result.data, function (k, v) {
                                tpl += self.templateDailySchedule(v);
                            });
                        } else {
                            tpl = '<span class="help-block font-14 p-3">Tidak ada data</span>';
                        }

                        container.html(tpl);
                        helper.initTooltip();
                    } else {
                        container.html('<div class="help-block p-3 text-center">' + result.message + '</div>');
                        helper.showNotification({
                            message: result.message,
                            type: 'danger',
                        });
                    }
                    $('[data-toggle="tooltip"]').tooltip('hide');
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to connect to server',
                        type: 'danger',
                    });
                }
            });
        },
        getDoctorInfo: function (id, callback) {
            var self = this,
                container = $('.js-doctorOverall', parent);

            $.ajax({
                type: 'GET',
                url: api_doctor_info,
                data: {
                    id: id
                },
                cache: false,
                beforeSend: function () {},
                success: function (result) {
                    var tpl = result;
                    container.html(tpl);

                    if(typeof(callback) === 'function') {
                        callback();
                    }

                    helper.initTooltip();
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to connect to server',
                        type: 'danger',
                    });
                }
            });
        }
    };

    var today = moment().format('DD-MM-YYYY');
    main.getDailySchedule(today);

    parent.on('click', '.js-changeDailySchedule', function () {
        var me = $(this);
        var mainDate = $('.js-datePickerDaily', parent);
        var date = mainDate.val();
        var type = me.data('type');
        if(type === 'prev') {
            date = moment(date, "DD-MM-YYYY").add(-1, 'days');
        } else {
            date = moment(date, "DD-MM-YYYY").add(1, 'days');
        }
        date = moment(date).format('DD-MM-YYYY');
        mainDate.val(date);
        main.getDailySchedule(date);
    });

    parent.on('dp.change', '.js-datePickerDaily', function (e) {
        var me = $(this);
        var date = $('.js-datePickerDaily', parent).val();
        main.getDailySchedule(date);
    });

    parent.on('click', '.js-showDoctorInfo', function () {
        var me = $(this);
        var id = me.data('id');
        main.getDoctorInfo(id, function () {
            setTimeout(function () {
                me.closest('.telemed-content').animate({
                    scrollTop: 1500
                }, 1000);
            },100);
        });
    });

    parent.on('click', '.js-removeDoctorInfo', function () {
        var me = $(this);
        me.closest('.doctor-overall').slideUp(500, function () {
            $(this).remove();
        });
    });

    parent.on('click', '.js-removeDoctor', function () {
        var me = $(this);
        var id = me.data('id');
        var name = me.data('name');
        var doctor_id = me.data('doctor-id');
        var shift_id = me.data('shift-id');
        var row = $('.js-doctorList[data-id="' + id + '"]', me.closest('.schedule-doctor-list'));

        var swal_opts = {
            title: 'Hapus Dokter',
            text: 'Apakah anda yakin untuk menghapus ' + name + ' dari shift?',
            showCancelButton: true,
            confirmButtonClass: 'btn-primary rounded',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
        };

        helper.showDialogModal(swal_opts, function () {
            $.ajax({
                type: 'GET',
                url: api_delete_schedule,
                data: {
                    doctorId : doctor_id,
                    shiftId : shift_id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                beforeSend: function () {},
                success: function (result) {
                    row.slideUp(300, function () {
                        $(this).remove();
                    });
                    $('[data-toggle="tooltip"]').tooltip('hide');
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to delete shift',
                        type: 'danger',
                    });
                }
            });
        });
    });

    parent.on('click', '.js-showAssignBox', function () {
        var modal = $('#modalAssignDoctor');
        modal.modal('show');

        $('.js-datePickerAssignDoctor').datetimepicker({
            format: 'DD-MM-YYYY',
            useCurrent: false
        });
    });

    $(document).on('click', '.js-btnAssignDoctor', function () {
        var me = $(this);
        var modal = $('#modalAssignDoctor');
        var form = me.closest('form');

        if(form.valid()) {
            var swal_opts = {
                title: 'Assign Dokter?',
                text: 'Apakah anda ingin mendaftarkan dokter ke dalam shift ini?',
                showCancelButton: true,
                confirmButtonClass: 'btn-primary rounded',
                confirmButtonText: 'DAFTARKAN',
                cancelButtonText: 'BATAL',
            };

            modal.modal('hide');
            helper.showDialogModal(swal_opts, function () {
                form.submit();
            }, function () {
                modal.modal('show');
            });
        }

        return false;
    });

    $('.js-datePickerDaily', parent).datetimepicker({
        format: 'DD-MM-YYYY',
        ignoreReadonly: true
    });
});
