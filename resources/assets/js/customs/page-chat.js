$(document).ready(function () {
    var parent = $('#pageChat');
    var isGetQueueList = true, isGetQueueCount = true;
    var emptyMessage = 'Saat ini anda belum memiliki pasien, harap menunggu';
    var firstMessage = 'Hi, Ada yang bisa kami bantu?';
    var main = {
        templateChatItem: function (data) {
            return (
                '<div class="chat-item js-chatQueue" data-user-id="' + data.userId + '" data-name="' + data.userName + '" data-queue-id="' + data.queueId + '" data-image="' + data.imageUrl + '" data-status="' + data.status + '" data-spec="' + data.spec + '">' +
                '    <div class="row row-5">' +
                '        <div class="col-2 text-center">' +
                '            <img src="' + data.imageUrl + '" alt="">' +
                '        </div>' +
                '        <div class="col-10">' +
                '            <div class="row row-5">' +
                '                <div class="col-8">' +
                '                    <div class="name">' + data.userName + '</div>' +
                '                    <div class="spec">' + data.spec + '</div>' +
                '                </div>' +
                '                <div class="col-4 text-right">' +
                '                    <div class="time">' + data.time + '</div>' +
                '                    <a class="btn btn-sm btn-block mt-1 btn-orange rounded">Mulai</a>' +
                '                </div>' +
                '            </div>' +
                '            <div class="row row-5">' +
                '                <div class="col-12 pt-2">' +
                '                    <div class="message">' + data.message + '</div>' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '</div>'
            );
        },
        getQueueList: function (action) {
            var self = this;
            $.ajax({
                type: 'GET',
                url: chat_api_base_url + '/chat/queue',
                data: {},
                cache: false,
                beforeSend: function () {
                    isGetQueueList = false;
                },
                success: function (result) {
                    var tpl = '', totalWaiting = 0;
                    if (result.status === 'success') {
                        if (result.data.length > 0) {
                            $.each(result.data, function (k, v) {
                                if (v.status === 'waiting') {
                                    var spec = '';
                                    spec += ((v.summary.gender).toLowerCase() === 'm') ? 'Pria' : 'Wanita';
                                    spec += (v.summary.age === undefined || v.summary.age === '' || v.summary.age === '-') ? '' : (', ' + v.summary.age + ' tahun');
                                    var dataQueue = {
                                        imageUrl: (v.summary.avatarURL.length > 0) ? v.summary.avatarURL : img_path + '/avatar-default.png',
                                        queueId: v.queue_id,
                                        userName: v.user_name,
                                        userId: v.user_id,
                                        status: v.status,
                                        spec: spec,
                                        time: moment(v.created_at).locale('en').fromNow(),
                                        message: ''
                                    };
                                    tpl += self.templateChatItem(dataQueue);
                                    totalWaiting = totalWaiting + 1;
                                }
                            });

                            var incomingCount = $('.js-incomingCount', parent);
                            incomingCount.each(function () {
                                if ($(this).hasClass('on-tab')) {
                                    $(this).html((totalWaiting > 0) ? '(' + totalWaiting + ')' : '');
                                } else {
                                    $(this).html((totalWaiting > 0) ? totalWaiting : '');
                                }
                            });

                            if (totalWaiting < 1) {
                                tpl = '' +
                                    '<div class="chat-item">' +
                                    '   <div class="empty">' + emptyMessage + '</div>' +
                                    '</div>';
                                $('.js-welcomeMessage', parent).html(emptyMessage);
                            } else {
                                $('.js-welcomeMessage', parent).html('Halaman ini akan menampilkan percakapan anda dengen pasien.<br>Saat ini anda memiliki <span class="js-incomingCount">' + totalWaiting + '</span> pasien yang menunggu, yuk mulai konsultasi anda sekarang!');
                            }
                        } else {
                            tpl = '' +
                                '<div class="chat-item">' +
                                '   <div class="empty">' + emptyMessage + '</div>' +
                                '</div>';
                        }
                        $('#incoming', parent).html(tpl);
                        isGetQueueList = true;
                    } else {
                        helper.showNotification({
                            message: result.message,
                            type: 'danger',
                        });
                    }
                },
                error: function (error) {
                    isGetQueueList = true;
                    if (action !== 'hideErrorMessage') {
                        helper.showNotification({
                            message: 'Failed to get queue lists',
                            type: 'danger',
                        });
                    }
                }
            });
        },
        getQueueCount: function (action) {
            $.ajax({
                type: 'GET',
                url: chat_api_base_url + '/chat/queue/count',
                data: {},
                cache: false,
                beforeSend: function () {
                    isGetQueueCount = false;
                },
                success: function (result) {
                    if (result.status === 'success') {
                        var incomingCount = $('.js-incomingCount', parent);
                        incomingCount.each(function () {
                            if ($(this).hasClass('on-tab')) {
                                $(this).html((result.data > 0) ? '(' + result.data + ')' : '');
                            } else {
                                $(this).html((result.data > 0) ? result.data : '');
                            }
                        });

                        if (result.data < 1) {
                            $('.js-welcomeMessage', parent).html(emptyMessage);
                        } else {
                            $('.js-welcomeMessage', parent).html('Halaman ini akan menampilkan percakapan anda dengen pasien.<br>Saat ini anda memiliki <span class="js-incomingCount">' + result.data + '</span> pasien yang menunggu, yuk mulai konsultasi anda sekarang!');
                        }
                        isGetQueueCount = true;
                    } else {
                        helper.showNotification({
                            message: result.message,
                            type: 'danger',
                        });
                    }
                },
                error: function (error) {
                    isGetQueueCount = true;
                    if (action !== 'hideErrorMessage') {
                        helper.showNotification({
                            message: 'Failed to get queue count',
                            type: 'danger',
                        });
                    }
                }
            });
        },
        getPreScreening: function (elm) {
            var self = this;
            var colDetail = $('.js-colDetail', parent);
            var reference = $('.js-reference', colDetail);
            $('.chat-empty', colDetail).remove();

            if (elm.data('chat-id') !== undefined) {
                $.ajax({
                    type: 'GET',
                    url: chat_api_base_url + '/chat/prescreening/' + elm.data('chat-id'),
                    data: {
                        chat_mode: elm.data('status')
                    },
                    cache: false,
                    success: function (result) {
                        if (result.status === 'success') {
                            var data = result.data;
                            var textInput = $('textarea[data-chat-id="' + elm.data('chat-id') + '"]');
                            var timerWrapper = $('.js-timerWrapper', colDetail);

                            if (data.length !== 0) {
                                $('.js-dataGender', parent).html(data.summary.gender !== undefined ? (((data.summary.gender).toLowerCase() === 'm') ? 'Pria' : 'Wanita') : '-');
                                $('.js-dataAge', parent).html(data.summary.age !== undefined ? (data.summary.age + ' tahun') : '-');
                                $('.js-dataBloodType', parent).html((data.summary.bloodtype === undefined || data.summary.bloodtype === '') ? '-' : data.summary.bloodtype);
                                $('.js-dataHeight', parent).html(data.summary.height !== undefined ? (data.summary.height + ' cm') : '-');
                                $('.js-dataWeight', parent).html(data.summary.weight !== undefined ? (data.summary.weight + ' kg') : '-');

                                var symptoms = data.summary.symptoms;
                                var symptoms_display = '';
                                if (symptoms !== undefined) {
                                    if (symptoms.length > 0) {
                                        $.each(symptoms, function (k, v) {
                                            symptoms_display += v + (k < symptoms.length - 1 ? ', ' : '')
                                        });
                                    } else {
                                        symptoms_display = '-';
                                    }
                                } else {
                                    symptoms_display = '-';
                                }
                                $('.js-dataSymptoms', parent).html(symptoms_display);

                                var allergies = data.summary.allergies;
                                var allergies_display = '';
                                if (allergies !== undefined) {
                                    if (allergies.length > 0) {
                                        $.each(allergies, function (k, v) {
                                            allergies_display += v + (k < allergies.length - 1 ? ', ' : '')
                                        });
                                    } else {
                                        allergies_display = '-';
                                    }
                                } else {
                                    allergies_display = '-';
                                }
                                $('.js-dataAllergies', parent).html(allergies_display);

                                $('.js-dataDuration', parent).html(data.summary.duration);
                                $('.js-dataAdditionalNotes', parent).html((data.summary.symptomsDetail === '' || data.summary.symptomsDetail === undefined) ? '-' : data.summary.symptomsDetail);
                            }

                            if (elm.data('status') === 'accepted' && $('.chat-message', colDetail).length < 1) {
                                textInput.val(firstMessage);
                                setTimeout(function () {
                                    $('.js-sendMessage', colDetail).trigger('click');
                                }, 100);
                            }

                            if (elm.data('status') === 'accepted' || elm.data('status') === 'extended') {
                                self.getMedicalReportForm('a', $('.js-preScreeningA', parent));
                                self.getMedicalReportForm('b', $('.js-preScreeningB', parent));
                            }

                            if (elm.data('status') !== 'match') {
                                reference.removeClass('d-none');
                            } else {
                                reference.addClass('d-none');
                            }

                            $('.js-chatInfo', parent).removeClass('d-none');

                            timerWrapper.addClass('d-none');

                            $('.js-timer', parent).countdown($('.js-timer', parent).data('expired'), function (event) {
                                $(this).html(event.strftime('%M : %S'));
                                timerWrapper.removeClass('d-none');
                            }).on('finish.countdown', function () {
                                textInput.attr('disabled', true);
                                if (data.status === 'match') {
                                    textInput.attr('placeholder', 'Mohon menunggu sampai ' + data.name + ' siap');
                                }
                                textInput.closest('.chat-input').addClass('disabled');

                                if (elm.data('status') === 'accepted' || elm.data('status') === 'extended') {
                                    var swal_opts = {
                                        title: "Informasi",
                                        text: "Waktu percakapan anda telah selesai",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-primary",
                                        confirmButtonText: "Ok",
                                        cancelButtonText: "Tidak",
                                    };

                                    helper.showDialogModal(swal_opts);
                                } else {
                                    timerWrapper.addClass('d-none');
                                }
                            });

                            colDetail.attr('data-prev-status', elm.data('status'));
                        } else {
                            helper.showNotification({
                                message: result.message,
                                type: 'danger',
                            });
                        }
                    },
                    error: function (error) {
                        helper.showNotification({
                            message: 'Failed to get pre screening data',
                            type: 'danger',
                        });
                    }
                });
            }
        },
        joinChatGroup: function (elm, callback) {
            var self = this;
            $.ajax({
                type: 'POST',
                url: chat_api_base_url + '/chat/join',
                dataType: 'json',
                data: {
                    queue_id: elm.data('queue-id')
                },
                cache: false,
                success: function (result) {
                    if (result.status === 'success') {
                        self.createGroupChannel(elm, result.data.chatqueues_id);
                        if (typeof (callback) === 'function') {
                            callback();
                        }
                    } else {
                        /*var swal_opts = {
                            title: "Oops",
                            text: "Pasien ini sedang berkonsultasi dengan dokter lain, silahkan pilih pasien berikutnya",
                            showCancelButton: false,
                            confirmButtonClass: "btn-primary",
                            confirmButtonText: "Ok",
                        };

                        helper.showDialogModal(swal_opts, function () {
                            elm.remove();
                        });*/

                        helper.showNotification({
                            message: result.message,
                            type: 'danger',
                        });
                    }
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to connect to server',
                        type: 'danger',
                    });
                }
            });
        },
        createGroupChannel: function (elm, id) {
            var self = this;
            var time = new Date().getTime();
            var channel_url = 'konsultasi-dokter-' + doctor_userid + '-dengan-user-' + elm.data('user-id') + '-' + time;
            var user_id = elm.data('user-id');
            var user_data = {
                'user_id': user_id,
                'user_name': elm.data('name'),
                'chat_id': id,
                'spec': elm.data('spec'),
                'user_leave_channel': false,
                'status': 'match',
                'expired': moment().format("YYYY/MM/DD H:m:s"),
                'doctor': {
                    'id': doctor_userid,
                    'sendbird_id': doctor_userid_sendbird,
                    'name': doctor_nickname,
                    'avatar': doctor_avatar,
                    'specialization_id': doctor_specialization_id,
                    'specialization_name': doctor_specialization_name
                },
                'channel_url': channel_url,
            };

            var data = {
                'name': elm.data('name'),
                'channel_url': channel_url,
                'cover_url': elm.data('image'),
                'inviter_id': doctor_userid_sendbird,
                'user_ids': [doctor_userid_sendbird, prefix_patient_userid + user_id],
                'data': JSON.stringify(user_data)
            };

            self.createUserSendbird(prefix_patient_userid + user_id, elm.data('name'), elm.data('image'), function () {
                self.createGroupChannelProcess(data);
            });
        },
        createGroupChannelProcess: function (data) {
            var self = this;
            $.ajax({
                type: 'POST',
                url: chat_api_sendbird_base_url + '/group_channels',
                dataType: 'json',
                data: JSON.stringify(data),
                headers: {
                    'Api-token': chat_api_token,
                },
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    setTimeout(function () {
                        self.updateChatListStatus();
                        self.updateSendBirdProfile(doctor_userid_sendbird, {
                            'profile_url': doctor_avatar,
                            'nickname': doctor_nickname
                        });
                    }, 1000);
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to create chat room',
                        type: 'danger',
                    });
                }
            });
        },
        createUserSendbird: function (user_id, nickname, profile_url, callback) {
            var data = {
                'user_id': user_id,
                'nickname': nickname,
                'profile_url': profile_url,
            };

            $.ajax({
                type: 'GET',
                url: chat_api_sendbird_base_url + '/users/' + user_id,
                dataType: 'json',
                data: {},
                headers: {
                    'Api-token': chat_api_token,
                },
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (typeof (callback) === 'function') {
                        callback();
                    }
                },
                error: function (error) {
                    if (error.responseJSON.error) {
                        if (error.responseJSON.message === 'User not found.') {
                            $.ajax({
                                type: 'POST',
                                url: chat_api_sendbird_base_url + '/users',
                                dataType: 'json',
                                data: JSON.stringify(data),
                                headers: {
                                    'Api-token': chat_api_token,
                                },
                                contentType: 'application/json; charset=utf-8',
                                cache: false,
                                success: function (result) {
                                    if (typeof (callback) === 'function') {
                                        callback();
                                    }
                                },
                                error: function (error) {
                                }
                            });
                        }
                    }
                }
            });
        },
        leaveGroupChannel: function (channel_url) {
            var self = this;
            var data = {
                'user_ids': [doctor_userid_sendbird]
            };

            $.ajax({
                type: 'PUT',
                url: chat_api_sendbird_base_url + '/group_channels/' + channel_url + '/leave',
                dataType: 'json',
                data: JSON.stringify(data),
                headers: {
                    'Api-token': chat_api_token,
                },
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    // location.reload();
                    var colDetail = $('.js-colDetail', parent);
                    $('.chat-center', colDetail).html('' +
                        '<div class="chat-empty">' +
                        '   <div class="empty-content">' +
                        '       <div class="content-title"></div>' +
                        '       <div class="content-image"></div>' +
                        '       <div class="content-desc js-welcomeMessage"></div>' +
                        '   </div>' +
                        '</div>');

                    colDetail.removeAttr('channel-url');
                    colDetail.removeAttr('data-status');
                    colDetail.removeAttr('data-prev-status');
                    colDetail.removeAttr('data-chat-id');
                    $('.js-chatInfo', parent).addClass('d-none');
                    self.getQueueCount();
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to leave from chat room',
                        type: 'danger',
                    });
                }
            });
        },
        updateSendBirdProfile: function (user_id, data) {
            $.ajax({
                type: 'PUT',
                url: chat_api_sendbird_base_url + '/users/' + user_id,
                dataType: 'json',
                data: JSON.stringify(data),
                headers: {
                    'Api-token': chat_api_token,
                },
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                },
                error: function (error) {
                }
            });
        },
        endChat: function (chat_id, channel_url, type) {
            var self = this;
            var swal_opts = {
                title: "Akhiri Percakapan",
                text: "Apakah anda yakin ingin mengakhiri percakapan ini?",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            };

            if (type === 'auto') {
                swal_opts = {
                    title: "Informasi",
                    text: "Waktu percakapan anda telah selesai",
                    showCancelButton: false,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "Ok",
                    cancelButtonText: "Tidak",
                };
            }

            helper.showDialogModal(swal_opts, function () {
                self.endChatProcess(chat_id, channel_url);
            });
        },
        endChatProcess: function (chat_id, channel_url) {
            var self = this;
            $.ajax({
                type: 'POST',
                url: chat_api_base_url + '/chat/end',
                dataType: 'json',
                data: {
                    chat_id: chat_id
                },
                cache: false,
                success: function (result) {
                    self.leaveGroupChannel(channel_url);
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to connect to server',
                        type: 'danger',
                    });
                }
            });
        },
        refreshQueue: function () {
            var self = this;
            setInterval(function () {
                if ($('[href="#incoming"]', parent).hasClass('active')) {
                    main.getQueueList('hideErrorMessage');
                    isGetQueueList = false;
                } else {
                    main.getQueueCount('hideErrorMessage');
                    isGetQueueCount = false;
                }
            }, 3000);
        },
        getMedicalReportForm: function (section, container) {
            $.ajax({
                url: $('#preScreeningSectionUrl', parent).val(),
                type: 'GET',
                dataType: 'json',
                data: {
                    section: section
                },
                beforeSend: function () {
                },
                success: function (result) {
                    if (result.status === 'success') {
                        var tpl = '', checked, disabled, required, value;
                        $.each(result.data, function (k, v) {
                            var data_child = '';
                            $.each(v.data, function (x, y) {
                                required = (y.required) ? ' required="required"' : '';
                                if (y.type === 'checkbox') {
                                    checked = (y.value !== null) ? ' checked' : '';
                                    data_child += '' +
                                        '<div class="animated-checkbox mb-3 js-psList" data-type="' + y.type + '" data-required="' + y.required + '" data-placeholder="' + y.placeholder + '" data-label="' + y.label + '">' +
                                        '    <label>' +
                                        '        <input type="checkbox" name="" value=""' + required + checked + '/>' +
                                        '        <span class="label-text">' + y.label + (y.required ? '*' : '') + '</span>' +
                                        '    </label>' +
                                        '</div>';
                                } else if (y.type === 'checkbox_textbox') {
                                    checked = (y.value !== null) ? ' checked' : '';
                                    disabled = (y.value === null) ? ' disabled' : '';
                                    value = (y.value !== null) ? y.value : '';
                                    data_child += '' +
                                        '<div class="animated-checkbox mb-3 js-psList" data-type="' + y.type + '" data-required="' + y.required + '" data-placeholder="' + y.placeholder + '" data-label="' + y.label + '">' +
                                        '    <label>' +
                                        '        <input type="checkbox" name="" value="" data-type="show-input"' + required + checked + '/>' +
                                        '        <span class="label-text">' + y.label + (y.required ? '*' : '') + '</span>' +
                                        '    </label>' +
                                        '    <input class="form-control rounded mt-2" name="" type="text" placeholder="' + y.placeholder + '" autocomplete="off" value="' + value + '"' + disabled + '>' +
                                        '</div>';
                                } else if (y.type === 'textbox') {
                                    value = (y.value !== null) ? y.value : '';
                                    data_child += '' +
                                        '<div class="js-psList" data-type="' + y.type + '" data-required="' + y.required + '" data-placeholder="' + y.placeholder + '" data-label="' + y.label + '">' +
                                        '   <input class="form-control rounded" name="" type="text" placeholder="' + y.placeholder + '" autocomplete="off" value="' + value + '"' + required + '>' +
                                        '</div>';
                                } else if (y.type === 'textarea') {
                                    value = (y.value !== null) ? y.value : '';
                                    data_child += '' +
                                        '<div class="js-psList" data-type="' + y.type + '" data-required="' + y.required + '" data-placeholder="' + y.placeholder + '" data-label="' + y.label + '">' +
                                        '   <textarea class="form-control rounded" name="" placeholder="' + y.placeholder + '"' + required + '>' + value + '</textarea>' +
                                        '</div>';
                                }

                                if (y.required) {
                                    data_child += '<label class="error d-none">Harus Diisi</label>';
                                }
                            });

                            tpl += '' +
                                '<div class="form-section mb-4 js-preScreeningSection">' +
                                '    <div class="form-legend mb-3 js-psLabel">' + v.label + '</div>' +
                                '    <div class="form-group mb-4 js-psData">' + data_child + '</div>' +
                                '</div>';
                        });

                        container.html(tpl);

                        /* FORM */
                        $('[data-type="show-input"]', container).each(function () {
                            $(this).on('click', function () {
                                var me = $(this);
                                var input = me.closest('div').find('input[type="text"]');
                                if (me.is(':checked')) {
                                    input.removeAttr('disabled');
                                    input.focus();
                                } else {
                                    input.attr('disabled', true);
                                    input.val('');
                                }
                            });
                        });
                    } else {
                        helper.showNotification({
                            message: result.message,
                            type: 'danger',
                        });
                    }
                },
                error: function (error) {
                    helper.showNotification({
                        message: 'Failed to get medical report form',
                        type: 'danger',
                    });
                }
            });
        },
        saveMedicalReport: function (btn, chat_id, data) {
            $.ajax({
                url: chat_api_base_url + '/chat/medical/save',
                type: 'POST',
                dataType: 'json',
                data: {
                    chat_id: chat_id,
                    report: data,
                },
                beforeSend: function () {
                    helper.buttonLoading(btn, 'loading');
                },
                success: function (result) {
                    helper.buttonLoading(btn, 'reset');
                    helper.showNotification({
                        message: 'Medical Report berhasil di perbarui',
                        type: 'success',
                    });
                },
                error: function (error) {
                    helper.buttonLoading(btn, 'reset');
                    helper.showNotification({
                        message: 'Failed to to save medical report',
                        type: 'danger',
                    });
                }
            });

            return false;
        },
        updateGroupChat: function (elm) {
            var user_name = $('.name', elm).text(),
                chat_id = elm.attr('data-chat-id'),
                channel_url = elm.attr('data-channel-url'),
                status = elm.attr('data-status'),
                user_leave_channel = elm.attr('data-leave'),
                expired = elm.attr('data-expired'),
                placeholder = 'Silahkan ketik disini...';

            var colDetail = $('.js-colDetail', parent);
            var reference = $('.js-reference', colDetail);

            var textInput = $('textarea', colDetail),
                timer = $('.js-timer', colDetail);

            colDetail.attr('data-chat-id', chat_id);
            colDetail.attr('data-channel-url', channel_url);
            colDetail.attr('data-status', status);

            timer.attr('data-expired', expired);

            if (user_leave_channel === true) {
                placeholder = user_name + ' telah mengakhiri percakapan';
                textInput.attr('disabled', true);
                textInput.closest('.chat-input').addClass('disabled');
                textInput.attr('placeholder', placeholder);
            } else {
                switch (status) {
                    case 'waiting':
                        placeholder = user_name + ' sedang menunggu ingin berkonsultasi dengan anda';
                        break;
                    case 'match':
                        placeholder = 'Mohon menunggu sampai ' + user_name + ' siap';
                        break;
                    case 'declined':
                        placeholder = user_name + ' telah membatalkan konsultasi dengan anda';
                        break;
                    case 'done':
                        placeholder = 'Konsultasi anda dengan ' + user_name + ' telah selesai';
                        break;
                    default:
                        placeholder = 'Silahkan ketik disini...';
                        break;
                }

                if (status === 'accepted' && $('.chat-message', colDetail).length < 1) {
                    textInput.val(firstMessage);
                    setTimeout(function () {
                        $('.js-sendMessage', colDetail).trigger('click');
                    }, 100);
                }

                if (status === 'accepted' || status === 'extended') {
                    textInput.removeAttr('disabled');
                    textInput.closest('.chat-input').removeClass('disabled');
                } else {
                    textInput.attr('disabled', true);
                    textInput.closest('.chat-input').addClass('disabled');
                }

                if (status !== 'match') {
                    reference.removeClass('d-none');
                } else {
                    reference.addClass('d-none');
                }

                textInput.attr('placeholder', placeholder);

                timer.countdown(expired, function (event) {
                    $(this).html(event.strftime('%M : %S'));
                }).on('finish.countdown', function () {
                    textInput.attr('disabled', true);
                    textInput.attr('placeholder', (status === 'match') ? 'Mohon menunggu sampai ' + user_name + ' siap' : 'Waktu percakapan anda telah selesai');
                    textInput.closest('.chat-input').addClass('disabled');
                });
            }
        },
        updateChatListStatus: function () {
            var chatItem = $('.js-chatItem', parent);
            if (chatItem.length > 0) {
                $('.js-ongoingEmpty', parent).remove();
                $('.js-chatItem', parent).each(function () {
                    var me = $(this);
                    var status = me.data('status');
                    var expired = me.data('expired');
                    var leave = me.data('leave');
                    var notes = me.find('.notes');
                    var txt = notes.find('.txt');
                    var timer = notes.find('.timer');
                    var msg = '', time = '';

                    if (status !== 'waiting') {
                        notes.removeClass('timeout');
                        if (status === 'match') {
                            msg = 'Menunggu Pasien';
                            time = '';
                            notes.removeClass('d-none');
                        } else if (status === 'accepted' || status === 'extended') {
                            msg = 'Sisa Waktu:';
                            time = '00:00';
                            notes.removeClass('d-none');
                        } else if (status === 'declined') {
                            msg = 'Dibatalkan';
                            time = '';
                            notes.removeClass('d-none');
                        } else if (status === 'done') {
                            msg = 'Telah berakhir';
                            time = '';
                            notes.removeClass('d-none');
                        } else {
                            notes.addClass('d-none');
                        }

                        timer.text(time);

                        timer.countdown(expired, function (event) {
                            txt.text(msg);
                            $(this).html(event.strftime('%M : %S'));
                        }).on('finish.countdown', function () {
                            if (status === 'accepted' || status === 'extended') {
                                txt.text('Waktu habis');
                                $(this).html('');
                                notes.addClass('timeout');
                            } else if (status === 'done') {
                                txt.text('Telah berakhir');
                                $(this).html('');
                                notes.removeClass('timeout');
                            } else {
                                notes.removeClass('timeout');
                                notes.addClass('d-none');
                            }
                        });
                    }
                });
            } else {
                var tpl = '' +
                    '<div class="chat-item js-ongoingEmpty">' +
                    '   <div class="empty">' + emptyMessage + '</div>' +
                    '</div>';
                $('#default_item_group').html(tpl);
            }
        }
    };

    main.getQueueList();
    main.getQueueCount();
    main.refreshQueue();

    /* CREATE DOCTOR SENDBIRD */
    main.createUserSendbird(doctor_userid_sendbird, doctor_nickname, doctor_avatar);

    parent.on('click', '[href="#incoming"]', function () {
        main.getQueueList();
    });

    parent.on('click', '[href="#ongoing"]', function () {
        setTimeout(function () {
            main.updateChatListStatus();
        }, 2000);
    });

    parent.on('click', '.js-chatQueue', function () {
        var me = $(this);
        main.joinChatGroup(me, function () {
            $('[href="#ongoing"]', parent).trigger('click');
        });
    });

    parent.on('click', '.js-chatItem', function () {
        var me = $(this);
        main.getPreScreening(me);
        $('.js-colDetail', parent).addClass('open');
        $('.js-colDetail', parent).attr('data-chat-id', me.data('chat-id'));
        $('.js-colDetail', parent).attr('data-status', me.data('status'));
        $('.js-colDetail', parent).attr('data-channel-url', me.data('channel-url'));
        $('.chat-input', parent).append($('.js-emojiWrapper', parent).html());
        helper.emojiPicker($('.input-text-area', parent), $('.js-emojiPicker', parent), $('.chat-input', parent));
    });

    parent.on('click', '.js-back', function () {
        $('.js-colDetail', parent).removeClass('open');
    });

    parent.on('click', '.js-summary', function () {
        $('.js-chatInfo', parent).addClass('open');
    });

    parent.on('click', '.js-closeSummary', function () {
        $('.js-chatInfo', parent).removeClass('open');
    });

    parent.on('click', '.js-endChat', function () {
        main.endChat($('.js-colDetail', parent).attr('data-chat-id'), $('.js-colDetail', parent).attr('data-channel-url'));
    });

    parent.on('click', '.js-goToSection', function () {
        var me = $(this);
        var section = $('[href="' + me.data('section') + '"]');
        section.trigger('click');
        me.closest('.tab-content').animate({
            scrollTop: 0
        }, 300);
    });

    parent.on('click', '.js-closeChat', function (e) {
        setTimeout(function () {
            if ($('.js-colDetail', parent).hasClass('open')) {
                $('.chat-empty').remove();
            }
        }, 100);

        var me = $(this);
        var swal_opts = {
            title: "Akhiri Percakapan",
            text: "Apakah anda yakin ingin mengakhiri percakapan ini?",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        };

        helper.showDialogModal(swal_opts, function () {
            main.endChatProcess(me.closest('.js-chatItem').attr('data-chat-id'), me.closest('.js-chatItem').attr('data-channel-url'));
        });

        return false;
    });

    $('.js-submitMedicalReport', parent).on('submit', function () {
        var me = $(this);
        var btn = $('[type="submit"]', me);
        if (me.valid()) {
            var preScreeningSection = $('.js-preScreeningSection', parent);
            var report = [];

            preScreeningSection.each(function () {
                var ps = $(this);
                var psData = $('.js-psData', ps);
                var psDataStore = [];
                $('.js-psList', psData).each(function () {
                    var list = $(this);
                    var value = null;

                    if (list.data('type') === 'checkbox') {
                        value = ($('[type="checkbox"]', list).is(':checked'));
                    } else if (list.data('type') === 'checkbox_textbox') {
                        if ($('[type="checkbox"]', list).is(':checked')) {
                            value = $('[type="text"]', list).val();
                        } else {
                            value = null;
                        }
                    } else if (list.data('type') === 'textbox') {
                        if ($('[type="text"]', list).val() !== '') {
                            value = $('[type="text"]', list).val();
                        } else {
                            value = null;
                        }
                    } else if (list.data('type') === 'textarea') {
                        if ($('textarea', list).val() !== '') {
                            value = $('textarea', list).val();
                        } else {
                            value = null;
                        }
                    }

                    psDataStore.push({
                        "type": list.data('type'),
                        "label": list.data('label'),
                        "required": list.data('required'),
                        "value": value,
                        "placeholder": list.data('placeholder')
                    });
                });
                report.push({
                    "label": $('.js-psLabel', ps).text(),
                    "data": psDataStore
                });
            });

            report = JSON.stringify(report);
            main.saveMedicalReport(btn, $('.js-colDetail', parent).data('chat-id'), report);

            return false;
        }
    });

    /* UPDATE CHANNEL */
    window.chatUpdateDetail = function (channel) {
        var colDetail = $('.js-colDetail', parent);
        var elm = $('.js-chatItem[data-channel-url="' + colDetail.attr('data-channel-url') + '"]');
        if (colDetail.hasClass('open')) {
            main.updateGroupChat(elm);
        }

        main.updateChatListStatus();
    };
});